import pygame
from random import randint

from src.objects.object_base import CustomObject
from src.main_const import *


key_sounds = []

class Key(CustomObject):
    def __init__(self, name, nameHard, x,y, levelData):
        global key_sounds
        if(len(key_sounds) == 0) : Key.pre_init()

        super().__init__(PATH.keys+name+".png",x,y,levelData)
        self.name = name
        self.nameHard = nameHard
        self.nameImg = pygame.image.load(PATH.keys + name+".png").convert_alpha()
        self.nameHardImg = pygame.image.load(PATH.keys + nameHard+"x.png").convert_alpha()
        self.activeName = self.name

        self.dur = -1
        self.hardMode = False

    def update(self):
        super().update()


    def didHeMessUp(self,shouldBeThis):
        global key_sounds
        
        if self.activeName == shouldBeThis:
            self.levelData.channel1.play(key_sounds[randint(0,5)])
            return False
        else:
            self.levelData.channel1.play(key_sounds[6])
            return True

    def makeItHarder(self):
        self.image = self.nameHardImg
        self.activeName = self.nameHard

    def reset(self):
        self.image = self.nameImg
        self.activeName = self.name

    @staticmethod
    def pre_init():
        global key_sounds
        if(len(key_sounds) != 0): return

        key_sounds = [0]*8
        key_sounds[0] = pygame.mixer.Sound(SOUNDS.anime+"aha"+".mp3")
        key_sounds[1] = pygame.mixer.Sound(SOUNDS.anime+"bang"+".mp3")
        key_sounds[2] = pygame.mixer.Sound(SOUNDS.anime+"cute"+".mp3")
        key_sounds[3] = pygame.mixer.Sound(SOUNDS.anime+"nani"+".mp3")
        key_sounds[4] = pygame.mixer.Sound(SOUNDS.anime+"omg"+".mp3")
        key_sounds[5] = pygame.mixer.Sound(SOUNDS.anime+"wow"+".mp3")
        key_sounds[6] = pygame.mixer.Sound(SOUNDS.anime+"scream"+".mp3")
