import pygame
from src.main_const import *
from src.levels.level_factory_data import LevelFactoryData
from src.popups.popup_base import Popup

person_dict = {}

class Cloud(Popup):
    def __init__(self, textArray:list, personArray:list, levelData:LevelFactoryData, onFinish = None):
        super().__init__(onFinish)
        global person_dict
        self.messageIdx = 0
        self.levelData = levelData
        self.textArray = textArray
        self.message = self.textArray[self.messageIdx]
        self.messageSurface = self.levelData.font.render(self.message, True, (0,0,0))
        self.personArray = personArray
        
        self.image_base = pygame.image.load(PATH.obj+"_message.png").convert_alpha()
        self.image_base = pygame.transform.scale(self.image_base, [SCREEN.w, 150])
        self.image_over = pygame.Surface((self.image_base.get_width(), self.image_base.get_height()-5), pygame.SRCALPHA, 32)
        self.image_over.fill((255,255,255))
        self.image = self.image_base.copy()
        self.rect = self.image.get_rect()
        self.rect.topleft = (0,0)
        self.image_over_offset = 0
        self.image_over_offset_max = self.image_base.get_width()
        self.person = person_dict[self.personArray[self.messageIdx]]

    def update(self):
        self.image = self.image_base.copy()
        if self.image_over_offset<self.image_over_offset_max:
            self.image_over_offset += SCREEN.w//(GAME.max_fps*5) ## RATE OF CHANGE
        else:
            self.next()    
        self.image.blit(self.messageSurface, (SCREEN.w//10, 20))
        self.image.blit(self.image_over, (SCREEN.w//10+self.image_over_offset,0)) 
        self.image.blit(self.person, (0,0))  

    def next(self):
        self.messageIdx += 1
        if(self.messageIdx >= len(self.textArray)): self.status = True
        else: 
            self.message = self.textArray[self.messageIdx]
            self.messageSurface = self.levelData.font.render(self.message, True, (0,0,0))
            self.person = person_dict[self.personArray[self.messageIdx]]
            self.image_over_offset = 0

    @staticmethod
    def pre_init():
        global person_dict
        peter = pygame.image.load(PATH.char+"peter.png").convert_alpha()
        wojciu = pygame.image.load(PATH.char+"wojciu.png").convert_alpha()
        weronika = pygame.image.load(PATH.char+"weronika.png").convert_alpha()
        magda = pygame.image.load(PATH.char+"magda.png").convert_alpha()

        peter = pygame.transform.scale(peter, [150,150])
        wojciu = pygame.transform.scale(wojciu, [150,150])
        weronika = pygame.transform.scale(weronika, [150,150])
        magda = pygame.transform.scale(magda, [150,150])


        person_dict["peter"] = peter
        person_dict["wojciu"] = wojciu
        person_dict["weronika"] = weronika
        person_dict["magda"] = magda



