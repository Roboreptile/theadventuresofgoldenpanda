import pygame
from src.main_const import *
from math import floor

class Queue(pygame.sprite.Sprite):
    def __init__(self, queue:dict, levelData, is_heads = False):
        pygame.sprite.Sprite.__init__(self)
        self.levelData = levelData
        self.timer = list(queue.keys())
        self.queue = list(queue.values())


        self.is_heads = is_heads
        if(is_heads == True):
            new_timer = [0]*len(self.timer)
            for idx,t in enumerate(self.timer):
                seconds = floor(t)
                str_t = str(t).split(".")[1]
                t_first = int(str_t[0])
                t_second = int(str_t[1])
                seconds += 0.25*t_first + 0.06*t_second
                new_timer[idx] = seconds
            self.timer = new_timer
            

        for i in range(len(self.timer)):
            self.timer[i] = self.timer[i]*GAME.max_fps*2


        self.idx = 0
        self.time = 0
        self.objects = []
        self.image = pygame.Surface((1,1))
        self.rect = self.image.get_rect()

        self.stopped = False
        


    def update(self):
        if(self.stopped == True): return
        self.time+=1
        if self.idx<len(self.queue):
            if self.timer[self.idx]<=self.time:

                self.objects.append(self.queue[self.idx])
                self.levelData.sprites.add(self.queue[self.idx])
                self.idx +=1
                
        while True:
            f = False
            for idx,obj in enumerate(self.objects):
                if not obj.isActive():
                    self.levelData.sprites.remove(self.objects.pop(idx))
                    f = True
                    break
            if not f: break   

        
    def isFinished(self):
        if self.idx>=len(self.queue) and len(self.objects) == 0: return True
        return False

    def scoreCount(self):
        if(self.is_heads):
            score = 0
            for head in self.queue:
                if(head.is_vlad==False):
                    score += 1
            return score
        return len(self.queue)
        
    def stop(self):
        self.stopped = True
        self.levelData.sprites.remove(self.objects)
        self.objects = []


                    