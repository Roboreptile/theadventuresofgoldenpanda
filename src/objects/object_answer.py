import pygame
from src.main_const import *
from src.objects.object_base import CustomObject


class Answer(CustomObject):
    def __init__(self, x,y, levelData, image, wrap = 80):
        super().__init__(PATH.obj+image,x+10,y,levelData, scale=(700,50))
        self.wrap = wrap-2

    def modify(self, answer ,key,on_answer):
        self.image = self.image_base.copy()
        answers = self.get_wrapped_answer(answer)
        for i in range(len(answers)):
            answers[i] = self.levelData.font_small.render(answers[i], True, (0,0,0))
        self.answers = answers
        self.key = key
        self.on_answer = on_answer
        for i in range(len(self.answers)):
            self.image.blit(self.answers[i], (60,2+20*i))


    def update(self):
        super().update()
        if(self.levelData.events!=None): self.handleEvents()

    def handleEvents(self):
        for event in self.levelData.events:
            if event.type == pygame.MOUSEBUTTONDOWN:
                if self.rect.collidepoint(event.pos):
                    self.on_answer(self.key)

    def get_wrapped_answer(self, answer):
        answers = []
        while(len(answer)>self.wrap):
            answers.append(answer[0:self.wrap] + "-")
            answer = "-" + answer[self.wrap:]
        answers.append(answer)
        return answers
        


                


