from src.objects.object_base import CustomObject
from src.levels.level_factory_data import LevelFactoryData
from src.main_const import *

class SecretBush(CustomObject):
    def __init__(self, x, y, levelData:LevelFactoryData):
        super().__init__(PATH.obj+"big_bush.png", x, y, levelData)

    def enter(self, pos):
        if abs(self.real_x - pos) < self.rect.width//2:
            if self.levelData.im_cheating == True: return True
            file = open("./src/.SAVE", "r")
            text = file.readline()
            text = [a for a in text]
            file.close()
            
            if(text[0] == "5" and
            text[1] == "5" and
            text[2] == "5" and
            text[3] == "5" and
            text[4] == "5" and
            text[5] == "5" and
            text[6] == "5"):
                return True   
        return False
    def could_enter(self, pos):
        return self.enter(pos)
