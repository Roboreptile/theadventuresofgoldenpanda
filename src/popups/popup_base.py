import pygame
from os import listdir
from src.main_const import *
from pygame.surface import Surface
from pygame.image import load

class Popup(pygame.sprite.Sprite):
    def __init__(self, functionPointer):
        pygame.sprite.Sprite.__init__(self)
        self.status = False
        self.f = functionPointer

    def isFinished(self):
        if(self.status and self.f!=None): self.f()
        return self.status

    



