from src.objects.object_base import CustomObject
from src.levels.level_factory_data import LevelFactoryData
from src.main_const import *

class Repeater(CustomObject):
    def __init__(self, count, levelData:LevelFactoryData, callback):
        super().__init__(PATH.obj+"transparent.png", 0, 0, levelData)
        self.counter = 0
        self.count=count
        self.callback = callback
    def update(self):
        super().update()
        self.counter+=1
        if(self.counter==self.count):
            self.callback()
            self.counter = 0