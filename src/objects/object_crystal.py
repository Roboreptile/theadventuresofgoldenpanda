from src.objects.object_base import CustomObject
from src.levels.level_factory_data import LevelFactoryData
from src.main_const import *

class Crystal(CustomObject):
    def __init__(self, color, x, y, levelData:LevelFactoryData):
            super().__init__(PATH.anims+color+".png", x, y, levelData, 0, 65, 3)

    def enter(self, pos):
        if abs(self.real_x - pos) < self.rect.width//2:
            return True   
        return False
    def could_enter(self, pos):
        return self.enter(pos)