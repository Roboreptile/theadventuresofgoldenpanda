import pygame
from src.popups.popup_plus import PopupPlus
from src.main_const import *

s3_array = []

class Spark3(PopupPlus):
    def __init__(self, levelData, onFinish = None, change_every_x_frames = 4):
        global s3_array
        super().__init__(onFinish, s3_array, levelData, change_every_x_frames, True)

    @staticmethod
    def pre_init():
        global s3_array
        s3_array = PopupPlus.loadGIFFolder("sparkles3",scale=(400,400))