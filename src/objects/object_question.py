import pygame
from src.main_const import *
from src.objects.object_base import CustomObject

class Q:
    def __init__(self, image, sound, text, a,b,c, a_key = None, b_key = None, c_key = None, on_finish = None, bg = None):
        self.image = image
        self.sound = sound
        self.a = a
        self.b = b
        self.c = c
        self.a_key = a_key
        self.b_key = b_key
        self.c_key = c_key
        self.on_finish = on_finish
        self.text = text
        self.bg = bg


class Question(CustomObject):
    def __init__(self, x,y, levelData, a,b,c, wrap = 80, is_romantic = True, scale=(700,445)):
        super().__init__(PATH.obj+"transparent.png",x+10,y,levelData, scale=scale)
        
        self.a = a
        self.b = b
        self.c = c

        self.wrap = wrap
        self.answered = False
        self.answer = None
        self.on_finish = None
        self.block = False

        self.cloud = pygame.image.load(PATH.obj+"head_cloud.png").convert_alpha()
        self.cloud = pygame.transform.scale(self.cloud, (720,100))

        self.romantic = is_romantic
        self.scale = scale


    def modify(self, q:Q, path_img =PATH.dating, path_sound = SOUNDS.dating_sim):
        self.levelData.channel2.play(pygame.mixer.Sound(path_sound+q.sound+".mp3"))
        self.image_base = pygame.image.load(path_img+q.image).convert_alpha()
        self.image_base = pygame.transform.scale(self.image_base, self.scale)
        self.image = self.image_base
        self.images = [self.image_base]
 
        x = 50
        y = 150
        y_off = 40
        font = self.levelData.font
        color = (200,200,0)
        if(self.romantic):
            self.image.blit(self.cloud, (0,0))
            x = 10
            y = 5
            y_off = 20
            font = self.levelData.font_small
            color = (0,0,0)

        texts = self.get_wrapped_text(q.text)
        for i in range(len(texts)):
            texts[i] = font.render(texts[i], True, color)
            self.image.blit(texts[i], (x,y+y_off*i))        

        self.a.modify(q.a,q.a_key,self.on_answer)
        self.b.modify(q.b,q.b_key,self.on_answer)
        self.c.modify(q.c,q.c_key,self.on_answer)

        self.on_finish = q.on_finish
        self.answered = False
        self.answer = None
        self.block = False

        if(q.bg!=None):
            s = pygame.mixer.Sound(q.bg)
            s.set_volume(0.5)
            self.levelData.channel0.play(s)

    def stop(self):
        self.block = True
        self.on_finish = None
        self.answered = False
        self.answer = None

    def on_answer(self,key:str):
        self.answered = True
        self.answer = key

    def get_wrapped_text(self, text):
        texts = []

        t_arr = text.split(" ")
        while(len(t_arr)>0):
            t = []
            l = 0
            idx = 0
            while True:
                if(len(t_arr)==idx): break
                if(l + len(t_arr[idx])<self.wrap):
                    t.append(t_arr[idx])
                    l += len(t_arr[idx])
                else: break
                idx+=1

            texts.append(" ".join(t))

            if(len(t_arr) == idx): break
            else: t_arr = t_arr[idx:]

        return texts
        