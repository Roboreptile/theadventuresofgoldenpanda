# TheAdventuresOfGoldenPanda

## Before Lanuch:

1. In the command line, navigate to the folder directory and run (preferably on a fresh python environment):
   Windows:
   `python -m pip install -r requirements.txt`
   Linux/Mac:
   `python3 -m pip install -r requirements.txt`
2. (Optional) If the screen size is incorrect, change it in the *src/const.py* - the default is 1800 x 720

## Controls:

* Move - Arrow keys
* Jump - Arrow 'Up'
* Go inside/Open - Spacebar
* Restart level - TAB

## Launch:

On Windows:

`python main.py`

On Linux/Mac:

`python3 main.py`

## There is nothing else here

There is nothing else here

There is nothing else

There is nothing

There is

nothing
