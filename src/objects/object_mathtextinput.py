import pygame
from src.main_const import *
from src.levels.level_factory_data import LevelFactoryData
from src.objects.object_base import CustomObject
from src.objects.object_textinput import TextInput

class MathTextInput(TextInput):
    def __init__(self, x, y,  levelData, size = (280,50), ):
        super().__init__(x, y, levelData, size, "Wynik", (128,128,128),(0,0,0),"abc",2)

        self.txt_surface = self.levelData.font.render(self.string, True, self.color)
        

        self.active_surface = pygame.Surface((10,10), pygame.SRCALPHA, 32)
        self.active_surface.fill((0,255,0))

    def update(self):
        super().update()
    
    def isCorrect(self):
        if self.correct:
            self.correct = False
            self.string = self.message
            self.active = False
            return True
        return False