import pygame
from src.main_const import *
from src.popups.popup_base import Popup

heart_array = []

class Heart(Popup):
    def __init__(self, x,y, levelData, onFinish = None, change_every_x_frames = 1):
        global heart_array
        super().__init__(onFinish)
        self.idx = 0
        self.counter = 0
        self.images = heart_array.copy()
        self.image = self.images[self.idx]
        self.rect = self.image.get_rect()
        self.rect.center = (x,y)
        self.target = change_every_x_frames
        self.levelData = levelData
        self.pos = (x,y)

    def update(self):
        global heart_array
        self.counter +=1
        if self.counter>=self.target:
            self.counter = 0
            self.idx += 1
            if self.idx>=len(heart_array):
                self.status = True
            else:
                self.image = self.images[self.idx]

    @staticmethod
    def pre_init(scale = (200,200), count = 15, file = PATH.anims+"hearts.png"):
        global heart_array
        heart_array = [0]*count
        img = pygame.image.load(file)
        for i in range(0, count):
            w = img.get_width()//count
            h = img.get_height()
            new_surface = pygame.Surface((w,h), pygame.SRCALPHA, 32).convert_alpha()
            new_surface.blit(img, (-w*i,0))
            if scale!= None: new_surface = pygame.transform.scale(new_surface, scale)
            heart_array[i] = new_surface.copy()
