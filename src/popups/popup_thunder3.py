import pygame
from src.popups.popup_plus import PopupPlus
from src.main_const import *

t3_array = []

class Thunder3(PopupPlus):
    def __init__(self, levelData, onFinish = None, change_every_x_frames = 2):
        global t3_array
        super().__init__(onFinish, t3_array, levelData, change_every_x_frames, True)

    @staticmethod
    def pre_init():
        global t3_array
        t3_array = PopupPlus.loadGIFFolder("thunder3",scale=(400,400))