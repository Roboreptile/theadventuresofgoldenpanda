import pygame

from src.main_const import *
from src.player_main import Player

from src.popups.popup_thunder1 import Thunder1
from src.popups.popup_thunder2 import Thunder2
from src.popups.popup_thunder3 import Thunder3
from src.popups.popup_spark1 import Spark1
from src.popups.popup_spark2 import Spark2
from src.popups.popup_spark3 import Spark3

from src.levels.level_factory import LevelFactory
from src.levels.level_factory_data import LevelFactoryData
from src.popups.popup_darkness import Darkness
from src.popups.popup_heart import Heart
from src.popups.popup_trophy import Trophy
from src.popups.popup_over import GameOver
from src.popups.popup_genie_cloud import GenieCloud
from src.popups.popup_cloud import Cloud
from src.popups.popup_thumbs_down import ThumbsDown
from src.popups.popup_thumbs_up import ThumbsUp
from src.popups.popup_lolol import Lolol
from src.popups.popup_darksouls import DarkSouls
from src.popups.popup_light import Light
from src.popups.popup_boom import Boom
from src.popups.popup_fuse import Fuse
from src.popups.popup_correct import Correct
from src.popups.popup_wrong import Wrong

from src.popups.popup_crit_laser import LaserCrit
from src.popups.popup_fire import FireBoom
from src.popups.popup_forestfire import ForestFire
from src.popups.popup_heal import Heal
from src.popups.popup_ima_fire import ImaFireMyLazer
from src.popups.popup_kapow import Kapow
from src.popups.popup_ko import KO
from src.popups.popup_megalaser import MegaLaser
from src.popups.popup_load import Load
from src.popups.popup_nuke import Nuke
from src.popups.popup_planetboom import PlanetBoom



pygame.mixer.pre_init(44100, -16, 2, 512)
pygame.mixer.init()
pygame.font.init()

screen = None
background = None
inGame = True
levelData = LevelFactoryData()
levelData.font = pygame.font.SysFont('Comic Sans MS', 30)
levelData.font_small = pygame.font.SysFont('Comic Sans MS', 18)

#############
levelData.im_cheating = False
#############


# DO NOT EDIT ANYTHING UNLESS EXPLICITLY ALLOWED
class Game:

    @staticmethod
    def create():
        global screen, background, LevelFactoryData
        
        screen = pygame.display.set_mode([SCREEN.w,SCREEN.h])
        background = pygame.image.load(PATH.bgs+levelData.background).convert_alpha()
        background = pygame.transform.scale(background, [SCREEN.w,SCREEN.h])
        levelData.old_background = levelData.background
        screen.blit(background, (0,0))

        pygame.display.set_caption("The Adventures Of Golden Panda")
        pygame.display.update()

        # Add here inits for animations to load them into memory
        Darkness.pre_init()
        Heart.pre_init()
        Trophy.pre_init()
        GameOver.pre_init()
        GenieCloud.pre_init()
        Cloud.pre_init()
        ThumbsUp.pre_init()
        ThumbsDown.pre_init()
        Thunder1.pre_init()
        Thunder2.pre_init()
        Thunder3.pre_init()
        Spark1.pre_init()
        Spark2.pre_init()
        Spark3.pre_init()
        Lolol.pre_init()
        DarkSouls.pre_init()
        Light.pre_init()
        Boom.pre_init()
        Fuse.pre_init()
        LaserCrit.pre_init()
        FireBoom.pre_init()
        ForestFire.pre_init()
        Heal.pre_init()
        ImaFireMyLazer.pre_init()
        Kapow.pre_init()
        KO.pre_init()
        MegaLaser.pre_init()
        Load.pre_init()
        Nuke.pre_init()
        PlanetBoom.pre_init()
        Correct.pre_init()
        Wrong.pre_init()

    @staticmethod
    def play():
        global screen,background,inGame, levelData
        
        gameClock = pygame.time.Clock()
        pygame.mouse.set_system_cursor(pygame.SYSTEM_CURSOR_HAND)
        LevelFactory.init(levelData)
        levelData.player = Player(levelData)       
        levelData.popup_manager.add(Darkness(speed = GAME.max_fps//3, reverse=True))

        levelData.channel0 = pygame.mixer.Channel(0)
        levelData.channel1 = pygame.mixer.Channel(1)
        levelData.channel2 = pygame.mixer.Channel(2)
        levelData.channel3 = pygame.mixer.Channel(3)
        levelData.channel4 = pygame.mixer.Channel(4)


        levelData.clouds.append(Cloud(
            textArray = [
                "<UPDATE 1.0 - zmiany dostępne w CHANGELOG>",
                "Hiii there hello handsome man",
                "Mam nadzieję że przeczytałeś README bo nie chce mi się powtarzać controls :P",
                "Witaj w świecie krówciolandii, gdzie moc przyjaźni pokona nawet nagorsze potwory!",
                "Jednak aby ją ujażmić, musisz najpierw zdobyć 7 kryształów mocy w kolorach takich, jak drzwi, które teraz widzisz.",
                "Każde drzwi mają własną historię powstawania. Możesz dowolnie każde otworzyć i zajrzeć, co jest w środku.",
                "Powodzenia w zdobywaniu 7 kryształów!"
            ], 
            personArray = ["peter","peter","peter","peter","peter","peter","peter"], 
            levelData = levelData,),
        )

        while inGame:
            Game._handle_events()
            if(levelData.old_background != levelData.background):
                background = pygame.image.load(PATH.bgs+levelData.background).convert_alpha()
                background = pygame.transform.scale(background, [SCREEN.w,SCREEN.h])
                levelData.old_background = levelData.background
            LevelFactory.current()
            levelData.sprites.update()

            levelData.sprites.draw(screen)

            pygame.display.update()
            levelData.sprites.clear(screen, background)
            
            gameClock.tick(GAME.max_fps+1)           
        pygame.quit()
        exit()

    @staticmethod
    def _handle_events():
        global inGame, levelData
        levelData.events = pygame.event.get()
        for event in levelData.events:
            if event.type == pygame.QUIT:
                inGame = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    inGame = False
                if event.key == pygame.K_RIGHT:
                    levelData.player.move("right")
                if event.key == pygame.K_LEFT:
                    levelData.player.move("left")
                if event.key == pygame.K_UP:
                    levelData.player.jump()
                if event.key == pygame.K_SPACE:
                    if(levelData.popup_manager.ping()):
                        levelData.player.ping()
                if event.key == pygame.K_TAB:
                    levelData.level.reset()
            elif event.type == pygame.KEYUP:
                if event.key == pygame.K_RIGHT:
                    levelData.player.move("right",0)
                if event.key == pygame.K_LEFT:
                    levelData.player.move("left",0)

if __name__ == "__main__":
    mode = 0
    if(mode==1):
        try:
            Game.create()
            Game.play()
        except Exception as e:
            print("======= ERR =======")
            print(e)
            pygame.quit()
    else:
        Game.create()
        Game.play()