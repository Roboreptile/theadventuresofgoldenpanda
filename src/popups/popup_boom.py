import pygame
from src.popups.popup_plus import PopupPlus
from src.main_const import *

boom_array = []

class Boom(PopupPlus):
    def __init__(self, x, y, levelData, onFinish = None, change_every_x_frames = 2):
        global boom_array
        y = SCREEN.h - TILE.h - y + TILE.offset
        super().__init__(onFinish, boom_array, levelData, change_every_x_frames, x=x, y=y)

        self.levelData.channel3.play(pygame.mixer.Sound(SOUNDS.f+"boom.mp3"))

    @staticmethod
    def pre_init():
        global boom_array
        boom_array = PopupPlus.loadGIFFolder("boom")