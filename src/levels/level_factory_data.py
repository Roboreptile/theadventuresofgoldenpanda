from pygame.sprite import OrderedUpdates

class LevelFactoryData:
    color:str = "all"
    prev_color:str = "none"
    room_nr:int = 0
    prev_room_nr:int = 0

    level = 0
    level_len = 0
    sprites:OrderedUpdates = OrderedUpdates()
    font = 0
    font_small = 0
    level_sprites = []
    popup_manager = 0
    
    player = 0
    offset = [0]
    player_hand = [0]

    background = "loading.png"
    old_background = "none"
    
    events = None
    im_cheating = False

    channel0 = None ##bg
    channel1 = None ##objects
    channel2 = None ##doors
    channel3 = None ##effects
    channel4 = None ##idk


    force_restart = False
    clouds = []
