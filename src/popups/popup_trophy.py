import pygame
from src.main_const import *
from src.popups.popup_base import Popup

trophy_array = []

class Trophy(Popup):
    def __init__(self, levelData, onFinish = None, change_every_x_frames = 2):
        global trophy_array
        super().__init__(onFinish)
        self.idx = 0
        self.counter = 0
        self.images = trophy_array.copy()
        self.image = self.images[self.idx]
        self.rect = self.image.get_rect()
        self.rect.center = (SCREEN.w//2,SCREEN.h//2)
        self.target = change_every_x_frames
        self.levelData = levelData
        self.pos = self.rect.center

        self.levelData.channel3.play(pygame.mixer.Sound(SOUNDS.welcome+"victory.mp3"))

    def update(self):
        global trophy_array
        self.counter +=1
        if self.counter>=self.target:
            self.counter = 0
            self.idx += 1
            if self.idx>=len(trophy_array):
                self.status = True
            else:
                self.image = self.images[self.idx]

    @staticmethod
    def pre_init(scale = (400,400), count = 48, file = PATH.anims+"trophy.png"):
        global trophy_array
        trophy_array = [0]*count
        img = pygame.image.load(file)
        for i in range(0, count):
            w = img.get_width()//count
            h = img.get_height()
            new_surface = pygame.Surface((w,h), pygame.SRCALPHA, 32).convert_alpha()
            new_surface.blit(img, (-w*i,0))
            if scale!= None: new_surface = pygame.transform.scale(new_surface, scale)
            trophy_array[i] = new_surface.copy()