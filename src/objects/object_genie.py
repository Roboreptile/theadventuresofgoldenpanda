import pygame
from math import cos,pi

from src.objects.object_base import CustomObject
from src.main_const import *
from src.levels.level_factory_data import LevelFactoryData


class Genie(CustomObject):
    def __init__(self, x, y, levelData, size, background = None):
        super().__init__(PATH.obj+"genie.png", x, y, levelData, scale=size)
        self.radians = 0
        
    def update(self):
        super().update()
        self.rect.center = (self.rect.center[0],self.rect.center[1] + cos(self.radians)*30)
        self.radians += 2*pi/180
        if self.radians>=2*pi: self.radians = 0 

