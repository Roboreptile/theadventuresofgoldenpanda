import pygame
from src.popups.popup_plus import PopupPlus
from src.main_const import *


class PopupBoss(PopupPlus):
    def __init__(self, levelData, pos = None, onFinish = None, arr =[], change_every_x_frames = 2 , centered = False):
        if(pos == None):
            super().__init__(onFinish, arr, levelData, change_every_x_frames, False)
        else:
            p = [pos[0],pos[1]]
            if(centered == False):
                p[1] = SCREEN.h - TILE.h - pos[1] + TILE.offset
                
            super().__init__(onFinish, arr, levelData, change_every_x_frames, False, p[0], p[1], centered)

    @staticmethod
    def loadGIFFolder(folder, scale = None):
        return PopupPlus.loadGIFFolder("_boss/"+folder, scale)