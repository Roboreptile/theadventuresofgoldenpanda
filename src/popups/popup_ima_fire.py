import pygame
from src.popups.popup_boss import PopupBoss
from src.main_const import *


imafire_boss_arr = []

class ImaFireMyLazer(PopupBoss):
    def __init__(self, levelData, pos, onFinish = None):
        global imafire_boss_arr
        super().__init__(levelData, pos, onFinish, imafire_boss_arr, 6, True)

    @staticmethod
    def pre_init():
        global imafire_boss_arr
        imafire_boss_arr = PopupBoss.loadGIFFolder("iam_firin_mah_lazer", scale = (300,300))

