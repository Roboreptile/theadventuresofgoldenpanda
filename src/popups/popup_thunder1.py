import pygame
from src.popups.popup_plus import PopupPlus
from src.main_const import *

t1_array = []

class Thunder1(PopupPlus):
    def __init__(self, levelData, onFinish = None, change_every_x_frames = 2):
        global t1_array
        super().__init__(onFinish, t1_array, levelData, change_every_x_frames, True)

    @staticmethod
    def pre_init():
        global t1_array
        t1_array = PopupPlus.loadGIFFolder("thunder1",scale=(400,400))