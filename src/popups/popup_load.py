import pygame
from src.popups.popup_boss import PopupBoss
from src.main_const import *


load_boss_arr = []

class Load(PopupBoss):
    def __init__(self, levelData, pos, onFinish = None):
        global load_boss_arr
        super().__init__(levelData, pos, onFinish, load_boss_arr, 2, True)

    @staticmethod
    def pre_init():
        global load_boss_arr
        load_boss_arr = PopupBoss.loadGIFFolder("load", scale = (300,300))

