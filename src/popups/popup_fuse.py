import pygame
from src.popups.popup_plus import PopupPlus
from src.main_const import *

fuse_array = []

class Fuse(PopupPlus):
    def __init__(self, x, y, levelData, onFinish):
        global fuse_array
        y = SCREEN.h - TILE.h - y + TILE.offset
        super().__init__(onFinish, fuse_array, levelData, 2, False, x=x, y=y)

        self.levelData.channel3.play(pygame.mixer.Sound(SOUNDS.f+"fuse.mp3"))

    @staticmethod
    def pre_init(seconds=20):
        global fuse_array

        fuse_array = PopupPlus.loadGIFFolder("fuse")
        fuse_array = fuse_array*(seconds*2//3)