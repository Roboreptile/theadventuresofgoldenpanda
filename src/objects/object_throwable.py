import pygame
from src.main_const import *
from src.levels.level_factory_data import LevelFactoryData
from src.objects.object_base import CustomObject
from src.popups.popup_heart import Heart
from src.popups.popup_darkness import Darkness
from random import randint



class Throwable(CustomObject):
    def __init__(self, image, x,y, xvel, yvel, levelData, isWalking = True, _type = None, surface = None, random_angle = 0, reduce_g = 1):
        if _type == "Turtle":
            y = randint(-10, SCREEN.h-300)
            if xvel < 0 : image = PATH.obj + "turtler.png"
            else: image = PATH.obj + "turtle.png"
        super().__init__(image,x,y,levelData, angle_change=random_angle, image_base=surface)
        self.type = _type
        self.xvel = xvel
        self.yvel = yvel
        self.isWalking = isWalking
        self.originalyvel = yvel
        self.originalxvel = xvel
        self.originaly = self.rect.centery
        self.reduce_g = reduce_g
        
    def update(self):
        
        if self.isWalking:
            self.rect_base = (self.rect_base[0] + self.xvel, self.rect_base[1])
        else:
            self.yvel -= PLAYER.g//self.reduce_g
            self.rect_base = (self.rect_base[0] + self.xvel, self.rect_base[1] - self.yvel)

        super().update()     
        if self.active:
            if self.rect.topleft[0] > SCREEN.w or self.rect.topright[0] < 0:
                self.active = False
            if self.rect.topleft[1] > SCREEN.h - TILE.h + TILE.offset:
                self.active = False

        if self.type == None:
            pass
        elif self.type == "Turtle":
            self.typeTurtle()
        elif self.type == "Bird":
            self.typeBird()
        elif self.type == "Wombat":
            self.typeWombat()

    def typeTurtle(self):
        if self.active and self.levelData.player_hand[0]:
            for event in self.levelData.events:
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if self.rect.collidepoint(event.pos):
                        self.levelData.player_hand[0] = 0
                        self.levelData.level.increaseScore()
                        self.active = False
                        self.levelData.popup_manager.add(Heart(self.rect.centerx,self.rect.centery, self.levelData, None, 2))


    def typeBird(self):
        if not self.active:
            self.xvel = self.xvel
            self.yvel = randint(self.originalyvel-15,self.originalyvel+5)
            self.active = True
            if self.xvel>0:
                self.rect_base = (-self.levelData.offset[0], self.originaly)
            else:
                self.rect_base = (-self.levelData.offset[0]+SCREEN.w, self.originaly)

    def typeWombat(self):
        if not self.active:
            self.xvel = randint(self.originalxvel-5,self.originalxvel+5)
            self.yvel = 0
            self.active = True
            if self.xvel>0:
                self.rect_base = (-self.levelData.offset[0], self.originaly)
            else:
                self.rect_base = (-self.levelData.offset[0]+SCREEN.w, self.originaly)
    def isActive(self):
        return self.active