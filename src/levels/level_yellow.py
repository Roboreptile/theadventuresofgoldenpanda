import pygame
from src.objects.object_genie_textinput import GenieTextInput
from src.main_const import *
from src.levels.level_base import Level
from src.objects.object_base import CustomObject
from src.objects.object_floor import Floor
from src.objects.object_portal import Portal
from src.objects.object_textinput import TextInput
from src.objects.object_board import Board
from src.objects.object_door import Door
from src.objects.object_throwable import Throwable
from src.objects.object_basket import Basket
from src.objects.object_seaweed import SeaWeed
from src.objects.object_queue import Queue
from src.popups.popup_trophy import Trophy
from src.popups.popup_over import GameOver
from src.objects.object_keyboard import KeyBoard
from src.objects.object_keyboard_button import Key
from src.objects.object_genie import Genie
from src.popups.popup_genie_cloud import GenieCloud
from src.objects.object_luckyblock import LuckyBlock
from src.objects.object_answer import Answer
from src.objects.object_question import Question, Q
from src.objects.object_quiz import Quiz
from src.popups.popup_cloud import Cloud
from src.popups.popup_darksouls import DarkSouls
from src.objects.object_crystal import Crystal



class YellowLevelOne(Level):
    def __init__(self, levelData):
        super().__init__(levelData)
        self.levelData.level_len = TILE.w*16
        self.levelData.background = "palace.jfif"

        self.genietextBox = GenieTextInput(600, 360, self.levelData, size = (600,50))
        self.textBox = TextInput(1610, 360, self.levelData, key = "lolol")

        self.genie = Genie(-400,-400,levelData,size=(240,350))
        
        self.starting_objects = [
            Portal(0,0,levelData),
            Door("yellow", 1600, 0, levelData),
            Board(1600,350, levelData, size = (330, 70)),
            self.textBox,
            self.genietextBox,
            self.genie,
        ]
        self.stageOne()
        self.loadProgress()

        levelData.popup_manager.add(GenieCloud(500,300,self.genie,levelData,onFinish=None,change_every_x_frames=3))

        s = pygame.mixer.Sound(SOUNDS.yellow+"cave.mp3")
        s.set_volume(0.5)
        self.levelData.channel0.play(s)

        self.levelData.popup_manager.add(Cloud(
            textArray = [
            "Witaj na poziomach żółtych - CRAZY",
            "Oto przed Tobą magiczny cool ziom Genie.","Potrafi odpowiedzieć na każde pytanie!",
            "Ale odpowiada tylko tak/nie.","Spróbuj zgadnąć hasło przy jak najmniejszej ilości pytań!",
            "<NLP AI trochę nawala ;D Have fun!>"], 
            personArray = ["peter","peter","peter","peter","peter","peter"], 
            levelData = levelData,),
        )



        if levelData.im_cheating: self.open()

    def stageOne(self):
        self.objects = self.starting_objects.copy()
        

    def doNextStep(self):
        if self.textBox.isCorrect():
            self.levelData.popup_manager.add(Trophy(self.levelData, onFinish=lambda:self.open())) 



class YellowLevelTwo(Level):
    def __init__(self, levelData):
        super().__init__(levelData)
        self.levelData.level_len = TILE.w*13
        #self.levelData.background = "main.png"

        self.a = Answer(500,120,levelData, "answer2.png")
        self.b = Answer(500,70,levelData, "answer2.png")
        self.c = Answer(500,20,levelData, "answer2.png")

        self.question = Question(500,165,levelData, self.a, self.b, self.c)
        self.quiz = Quiz(500,0,levelData, self.question,
        {
            "hi":Q("1.jpg","1", 
                "Cześć przystojniaku...",
                "Hello m'lady would you like to ride my horse", 
                "Um... cześć, czy mogę postawić Ci drink-k-k-aaa...?",
                "Cześć, czy Twój ojciec jest wolny?",
                a_key="2",b_key="1.2.f",c_key="3"),
            "1.2.f":Q("1.2.f.jpg","1.2.f",
                "Umm.. A MOŻE KUP SOBIE TROCHĘ MĘSKOŚCI!", 
                ":(", ":(",":(", 
                on_finish=lambda:self.defeat()),
            "2":Q("2.jpg","2", 
                "Aww, masz kucyka?",
                "Oriera! <mrug> <mrug>", 
                "Jest w mojej piwnicy, chcesz zobaczyć?",
                "Pani, prawdziwy certyfikowany kuc, nawet na korwina głosuje!",
                a_key="4", b_key = "2.2.f", c_key = "2.3.f"),
            "2.2.f":Q("1.2.f.jpg","2.2.f",
                "Myślisz, że tak się zwraca do damy?",
                ":(", ":(",":(", 
                on_finish=lambda:self.defeat()),
            "2.3.f":Q("1.2.f.jpg","2.3.f",
                "Fuuu!!!",
                ":(", ":(",":(",  
                on_finish=lambda:self.defeat()),
            "3":Q("3.jpg","3", 
                "Nie, jest w więzieniu za przemyt cukru białego bez podatku VAT! ;( <płacze>",
                "My horse would make you feel better!", 
                "Czy to znaczy że rodziców nie ma w domu?",
                "Wygląda że posada 'daddy' jest w Twojej rodzinie wolna?", 
                a_key = "2.2.f", b_key = "5", c_key = "6"),
            "4":Q("1.jpg","4", 
                "Ogiery często parskają. Myślisz, że na jego widok też parsknę śmiechem? ",
                "Możemy sprawdzić tę teorię w toalecie",
                "Słyszysz ten trzask? To moje ego które się rozsypało na 1000 kawałeczków :(",
                "Mówiłem już, że mam jacht?", 
                a_key = "4.1.f", b_key = "4.2.f", c_key = "4.3.f"),
            "4.1.f":Q("1.2.f.jpg","4.1.f",
                "A co ja fizyk teoretyczny?",
                ":(",":(",":(",
                on_finish=lambda:self.defeat()),
            "4.2.f":Q("1.2.f.jpg","4.2.f",
                "Hmm.. Łzy współczucia to jedyny sposób w którym umiesz sprawić żebym była morka?.png","4.2f",
                ":(",":(",":(",
                on_finish=lambda:self.defeat()),
            "4.3.f":Q("2.jpg","4.3.f",
                "<Rumieni się> O rany, to gdzie ta Twoja stajnia? Może tam pojedziemy?",
                "OK","OK","OK",
                a_key="ok",b_key="ok",c_key="ok"),
            "ok":Q("ok.jpg", "horse",    
                "Akcja przenosi się do stajni",
                "...","...","...",
                a_key="15",b_key="15",c_key="15"),
            "5":Q("5.jpg","5",
                "W sumie to nie wiem <dzwoni do mamy>",
                "Zapytaj, czy ma czas wieczorem",
                "Rany, gadasz tyle że zdążyłbym skończyć",
                "Czekaj pomogę Ci, daj numer",
                a_key="5.1.f",b_key="5.1.f"),
            "5.1.f":Q("1.2.f.jpg","5.1.f",
                "EWWW zboczeniec!",
                ":(",":(",":(",
                on_finish=lambda:self.defeat()),
            "6":Q("1.jpg","6",
                "Hmm.. a co, przyszedłeś aplikować?",
                "Jestem gotowy na egzamin ustny!",
                "Pije Harnasia w 15 sekund!",
                "Jak trzeba nawet matkę mogę robić!",
                a_key="7",b_key="8",c_key="5.1.f"),
            "7":Q("1.jpg","7",
                "Ok.. to na jakiej częstotliwości są fale 5G?",
                "Wtf woman?",
                "3600-3800 MHz",
                "2.5 GHz",
                a_key="7.1.f",b_key="9",c_key="7.2.f"),
            "7.1.f":Q("1.2.f.jpg","7.1.f",
                "Rozmowę uznaję za zakończoną, dziękujemy Panu, oddzwonimy",
                "<nigdy nie oddzwania>",
                "<nigdy nie oddzwania>",
                "<nigdy nie oddzwania>",
                on_finish=lambda:self.defeat()),
            "7.3.f":Q("1.2.f.jpg","7.3.f",
                "To mikrofale, zupełnie jak Twój mikropenis!",
                ":(",":(",":(",
                on_finish=lambda:self.defeat()),
            "8":Q("6.jpg","8",
                "Ja piję w 14! <dziki śmiech>",
                "<wyzwij na pojedynek>",
                "<podziekuj>",
                "<odejdz, przegrany>",
                a_key = "8.1",b_key="8.f",c_key="8.f"),
            "8.f":Q("road.jpg","defeat",                 ##
                "<Odeszłeś. Czego się spodziewałeś?>",
                ":(",":(",":(",
                on_finish=lambda:self.defeat()),
            "8.1":Q("7.1.jpg","beer",                 ##
                "*Przegrywasz*",
                "Rewanż?",
                "<Odejdź>",
                "<Udaj, że odchodzisz, może za Tobą pobiegnie>",
                a_key="8.2",b_key="8.f",c_key="8.f"),
            "8.2":Q("7.2.jpg","beer",                 ##
                "*Przegrywasz*",
                "Do 3 razy sztuka?",
                "<Odejdź>",
                "<Udaj, że odchodzisz, może za Tobą pobiegnie>",
                a_key="8.3",b_key="8.f",c_key="8.f"),    
            "8.3":Q("7.3.jpg","beer",                 ##
                "*Przegrywasz*",
                "To może do 4?",
                "<Odejdź>",
                "<Pijany> Następnym razem bym Cię pokonał, ale nie chciałabyś może pouprawiać seksu?",
                a_key="8.3.1",b_key="8.f",c_key="8.3.f"),     
            "8.3.1":Q("8.jpg","drunk",                ##   
                "*Tracisz przytomność od nadmiaru Harnasia",
                ":(",":(",":(",
                on_finish=lambda:self.defeat()),
            "8.3.f":Q("9.jpg","8.3.f",
                "<Pijana> W sumie OK, szykuj dupę <Kręci Cię to?>",
                "Oj Tak!",
                "Nie!",
                "Fork no!",
                a_key = "8.3.f.1",b_key="8.3.f.2",c_key="8.3.f.2"),
            "8.3.f.1":Q("black.jpg","butt",                ##   
                "Musisz być teraz zadowolony z siebie",
                ":')",":')",":')",
                on_finish=lambda:self.defeat()),
            "8.3.f.2":Q("black.jpg","butt",                ##   
                "<Po pijaku za wolno biegłeś, przykro mi :(>",
                ":'(",":'(",":'(",
                on_finish=lambda:self.defeat()),
            "9":Q("10.jpg","9",
                "Rany, spokojnie, żartowałam, co za kujon...",
                "No cóż, wiesz już że mam duży mózg... chcesz zobaczyć co jeszcze?",
                "Ej... SAMA JESTEŚ KUJON :(",
                "I take you very srsly bbe",
                a_key="10",b_key="9.2.f",c_key="9.1.f"),
            "9.1.f":Q("11.jpg","9.1.f",                ##   
                "... <Facepalm>",
                ":'(",":'(",":'(",
                on_finish=lambda:self.defeat()), 
            "9.2.f":Q("11.2.jpg","9.2.f",                ##   
                "Tak? To teraz Ty jeszcze będziesz sam <Odchodzi>",
                ":'(",":'(",":'(",
                on_finish=lambda:self.defeat()), 
            "10":Q("1.jpg","10",
                "Hmm.. Cóź, chyba będę musiała Cię przyjąć na okres próbny. Zaczynasz od zaraz",
                "Pokazać Ci mojego ogiera?",
                "Czy przwidziane są jakieś wyjazdy służbowe?",
                "Jakie jest moje pierwsze zadanie?",
                a_key="15",b_key="10.2.f",c_key="11"),
            "10.2.f":Q("house.jpg","10.2.f",
                "Tak.. <zabiera Cię do domu>",
                "Yeah!!!",
                "Wooo hooo!!",
                "Now that's progress!!!!",
                a_key="12",b_key="12",c_key="12"),
            "11":Q("10.jpg","11",
                "A jakie masz propozycje?",
                "Mogę być Twoim szoferem w drodze do domu",
                "Mogę wyczyścić Ci garnczek",
                "Mogę Ci wytłumaczyć, dlaczego Linux to najlpeszy system operacyjny, każdy powinien go używać",
                a_key="12",b_key="11.2.f",c_key="11.3.f"),
            "11.2.f":Q("2.jpg","11.2.f",
                "W sumie to dobry pomysł, chyba zostawiłam go w stajni",
                "Wait wha-",
                "No, to idealnie, idziemy",
                "Nie o ten garnczek mi chodził-",
                a_key="15",b_key="15",c_key="15"),
            "11.3.f":Q("1.2.f.jpg","11.3.f",
                "Mam alergię na pingwiny!",
                ":(",":(",":(",
                on_finish=lambda:self.defeat()),
            "12":Q("12.jpg","12",
                "<U niej w domu, przedsionek> Mmmm... i jak wrażenia?",
                "Dobre, pomarańczowe",
                "Podoba mi się... widok",
                "<Poprawiając wąs> Dobra, Graża, pokaż mi mój fotel i zrób mi kanapkę",
                a_key="12.1",b_key="13",c_key="12.2"),
            "12.1":Q("12.f.jpg","door_slam",            ##
                "<Wyrzuca Cię z domu>",
                ":(",":(",":(",
                on_finish = lambda:self.defeat()),
            "12.2":Q("12.2.jpg","kiepscy",              ##
                "Zasiadasz na tronie,a ona do końca życia robi Ci kanapki. Nie jest to zakończenie na jakie liczyłeś, ale to relacja z której jesteś szczęśliwy",
                ":')",":')",":')",
                on_finish= lambda:self.defeat()),
            "13": Q("13.jpg","13",
                "Podoba Ci się coś... poza widokiem?",
                "Bardzo ładna kurtka",
                "Bardzo ładna kurtka",
                "Bardzo ładna kurtka",
                a_key="13.f",b_key="14",c_key="13.f"),
            "13.f":Q("12.f.jpg","defeat",
                "<Tym razem nie zabrzmiałeś wystarczająco męsko. Sory :(",
                ":(",":(",":(",
                on_finish=lambda:self.defeat()),
            "14":Q("14.jpg","14",
                "Aż tak zabiera Twoją uwagę? W takim razie może ją zdejmę...",
                "Nie jeśli ja zrobię to pierwszy! <Przyciskasz ją do ściany i próbujesz włożyć marcheweczkę do jej garnczka>",
                "Może od razu całą resztę? <wink> <wink>",
                "Nie.",
                a_key="14.3.f",b_key="14.3.f.f",c_key="14.1.f"),
            "14.3.f":Q("14.f.jpg", "14.3.f",
                "Ohh.. Bierz mnie tygrysie! Ahh Ahhhh",
                "OMG did I do it?",
                "Oł yeahhhh bbyyy",
                "That's right!",
                on_finish = lambda:self.victory()),
            "14.3.f.f":Q("black.jpg","silence",
                "Rozczarowałeś nas Michał.",
                "...","...","...",
                a_key="15",b_key="15",c_key="15"),
            "14.1.f":Q("14.2.jpg","14.3.f",
                "No to nie.",
                ":(",":(",":(",
                on_finish = lambda:self.defeat()),
            "15":Q("15.jpg","15",
                "<Akcja przenosi się do stajni> I co teraz?",
                "Zagwiżdż na konia",
                "Przynieś garnczek",
                "Mnie się pytasz? Co ja tu w ogóle robię?",
                a_key = "16",b_key="17",c_key="15.1.f"),
            "15.1.f":Q("15.1.jpg","15.1.f",
                "Phi, nie lubię niezdecydowanych mężczyzn!",
                ":(",":(",":(",
                on_finish = lambda:self.defeat()),
            "16":Q("16.jpg","16",
                "No i gdzie ten ogier?",
                "Musimy poczekać aż przybiegnie, ale mam chyba pomysł co możemy w tym czasie robić <wink> <wink>",
                "Right here... <rozbierasz się>",
                "Konia nie ma, ale patrz co znalazłem!",
                a_key ="16.3.f",b_key="16.1.f",c_key="17"),
            "16.1.f":Q("16.1.jpg","16.1.f",
                "Chciałam prawdziwego ogiera a nie miniaturkę :(",
                ":(",":(",":(",
                on_finish = lambda:self.defeat()),
            "16.3.f":Q("17.jpg", "16.3.f",
                "Nie mówisz chyba o....",
                "Oj tak",
                "Oł yeah",
                "O tak bby",
                a_key = "17",b_key="17",c_key="17"),
            "17":Q("17.0.jpg","17",
                "<Zaczynasz pucować garnczek> O ale wypucowany!",
                "Ciebie też tak mogę",
                "Jestem w tym dobry, wiesz w czym też jestem dobry?",
                "Wkładasz do niego penisa",
                a_key = "17.1.f",b_key="17.2.f",c_key="18"),
            "17.1.f":Q("17.1.jpg", "17.1.f",
                "Ja Ciebie też",
                "Wut?",
                "What?",
                "Whaa?",
                a_key="17.1.f.f",b_key="17.1.f.f",c_key="17.1.f.f"),
            "17.1.f.f":Q("17.1.1.jpg", "17.1.f.f",
                "<Wyciąga penisa. Za późno>",
                ":'(",":'(",":'(",
                on_finish=lambda:self.defeat()),
            "17.2.f":Q("17.2.jpg","17.2.f",
                "W czym?",
                "We wsadzaniu do niego penisa",
                "We wsadzaniu do niego bolca",
                "We wsadzaniu do niego parówki",
                a_key="18",b_key="18",c_key="18"),
            "18":Q("18.jpg","18",
                "<Wsadzasz do niego penisa> What the fuck?!",
                "Co zazdrosna?",
                "Idk, Piotrek powiedział że to dobra metoda",
                "A co, też chcesz?",
                a_key="18.1.f",b_key="19",c_key="18.2.f"),
            "18.1.f":Q("18.1.jpg","18.1.f",
                "Nie, tylko ten garnczek lubi większe",
                "A skąd wiesz?",
                "What? Skąd wiesz?",
                "Co? Skąd wiesz?",
                a_key = "18.1.f.f",b_key="18.1.f.f",c_key="18.1.f.f"),
            "18.1.f.f":Q("18.2.jpg","18.1.f.f",
                "Bo wkładałam tam już wiele razy swojego",
                "Hol' up",
                "Wait WHAT",
                "Czy ja się przesłyszałem?",
                a_key="18.2.f.f",b_key="18.2.f.f",c_key="18.2.f.f"),
            "18.2.f":Q("18.2.jpg","18.2.f",
                "Tak",
                "Well then...",
                "Soo....",
                "Then...",
                a_key = "18.2.f.f",b_key="18.2.f.f",c_key="18.2.f.f"),
            "18.2.f.f":Q("18.f.jpg","garnek",
                "<Wkłada swojego penisa do garnka>",
                ":o",":o",":o",
                on_finish = lambda:self.defeat()),
            "19":Q("19.jpg","19",
                "No skoro tak powiedział... <Namiętne pocałunki>",
                "A wiesz, że mam Jacht?",
                "Jakim sposobem to zadziałało?",
                "Mmmm.. całujesz jak Twoja matka, a bardzo ją lubię",
                a_key="19.1.f",b_key="19.2.f",c_key="19.3.f"),
            "19.1.f":Q("15.1.jpg","19.1.f",
                "No i zruinowałeś ten moment :(",
                ":(",":(",":(",
                on_finish=lambda:self.defeat()),
            "19.2.f":Q("19.1.jpg","19.2.f",
                "Masz rację, to nie mogło być takie proste.",
                ":(",":(",":(",
                on_finish=lambda:self.defeat()),
            "19.3.f":Q("19.2.jpg","19.3.f",
                "Dzięku.. wait WHAT?",
                ":(",":(",":(",
                on_finish=lambda:self.defeat())
        }, "hi")
        
        self.starting_objects = [
            Portal(0,0,levelData),
            Door("yellow", 1200+50, 0, levelData),
            Floor(levelData),
            CustomObject(PATH.obj+"rainbow_flower.png",100,0,levelData),
            CustomObject(PATH.obj+"tree.png", 480, 0, self.levelData),
            self.quiz,
            self.question,
            self.a,
            self.b,
            self.c,
            
            #CustomObject(PATH.obj+"flower_2.png", 450, -50, self.levelData),

            #CustomObject(PATH.obj+"flower_2.png", 1100, -50, self.levelData),
        ]
        self.stageOne()
        self.loadProgress()
        self.levelData.popup_manager.add(Cloud(
            textArray = ["Jeśli jest jeden poziom, na który najbardziej opłacało się czekać, to właśnie ten.",
            "Masz jedyną niewyobrażalną okazję...",
            "Zaliczyć Wojcia!",
            "Yay!",
            "Can you dominate this beautiful girly fella, Michał-senpai?",
            "<Podgłoś dźwięk, nagrania od Wojcia są mega ciche for some reason"], 
            personArray = ["peter","peter","peter","wojciu","peter","peter"], 
            levelData = self.levelData,),
        )

        if self.levelData.im_cheating: self.open()

    def stageOne(self):
        self.objects = self.starting_objects.copy()
        
    def doNextStep(self):        
        return False

    def victory(self):
        self.open()
        s = pygame.mixer.Sound(SOUNDS.yellow+"careless bork.mp3")
        s.set_volume(0.5)
        self.levelData.channel0.play(s)

    def defeat(self):
        self.levelData.popup_manager.add(DarkSouls(self.levelData, onFinish=lambda:self.reset()))
        pass



class YellowLevelThree(Level):
    def __init__(self, levelData):
        super().__init__(levelData)
        self.levelData.level_len = TILE.w*16
        self.levelData.background = "romeo.jpg"

        self.textBox = TextInput(1200+50, 360, self.levelData, key = "SorbetSiusiakowyJemSobie", max_len = 24, size = (600,50))
        self.starting_objects = [
            Portal(-10,0,levelData),
            Door("yellow", 1200+200, 0, levelData),
            CustomObject(PATH.obj+"sign_4.png",750,0,levelData),
            CustomObject(PATH.obj+"sign_4.png",900,0,levelData),
            Board(1185+50,350, levelData, size = (660, 70)),
            self.textBox
        ]
        self.stageOne()
        self.loadProgress()

        s = pygame.mixer.Sound(SOUNDS.yellow+"romeo.mp3")
        s.set_volume(0.5)
        self.levelData.channel0.play(s)

        self.levelData.popup_manager.add(Cloud(
            textArray = [
            "Uuuu... jak romantycznie!",
            "Michałku! Czas na literaturę piękną.",
            "A że nikt nie lubi w tych czasach czytać...",
            "Przygotowałem coś specjalnie dla Ciebie!",
            "Słuchaj uważnie.. ukryte albowiem jest hasło, co zaś powinno pozwolić przejśc dalej.",
            "<Jeśli nie otworzyło się teraz okienko z filmem, zajrzyj do folderu 'mov', masz pozwolenie>"], 
            personArray = ["wojciu","wojciu","peter","wojciu","peter","peter"], 
            levelData = levelData,),
        )

        if levelData.im_cheating: self.open()

    def stageOne(self):
        self.objects = self.starting_objects.copy()

    def doNextStep(self):
        if self.textBox.isCorrect():
            self.levelData.popup_manager.add(Trophy(self.levelData, onFinish=lambda:self.open())) 


class YellowLevelFour(Level):
    def __init__(self, levelData):
        super().__init__(levelData)
        self.levelData.level_len = TILE.w*16
        self.levelData.background = "recording_studio.jpg"

        self.textBox = TextInput(1200+200, 360, self.levelData, key = "sexy voice")
        self.starting_objects = [
            Portal(0,0,levelData),
            Door("yellow", 1200+200, 0, levelData),
            Board(1185+200,350, levelData, size = (330, 70)),
            self.textBox
        ]
        self.stageOne()
        self.loadProgress()

        s = pygame.mixer.Sound(SOUNDS.yellow+"studio.mp3")
        s.set_volume(0.5)
        self.levelData.channel0.play(s)

        self.levelData.popup_manager.add(Cloud(
            textArray = [
            "A teraz czas na kolejny challenge!",
            "Napisz do nas wiadomość, a powiemy Ci co masz nagrać na dyktafonie.",
            "Prześlij nam nagranie, a dostaniesz kod do pójścia dalej ;P"], 
            personArray = ["peter","wojciu","peter"], 
            levelData = levelData,),
        )

        if levelData.im_cheating: self.open()

    def stageOne(self):
        self.objects = self.starting_objects.copy()

    def doNextStep(self):
        if self.textBox.isCorrect():
            self.levelData.popup_manager.add(Trophy(self.levelData, onFinish=lambda:self.open())) 




class YellowLevelFive(Level):
    def __init__(self, levelData):
        super().__init__(levelData)
        self.levelData.background = "main.png"
        self.levelData.level_len = TILE.w*10

        self.starting_objects = [
            Floor(levelData),
            Portal(200,0,levelData),
            CustomObject(PATH.obj+"tree.png", 700, -5, self.levelData),
            Crystal("yellow",800,0,levelData),
            CustomObject(PATH.obj+"bush_2.png", 370, 0, self.levelData),
        ]
        self.stageOne()
        self.loadProgress()

        s = pygame.mixer.Sound(SOUNDS.f+"yayy.mp3")
        s.set_volume(0.5)
        self.levelData.channel0.play(s)

        self.levelData.popup_manager.add(Cloud(
            textArray = [
            "Hooray! Żółty kryształ jest Twój!"], 
            personArray = ["peter"], 
            levelData = levelData,),
        )

        if levelData.im_cheating: self.open()

    def stageOne(self):
        self.objects = self.starting_objects.copy()