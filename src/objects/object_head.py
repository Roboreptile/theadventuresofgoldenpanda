import pygame
from src.main_const import *
from src.objects.object_throwable import Throwable
from src.popups.popup_heart import Heart
from src.popups.popup_thunder1 import Thunder1
from src.popups.popup_thunder2 import Thunder2
from src.popups.popup_thunder3 import Thunder3
from src.popups.popup_spark1 import Spark1
from src.popups.popup_spark2 import Spark2
from src.popups.popup_spark3 import Spark3
from random import randint
from typing import Callable

head_images = [] 
x_coords  = [1,SCREEN.w-1]
y_coords  = [SCREEN.h//8, SCREEN.h//8]

class Head(Throwable):
    def __init__(self, levelData, stop_func: Callable[[], None] = None):
        global head_images, x_coords, y_coords
        idx = randint(0,3)

        pos_idx = randint(0,1)
        speed_idx = randint(2,3)
        speed = 9 * speed_idx

        x_speed = 18
        if(pos_idx==1): x_speed = -x_speed

        random_angle = randint(1,3)
        if(pos_idx==1): random_angle = -random_angle

        super().__init__('',x_coords[pos_idx],y_coords[pos_idx],x_speed,speed, levelData, False, "Head", head_images[idx],random_angle)
        self.is_vlad = False
        if(idx==0): self.is_vlad = True
        self.s = pygame.mixer.Sound(SOUNDS.orange + "dj stop.mp3")
        self.s.set_volume(0.5)
        self.stop_func = stop_func

    def update(self):
        super().update()
        if self.levelData.events != None: self.handleEvents()

        if(not self.is_vlad and self.rect.center[1]>SCREEN.h): self.stop_func()

    def handleEvents(self):
        for event in self.levelData.events:
            if event.type == pygame.MOUSEBUTTONDOWN and self.rect.collidepoint(event.pos):
                if(self.is_vlad):
                    self.stop_func()
                    self.levelData.channel0.play(self.s)
                else:
                    self.levelData.level.score[0] += 1
                    x = randint(0,5)
                    if(x == 0):   self.levelData.popup_manager.add(Spark1(self.levelData))
                    elif(x == 1): self.levelData.popup_manager.add(Spark2(self.levelData))
                    elif(x == 2): self.levelData.popup_manager.add(Spark3(self.levelData))
                    elif(x == 3): self.levelData.popup_manager.add(Thunder1(self.levelData))
                    elif(x == 4): self.levelData.popup_manager.add(Thunder2(self.levelData))
                    else:         self.levelData.popup_manager.add(Thunder3(self.levelData))

                self.active = False
                
    @staticmethod
    def pre_init():
        global head_images 
        if(len(head_images)!=0): return
        head_images = [0]*4
        head_images[0] = pygame.image.load(PATH.char+"vlad.png").convert_alpha()
        head_images[1] = pygame.image.load(PATH.char+"peter.png").convert_alpha()
        head_images[2] = pygame.image.load(PATH.char+"wojciu.png").convert_alpha()
        head_images[3] = pygame.image.load(PATH.char+"weronika.png").convert_alpha()

        for i in range(0,4):
            head_images[i] = pygame.transform.scale(head_images[i], (200,200))
