from src.objects.object_base import CustomObject
from src.levels.level_factory_data import LevelFactoryData
from src.main_const import *
import pygame

class BossButton(CustomObject):
    def __init__(self, type, x, y, levelData:LevelFactoryData, onClick):
        super().__init__(PATH.boss+type+".png", x, y, levelData)
        self.c = onClick
        self.charge = 0
        self.type = 0
        if(type == "charge_0"):
            self.type = 1
            self.images = [0]*7
            for i in range(1,8):
                self.images[i-1] = pygame.image.load(PATH.boss+"charge_"+str(i)+".png").convert_alpha()

    def update(self):
        super().update()
        if(self.levelData.events!=None):
            for event in self.levelData.events:
                if event.type == pygame.MOUSEBUTTONDOWN:
                        if self.rect.collidepoint(event.pos):
                            if(self.type == 1):
                                if(self.c() == True):
                                    self.charge += 1
                                    if(self.charge>7): self.charge = 7
                                    self.image = self.images[self.charge-1]
                            else:
                                self.c()