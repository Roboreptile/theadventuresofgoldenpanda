import pygame
from src.popups.popup_plus import PopupPlus
from src.main_const import *

lolol_array = []

class Lolol(PopupPlus):
    def __init__(self, levelData, onFinish = None, change_every_x_frames = 8):
        global lolol_array
        super().__init__(onFinish, lolol_array, levelData, change_every_x_frames)

        self.levelData.channel3.play(pygame.mixer.Sound(SOUNDS.f+"yayy.mp3"))

    @staticmethod
    def pre_init():
        global lolol_array
        lolol_array = PopupPlus.loadGIFFolder("lolol")