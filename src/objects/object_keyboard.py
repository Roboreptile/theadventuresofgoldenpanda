import pygame
from random import randint
from typing import List


from src.objects.object_base import CustomObject
from src.main_const import *
from src.popups.popup_trophy import Trophy
from src.popups.popup_over import GameOver
from src.objects.object_keyboard_button import Key

class KeyBoard(CustomObject):
    def __init__(self,x,y,levelData,keys: List[Key]):
        super().__init__(PATH.keys+"keyboard.jpg",x,y,levelData, scale = (600,180))
        self.hihi = ["a","adash","b","c","cdash","d","e","edash","f","g",
                    "h","i","j","k","l","m","n","o","p","r",
                    "s","sdash","t","u","v","w","x","y","z","zdash"]

        self.hihiHaha = ["a","b","c","chouse","cdash","d","dz","ddash","e","f",
                        "g","h","i","j","k","l","lj","m","n","nj",
                        "o","p","r","s","shouse","t","u","v","z","zhouse"]

        self.activeHihi = self.hihi
        self.hihiIdx = 0

        self.stage = 1
        self.keys = keys

        self.shuffle()

    def shuffle(self):
        for _ in range(100):
            obj1 = self.keys[randint(0,len(self.keys)-1)]
            obj2 = self.keys[randint(0,len(self.keys)-1)]
            obj2_rect = obj2.rect_base
            obj2.rect_base = obj1.rect_base
            obj1.rect_base = obj2_rect

    def restart(self):
        self.stage = 1
        self.levelData.popup_manager.add(GameOver(self.levelData))
        self.activeHihi = self.hihi
        self.hihiIdx = 0

        for key in self.keys:
            key.reset()

    def update(self):
        super().update()
        if self.levelData.events != None: self.handleEvents()
    
    def handleEvents(self):
        for event in self.levelData.events:
            if event.type == pygame.MOUSEBUTTONDOWN:
                for key in self.keys:
                    if key.rect.collidepoint(event.pos):
                        print(key.name+" should be: "+self.activeHihi[self.hihiIdx])
                        if(key.didHeMessUp(self.activeHihi[self.hihiIdx])):
                            self.restart()
                        else:
                            self.hihiIdx+=1
                            if(self.hihiIdx==len(self.activeHihi)):
                                if(self.stage == 1):
                                    self.stage = 2
                                    self.hihiIdx = 0
                                    self.activeHihi = self.hihiHaha
                                    for key in self.keys:
                                        key.makeItHarder()
                                else:
                                    self.levelData.popup_manager.add(Trophy(self.levelData))
                                    self.levelData.level.open()
                                    self.restart()
                        self.shuffle()
                        return


