from matplotlib.pyplot import sca
import pygame
from random import randint
from src.objects.object_base import CustomObject
from src.main_const import *

class Moo(CustomObject):
    def __init__(self, name, shared,x,y, levelData):
        super().__init__(PATH.obj+"red.png",x,y,levelData, scale=(250,250))
        self.active = pygame.image.load(PATH.obj + "green.png").convert_alpha()
        self.active = pygame.transform.scale(self.active, (250,250))


        self.cow  = pygame.image.load(PATH.moo + "cow "+str(name)+".png").convert_alpha()
        self.cow = pygame.transform.scale(self.cow, (210,210))

        self.image_base.blit(self.cow, (20,20))
        self.image = self.image_base

        self.active.blit(self.cow, (20,20))

        self.sounds = [
            pygame.mixer.Sound(SOUNDS.navy+"cow 1.mp3"),
            pygame.mixer.Sound(SOUNDS.navy+"cow 2.mp3")
            ]

        self.shared = shared
        self.correct = False
        self.clicked = False
        self.name = name

    def update(self):
        super().update()

        if(not self.clicked and self.levelData.events!=None):
            for event in self.levelData.events:
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if self.rect.collidepoint(event.pos):
                        self.levelData.channel1.play(self.sounds[randint(0,1)])
                        if(self.shared[0] == self.name):
                            self.correct = True
                        self.clicked = True
                        self.image = self.active
                        self.shared[0]+=1

    def reset(self):
        self.clicked = False
        self.image = self.image_base
        self.correct = False