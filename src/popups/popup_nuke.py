import pygame
from src.popups.popup_boss import PopupBoss
from src.main_const import *


nuke_boss_arr = []

class Nuke(PopupBoss):
    def __init__(self, levelData, onFinish = None):
        global nuke_boss_arr
        super().__init__(levelData, None, onFinish, nuke_boss_arr, 2, True)

    @staticmethod
    def pre_init():
        global nuke_boss_arr
        nuke_boss_arr = PopupBoss.loadGIFFolder("nuke", scale = (SCREEN.w,SCREEN.h))

