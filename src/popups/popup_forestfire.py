import pygame
from src.popups.popup_boss import PopupBoss
from src.main_const import *


forestfire_boss_arr = []

class ForestFire(PopupBoss):
    def __init__(self, levelData, onFinish = None):
        global forestfire_boss_arr
        super().__init__(levelData, None, onFinish, forestfire_boss_arr, 4, True)

    @staticmethod
    def pre_init():
        global forestfire_boss_arr
        forestfire_boss_arr = PopupBoss.loadGIFFolder("forest_fire", scale = (SCREEN.w,SCREEN.h))

