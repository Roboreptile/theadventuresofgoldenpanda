import pygame
from src.main_const import *
from src.levels.level_base import Level
from src.objects.object_base import CustomObject
from src.objects.object_floor import Floor
from src.objects.object_portal import Portal
from src.objects.object_textinput import TextInput
from src.objects.object_board import Board
from src.objects.object_door import Door
from src.objects.object_throwable import Throwable
from src.objects.object_basket import Basket
from src.objects.object_seaweed import SeaWeed
from src.objects.object_queue import Queue
from src.popups.popup_trophy import Trophy
from src.popups.popup_over import GameOver
from src.objects.object_keyboard import KeyBoard
from src.objects.object_keyboard_button import Key
from src.objects.object_genie import Genie
from src.popups.popup_genie_cloud import GenieCloud
from src.objects.object_luckyblock import LuckyBlock
from src.objects.object_mushroom import Mushroom
from src.objects.object_repeater import Repeater
from src.popups.popup_cloud import Cloud
from src.popups.popup_light import Light
from src.popups.popup_spark2 import Spark2
from src.objects.object_crystal import Crystal



class GreenLevelOne(Level):
    def __init__(self, levelData):
        super().__init__(levelData)
        self.levelData.level_len = TILE.w*13
        self.levelData.background = "tiger.jfif"

        self.textBox = TextInput(1200+200, 360, self.levelData, key = "31143")
        self.starting_objects = [
            Portal(0,0,levelData),
            Door("green", 1200+200, 0, levelData),
            CustomObject(PATH.obj+"rainbow_flower.png",100,0,levelData),
            CustomObject(PATH.obj+"sign_tiger.png",600,0,levelData),

            Board(1185+200,350, levelData, size = (330, 70)),
            self.textBox
        ]
        self.stageOne()
        self.loadProgress()

        s = pygame.mixer.Sound(SOUNDS.green+"jungle.mp3")
        s.set_volume(0.5)
        self.levelData.channel0.play(s)

        self.levelData.popup_manager.add(Cloud(
            textArray = [
            "Witaj w poziomach zielonych - specjalość twórcy tej gry, szanownej krówci!",
            "Te poziomy są zdecydowanie dużo trudniejsze niż pozostałe, i mogą zająć masę czasu.",
            "Ale rozwiązania zagadek są cudowne! Także polecam 10/10."], 
            personArray = ["peter","peter","peter"], 
            levelData = levelData,),
        )

        if levelData.im_cheating: self.open()

    def stageOne(self):
        self.objects = self.starting_objects.copy()

    def doNextStep(self):
        if self.textBox.isCorrect():
            self.levelData.popup_manager.add(Trophy(self.levelData, onFinish=lambda:self.open())) 


class GreenLevelTwo(Level):
    def __init__(self, levelData):
        super().__init__(levelData)
        self.levelData.level_len = TILE.w*15
        self.levelData.background = "dark.png"

        self.textBox = TextInput(1200+200, 360, self.levelData, key = "night5")
        self.starting_objects = [
            Portal(0,0,levelData),
            Door("green", 1200+200, 0, levelData),
            Repeater(60,self.levelData,lambda:self.weirdFlashes()),
            #Repeater(47,self.levelData,lambda:self.weirdSparks()),
            Board(1185+200,350, levelData, size = (330, 70)),
            self.textBox
        ]
        self.stageOne()
        self.loadProgress()

        s = pygame.mixer.Sound(SOUNDS.green+"dark.mp3")
        s.set_volume(0.5)
        self.levelData.channel0.play(s)

        self.levelData.popup_manager.add(Cloud(
            textArray = [
            "Oj.. ciemno tu.",
            "I jakieś śmieszne świecidełko na środku. Tylko coś słabo mruga.",
            "Szkoda."], 
            personArray = ["peter","peter","peter"], 
            levelData = levelData,),
        )

        if levelData.im_cheating: self.open()

    def stageOne(self):
        self.objects = self.starting_objects.copy()

    def doNextStep(self):
        if self.textBox.isCorrect():
            self.levelData.popup_manager.add(Trophy(self.levelData, onFinish=lambda:self.open())) 

    def weirdFlashes(self):
        self.levelData.popup_manager.add(Light(self.levelData))
    def weirdSparks(self):
        self.levelData.popup_manager.add(Spark2(self.levelData))


class GreenLevelThree(Level):
    def __init__(self, levelData):
        super().__init__(levelData)
        self.levelData.level_len = TILE.w * 13
        self.levelData.background = "mario.jpg"
        self.mushroom = Mushroom(800, 450, 5, 15, levelData)

        self.starting_objects = [
            Portal(10, 0, levelData),
            Door("green", 1200, 0, levelData, scale=(600, 600)),
            LuckyBlock(800, 350, self.mushroom, levelData),
            self.mushroom,
        ]
        self.stageOne()
        self.loadProgress()

        s = pygame.mixer.Sound(SOUNDS.green+"mario.mp3")
        s.set_volume(0.5)
        self.levelData.channel0.play(s)

        self.levelData.popup_manager.add(Cloud(
            textArray = [
            "To zdecydowanie najtrudniejszy poziom ze wszystkich.",
            "Gdy zapomniałem rozwiązania, przechodziłem go sam z dobre kilka dni.",
            "Good luck!"], 
            personArray = ["peter","peter","peter"], 
            levelData = levelData,),
        )

    def stageOne(self):
        self.objects = self.starting_objects.copy()

    def enterSuccess(self, obj, pos):
        self.levelData.player.shrink()
        super().enterSuccess(obj, pos)

    def reset(self):
        self.levelData.player.shrink()
        super().reset()

class GreenLevelFour(Level):
    def __init__(self, levelData):
        super().__init__(levelData)
        self.levelData.level_len = TILE.w*13
        self.levelData.background = "rainbow_flowers.png"

        self.textBox = TextInput(1200+200, 360, self.levelData, key = "PARÓWKA")
        self.starting_objects = [
            Portal(0,0,levelData),
            Door("green", 1200+200, 0, levelData),
            CustomObject(PATH.obj+"sign_rainbow.png",700,0,levelData),
            Board(1185+200,350, levelData, size = (330, 70)),
            CustomObject(PATH.obj+"rainbow_flower.png",200,0,levelData),
            CustomObject(PATH.obj+"rainbow_flower.png",300,0,levelData),
            CustomObject(PATH.obj+"rainbow_flower.png",400,0,levelData),
            CustomObject(PATH.obj+"rainbow_flower.png",500,0,levelData),
            CustomObject(PATH.obj+"rainbow_flower.png",600,0,levelData),
            CustomObject(PATH.obj+"rainbow_flower.png",700,0,levelData),
            CustomObject(PATH.obj+"rainbow_flower.png",800,0,levelData),
            CustomObject(PATH.obj+"rainbow_flower.png",900,0,levelData),
            CustomObject(PATH.obj+"rainbow_flower.png",1000,0,levelData),
            CustomObject(PATH.obj+"rainbow_flower.png",1100,0,levelData),
            CustomObject(PATH.obj+"rainbow_flower.png",1200,0,levelData),
            CustomObject(PATH.obj+"rainbow_flower.png",1300,0,levelData),
            CustomObject(PATH.obj+"rainbow_flower.png",1400,0,levelData),

            self.textBox
        ]
        self.stageOne()
        self.loadProgress()

        s = pygame.mixer.Sound(SOUNDS.green+"magic.mp3")
        s.set_volume(0.5)
        self.levelData.channel0.play(s)

        self.levelData.popup_manager.add(Cloud(
            textArray = [
            "I hope you enjoyed that break from hard tasks.",
            "Bo teraz czas na faktyczną najtrudniejszą zagadkę.",
            "Nie jestem w stanie stwierdzić, czy sam ją rozwiążesz. Jeśli tak - to szacun.",
            "Good luck... you will need it!"], 
            personArray = ["peter","peter","peter","peter"], 
            levelData = levelData,),
        )

        if levelData.im_cheating: self.open()

    def stageOne(self):
        self.objects = self.starting_objects.copy()

    def doNextStep(self):
        if self.textBox.isCorrect():
            self.levelData.popup_manager.add(Trophy(self.levelData, onFinish=lambda:self.open())) 


class GreenLevelFive(Level):
    def __init__(self, levelData):
        super().__init__(levelData)
        self.levelData.background = "main.png"
        self.levelData.level_len = TILE.w*10

        self.starting_objects = [
            Floor(levelData),
            Portal(200,0,levelData),
            Crystal("green",800,0,levelData),
            CustomObject(PATH.obj+"bush_1.png",-100,0,levelData),
            CustomObject(PATH.obj+"tree.png",380,0,levelData),

            CustomObject(PATH.obj+"rainbow_flower.png",300,0,levelData),
            CustomObject(PATH.obj+"rainbow_flower.png",500,0,levelData),
            CustomObject(PATH.obj+"rainbow_flower.png",700,0,levelData),
            
        ]
        self.stageOne()
        self.loadProgress()
        s = pygame.mixer.Sound(SOUNDS.f+"yayy.mp3")
        s.set_volume(0.5)
        self.levelData.channel0.play(s)

        self.levelData.popup_manager.add(Cloud(
            textArray = [
            "At long last... zielony kryształ",
            "Zgaduję, że już przeszedłeś inne poziomy, więc well...",
            "Pewnie spodziewałeś się że coś się stanie. A tu nic.",
            "Hmm.",
            "Hmmmmmmmmmmmmmmmmmm...........",
            "Well then. Szukaj."], 
            personArray = ["peter","peter","peter","peter","peter","peter"], 
            levelData = levelData,),
        )

        if levelData.im_cheating: self.open()

    def stageOne(self):
        self.objects = self.starting_objects.copy()