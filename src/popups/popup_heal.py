import pygame
from src.popups.popup_boss import PopupBoss
from src.main_const import *


heal_boss_arr = []

class Heal(PopupBoss):
    def __init__(self, levelData, pos, onFinish = None):
        global heal_boss_arr
        super().__init__(levelData, pos, onFinish, heal_boss_arr, 2, True)

    @staticmethod
    def pre_init():
        global heal_boss_arr
        heal_boss_arr = PopupBoss.loadGIFFolder("heal", scale = (300,300))

