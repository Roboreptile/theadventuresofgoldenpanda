import pygame
from src.popups.popup_boss import PopupBoss
from src.main_const import *


lasercrit_boss_arr = []

class LaserCrit(PopupBoss):
    def __init__(self, levelData, pos, onFinish = None):
        global lasercrit_boss_arr
        super().__init__(levelData, pos, onFinish, lasercrit_boss_arr, 2, True)

    @staticmethod
    def pre_init():
        global lasercrit_boss_arr
        lasercrit_boss_arr = PopupBoss.loadGIFFolder("crit", scale = (300,300))

