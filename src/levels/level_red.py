import pygame
from src.popups.popup_cloud import Cloud
from src.main_const import *
from src.levels.level_base import Level
from src.objects.object_base import CustomObject
from src.objects.object_floor import Floor
from src.objects.object_portal import Portal
from src.objects.object_textinput import TextInput
from src.objects.object_board import Board
from src.objects.object_door import Door
from src.objects.object_throwable import Throwable
from src.objects.object_basket import Basket
from src.objects.object_seaweed import SeaWeed
from src.objects.object_queue import Queue
from src.popups.popup_trophy import Trophy
from src.popups.popup_over import GameOver
from src.objects.object_crystal import Crystal

class RedLevelOne(Level):
    def __init__(self, levelData):
        super().__init__(levelData)

        self.levelData.level_len = TILE.w*15
        self.levelData.background = "taylor.png"
        self.starting_objects = [
            Portal(200,0,levelData),
            Board(800-200,0, levelData, size = (1000,500)),
            Door("red", 1800-200, 0, levelData)
        ]
        self.textBox = 0
        self.stageOne()
        self.loadProgress()
        
        self.stage = 1

        if levelData.im_cheating: self.open()

        s = pygame.mixer.Sound(SOUNDS.red+"taylor.mp3")
        s.set_volume(0.5)
        self.levelData.channel0.play(s)


    def stageOne(self):
        self.objects = self.starting_objects.copy()
        to_add = [
            CustomObject(PATH.emoji+"love.png", 1000-200, 150, self.levelData, scale = (300,300)),
            CustomObject(PATH.emoji+"story.png", 1400-200, 150, self.levelData, scale = (300,300)),
        ]
        for obj in to_add:
            self.objects.append(obj)
        self.textBox = TextInput(1300-150-200,40, self.levelData, key = "Love Story")
        self.objects.append(self.textBox)

        self.levelData.popup_manager.add(Cloud(
            textArray = ["Witaj w czerwonych poziomach! Zostały one specjalnie skonstruowane przez znaną Ci dość dobrze osobę...",
            "Hiiiiii <3",
            "Idk co w sumie dokładnie to miało być więc użyłem wyobraźni, najwyżej zwal winę na nią <or wutever else wink wink>",
            "Popisz się znajomością Taylor Swift i zgadnij tytuły piosenek po rebusie!",
            "<Piosenka w tle jest dla dekoracji>"], 
            personArray = ["peter","magda","peter","magda","peter"], 
            levelData = self.levelData,),
        )

    def stageTwo(self):
        self.objects = self.starting_objects.copy()
        to_add = [
            CustomObject(PATH.emoji+"blank.png", 1000-200, 150, self.levelData, scale = (300,300)),
            CustomObject(PATH.emoji+"space.png", 1400-200, 150, self.levelData, scale = (300,300)),
        ]
        for obj in to_add:
            self.objects.append(obj)
        self.textBox = TextInput(1300-150-200,40, self.levelData, key = "Blank Space")
        self.objects.append(self.textBox)

    def stageThree(self):
        self.objects = self.starting_objects.copy()
        to_add = [
            CustomObject(PATH.emoji+"shake.png", 800-200, 150, self.levelData, scale = (300,300)),
            CustomObject(PATH.emoji+"eat.png", 1150-200, 150, self.levelData, scale = (300,300)),
             CustomObject(PATH.emoji+"off.png", 1500-200, 150, self.levelData, scale = (300,300)),
        ]
        for obj in to_add:
            self.objects.append(obj)
        self.textBox = TextInput(1300-150-200,40, self.levelData, key = "Shake it off")
        self.objects.append(self.textBox)
        
    def stageFour(self):
        self.objects = self.starting_objects.copy()
        to_add = [
            CustomObject(PATH.emoji+"2.png", 1000-200, 150, self.levelData, scale = (300,300)),
            CustomObject(PATH.emoji+"2.png", 1400-200, 150, self.levelData, scale = (300,300)),
            #CustomObject(PATH.emoji+"22.png", 1000-200, 0, self.levelData, scale = (300,300)),
            #CustomObject(PATH.emoji+"22.png", 1400-200, 0, self.levelData, scale = (300,300)),
        ]
        for obj in to_add:
            self.objects.append(obj)
        self.textBox = TextInput(1300-150-200,40, self.levelData, key = "22")
        self.objects.append(self.textBox)
        
    def stageFive(self):
        self.objects = self.starting_objects.copy()
        to_add = [
            CustomObject(PATH.emoji+"bad.png", 1000-200, 150, self.levelData, scale = (300,300)),
            CustomObject(PATH.emoji+"blood.png", 1400-200, 150, self.levelData, scale = (300,300)),
        ]
        for obj in to_add:
            self.objects.append(obj)
        self.textBox = TextInput(1300-150-200,40, self.levelData, key = "Bad blood")
        self.objects.append(self.textBox)

    def doNextStep(self):
        if self.textBox.isCorrect():
            self.stage += 1
            if(self.stage == 2):
                self.stageTwo()
            elif(self.stage == 3):
                self.stageThree()
            elif(self.stage == 4):
                self.stageFour()
            elif(self.stage == 5):
                self.stageFive()
            else:
                self.levelData.popup_manager.add(Trophy(self.levelData, onFinish=lambda:self.open()))
            return True         
        return False

class RedLevelTwo(Level):
    def __init__(self, levelData):
        super().__init__(levelData)
        self.levelData.level_len = TILE.w*20
        self.levelData.background = "forest.jpg"
        self.textInputOne = TextInput(1200, 360, self.levelData, key = "Dziobak")
        self.textInputTwo = TextInput(1200, 360, self.levelData, key = "Wombat")
        self.activity = True
        self.levelData.player.lockMovement()
        self.starting_objects = [
            Floor(levelData),
            Portal(200,0,levelData),
            CustomObject(PATH.obj+"tree.png", SCREEN.w-370, -20, self.levelData),
            CustomObject(PATH.obj+"tree.png", -350, -20, self.levelData),
            Board(1185,350, levelData, size = (330, 70)),
            Door("red", 1200, 0, levelData),
            CustomObject(PATH.obj+"sign_2.png",-40,0,levelData),
            CustomObject(PATH.obj+"sign_1.png",1650,0,levelData),
            ]
        self.stageOne()
        self.loadProgress()

        s = pygame.mixer.Sound(SOUNDS.red+"forest.mp3")
        s.set_volume(0.7)
        self.levelData.channel0.play(s)

        self.levelData.popup_manager.add(Cloud(
            textArray = [
            "A co to za zwierzątka latają?",
            "Friggin Dziobek man....",
            "Teraz się chociaż zgadza ;D"], 
            personArray = ["magda","peter","peter"], 
            levelData = levelData,),
        )

        if self.levelData.im_cheating: 
            self.open()
            self.levelData.player.unlockMovement()

    def stageOne(self):
        self.objects = self.starting_objects.copy()
        toAdd = [
            Throwable(PATH.obj+"platypus.png",0,300,40,20,self.levelData, isWalking = False, _type="Bird"),
            Throwable(PATH.obj+"platypusr.png",SCREEN.w-10,300,-40,20,self.levelData, isWalking = False, _type="Bird"),
            self.textInputOne
        ]
        for obj in toAdd:
            self.objects.append(obj)

    def stageTwo(self):
        self.objects = self.starting_objects.copy()
        toAdd = [
            Throwable(PATH.obj+"wombat.png",0,0,10,0,self.levelData, isWalking = True, _type="Wombat"),
            Throwable(PATH.obj+"wombatr.png",SCREEN.w-10,0,-10,0,self.levelData, isWalking = True, _type="Wombat"),
            self.textInputTwo
        ]
        for obj in toAdd:
            self.objects.append(obj)
        
    def doNextStep(self):
        if self.textInputOne.isCorrect():
            self.stageTwo()
            return True
            
        if self.textInputTwo.isCorrect():
            self.objects = self.starting_objects.copy()
            self.levelData.player.unlockMovement()
            self.levelData.popup_manager.add(Trophy(self.levelData, onFinish=lambda:self.open()))
            return True         
        return False


class RedLevelThree(Level):
    def __init__(self, levelData):
        super().__init__(levelData)
        self.levelData.level_len = TILE.w*12
        self.levelData.background = "maledives.png"
        self.queue = Queue(
            {
                0:Throwable(PATH.obj+"turtle.png",0,0,10,0,levelData,isWalking = True, _type = "Turtle"),
                1:Throwable(PATH.obj+"turtler.png",SCREEN.w,0,-10,0,levelData,isWalking = True, _type = "Turtle"),
                2:Throwable(PATH.obj+"turtle.png",0,0,10,0,levelData,isWalking = True, _type = "Turtle"),
                2.5:Throwable(PATH.obj+"turtler.png",SCREEN.w,0,-10,0,levelData,isWalking = True, _type = "Turtle"),
                4:Throwable(PATH.obj+"turtle.png",0,0,10,0,levelData,isWalking = True, _type = "Turtle"),
                4.1:Throwable(PATH.obj+"turtle.png",0,0,10,0,levelData,isWalking = True, _type = "Turtle"),
                6:Throwable(PATH.obj+"turtle.png",0,0,10,0,levelData,isWalking = True, _type = "Turtle"),
                8:Throwable(PATH.obj+"turtler.png",SCREEN.w,0,-10,0,levelData,isWalking = True, _type = "Turtle"),
                10:Throwable(PATH.obj+"turtler.png",SCREEN.w,0,-10,0,levelData,isWalking = True, _type = "Turtle"),
                10.2:Throwable(PATH.obj+"turtle.png",0,0,10,0,levelData,isWalking = True, _type = "Turtle"),
                12:Throwable(PATH.obj+"turtler.png",SCREEN.w,0,-10,0,levelData,isWalking = True, _type = "Turtle"),
                14:Throwable(PATH.obj+"turtle.png",0,0,10,0,levelData,isWalking = True, _type = "Turtle"),
                16:Throwable(PATH.obj+"turtler.png",SCREEN.w,0,-10,0,levelData,isWalking = True, _type = "Turtle"),
                16.1:Throwable(PATH.obj+"turtler.png",SCREEN.w,0,-10,0,levelData,isWalking = True, _type = "Turtle"),
                18:Throwable(PATH.obj+"turtle.png",0,0,10,0,levelData,isWalking = True, _type = "Turtle"),
                18.1:Throwable(PATH.obj+"turtler.png",SCREEN.w,0,-10,0,levelData,isWalking = True, _type = "Turtle"),
                20:Throwable(PATH.obj+"turtle.png",0,0,10,0,levelData,isWalking = True, _type = "Turtle"),
                20.1:Throwable(PATH.obj+"turtler.png",SCREEN.w,0,-10,0,levelData,isWalking = True, _type = "Turtle"),
            }, levelData)
        self.score_count = self.queue.scoreCount()    
        self.starting_objects = [
            Portal(200,0,levelData),
            Door("red", 1200, 0, levelData),
            CustomObject(PATH.obj+"tree3.png", -80, -TILE.h+TILE.offset+2, self.levelData),
            Basket(0,-TILE.h+TILE.offset,levelData),
            self.queue,
            SeaWeed(levelData),
        ]
        self.stageOne()
        self.loadProgress()
        self.lock = False

        s = pygame.mixer.Sound(SOUNDS.red+"hawaii.mp3")
        s.set_volume(0.5)
        self.levelData.channel0.play(s)

        self.levelData.popup_manager.add(Cloud(
            textArray = [
            "Idk man, dostałem słowa 'żólwie na malediwach' so here you go!"], 
            personArray = ["peter"], 
            levelData = levelData,),
        )

        if levelData.im_cheating: self.open()

    def stageOne(self):
        self.objects = self.starting_objects.copy()

    def doNextStep(self):
        if self.queue.isFinished() and not self.lock:
            self.lock = True
            if self.score[0] == self.score_count:
                self.levelData.popup_manager.add(Trophy(self.levelData, onFinish=lambda:self.open()))
            else:
                self.levelData.popup_manager.add(GameOver(self.levelData))
            return True

        return False  

    def reset(self):
        super().reset()
        self.queue.stop()


class RedLevelFour(Level):
    def __init__(self, levelData):
        super().__init__(levelData)
        self.levelData.level_len = TILE.w*13
        self.levelData.background = "redroom.png"

        self.textBox = TextInput(1200, 360, self.levelData, key = "Nie wiem")
        self.starting_objects = [
            #Floor(levelData),
            Portal(200,0,levelData),
            Door("red", 1200, 0, levelData),
            Board(1185,350, levelData, size = (330, 70)),
            self.textBox
        ]
        self.stageOne()
        self.loadProgress()

        s = pygame.mixer.Sound(SOUNDS.red+"sexy.mp3")
        s.set_volume(0.5)
        self.levelData.channel0.play(s)

        self.levelData.popup_manager.add(Cloud(
            textArray = [
            "Prepare your butthole!",
            "Aby przejść ten poziom, musisz od Magdy zdobyć hasło.",
            "Nie wiemy, co za to będzie chciała. But we hope for the best <3",
            "Mmmmmm.. tyle możliwości!"], 
            personArray = ["peter","peter","peter","magda"], 
            levelData = levelData,),
        )

        if levelData.im_cheating: self.open()

    def stageOne(self):
        self.objects = self.starting_objects.copy()

    def doNextStep(self):
        if self.textBox.isCorrect():
            self.levelData.popup_manager.add(Trophy(self.levelData, onFinish=lambda:self.open()))    

class RedLevelFive(Level):
    def __init__(self, levelData):
        super().__init__(levelData)
        self.levelData.background = "main.png"
        self.levelData.level_len = TILE.w*10

        self.starting_objects = [
            Floor(levelData),
            Portal(200,0,levelData),
            CustomObject(PATH.obj+"tree2.png", -100, -20, self.levelData),
            CustomObject(PATH.obj+"tree.png", 700, -20, self.levelData),
            CustomObject(PATH.obj+"rainbow_flower.png",500,0,levelData),
            Crystal("red",800,0,levelData),
            ##CustomObject(PATH.anims+"red.png", 800,0, levelData, 0, 65, 3)
        ]
        self.stageOne()
        self.loadProgress()

        self.levelData.popup_manager.add(Cloud(
            textArray = [
            "Well done! Zdobywasz czerwony kryształ!"], 
            personArray = ["peter"], 
            levelData = levelData,),
        )

        s = pygame.mixer.Sound(SOUNDS.f+"yayy.mp3")
        s.set_volume(0.5)
        self.levelData.channel0.play(s)


        if levelData.im_cheating: self.open()

    def stageOne(self):
        self.objects = self.starting_objects.copy()