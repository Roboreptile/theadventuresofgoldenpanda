import pygame
from src.levels.level_welcome import WelcomeLevel
from src.levels.level_red import RedLevelOne, RedLevelTwo, RedLevelThree, RedLevelFour, RedLevelFive
from src.levels.level_orange import OrangeLevelOne, OrangeLevelTwo, OrangeLevelThree, OrangeLevelFour, OrangeLevelFive
from src.levels.level_yellow import YellowLevelOne, YellowLevelTwo, YellowLevelThree, YellowLevelFour, YellowLevelFive
from src.levels.level_green import GreenLevelOne, GreenLevelTwo, GreenLevelThree, GreenLevelFour, GreenLevelFive
from src.levels.level_blue import BlueLevelOne, BlueLevelTwo, BlueLevelThree, BlueLevelFour, BlueLevelFive
from src.levels.level_navy import NavyLevelFour, NavyLevelOne, NavyLevelTwo, NavyLevelThree, NavyLevelFive
from src.levels.level_purple import PurpleLevelOne,PurpleLevelTwo, PurpleLevelThree, PurpleLevelFour, PurpleLevelFive
from src.levels.level_black import BlackLevel, BlackLevelCredits

from src.levels.level_factory_data import LevelFactoryData
from src.popups.popup_manager import PopupManager

levelData:LevelFactoryData = 0
levelSprites = []

class LevelFactory:

    @staticmethod
    def init(_levelData):
        global levelData
        levelData = _levelData
        levelData.popup_manager = PopupManager(levelData)
    
    @staticmethod
    def current():
        global levelData, levelSprites

        # display queued popups
        levelData.popup_manager.handle()

        # if level requires rebuild, build new objects
        # doNextStep is true when rebuild is required
        # generate() returns level's objects, change self.objects to edit level content
        if levelData.level != 0 and levelData.level.doNextStep():
            levelData.sprites.remove(levelData.player)
            levelData.sprites.remove(levelSprites)
            levelSprites = levelData.level.generate()
            levelData.sprites.add(levelSprites)
            levelData.sprites.add(levelData.player)
            return

        # cleans sprites to make rendering easier, for each particle set active to false to remove it from the level
        if levelData.level != 0 and levelData.level.handleActivity():
            levelData.level.dealWithActivity()
            levelSprites = levelData.level.generate()

        if ((levelData.prev_color == levelData.color and levelData.prev_room_nr == levelData.room_nr) and not levelData.force_restart): return
        
        if levelData.level != 0:
            levelData.sprites.remove(levelData.player)
            levelData.sprites.remove(levelSprites)    
        
        levelData.prev_color = levelData.color
        levelData.prev_room_nr = levelData.room_nr
        
        if levelData.color == "all":
            if levelData.room_nr>-10000: levelData.level = WelcomeLevel(levelData)

        elif levelData.color == "red":
            if levelData.room_nr == 1: levelData.level = RedLevelOne(levelData)
            elif levelData.room_nr == 2: levelData.level = RedLevelTwo(levelData)
            elif levelData.room_nr == 3: levelData.level = RedLevelThree(levelData)
            elif levelData.room_nr == 4: levelData.level = RedLevelFour(levelData)
            elif levelData.room_nr == 5: levelData.level = RedLevelFive(levelData)
            else: print("ERROR")

        elif levelData.color == "orange":
            if levelData.room_nr == 1: levelData.level = OrangeLevelOne(levelData)
            elif levelData.room_nr == 2: levelData.level = OrangeLevelTwo(levelData)
            elif levelData.room_nr == 3: levelData.level = OrangeLevelThree(levelData)
            elif levelData.room_nr == 4: levelData.level = OrangeLevelFour(levelData)
            elif levelData.room_nr == 5: levelData.level = OrangeLevelFive(levelData)
            else: print("ERROR")

        elif levelData.color == "yellow":
            if levelData.room_nr == 1: levelData.level = YellowLevelOne(levelData)
            elif levelData.room_nr == 2: levelData.level = YellowLevelTwo(levelData)
            elif levelData.room_nr == 3: levelData.level = YellowLevelThree(levelData)
            elif levelData.room_nr == 4: levelData.level = YellowLevelFour(levelData)
            elif levelData.room_nr == 5: levelData.level = YellowLevelFive(levelData)
            else: print("ERROR")

        elif levelData.color == "green":
            if levelData.room_nr == 1: levelData.level = GreenLevelOne(levelData)
            elif levelData.room_nr == 2: levelData.level = GreenLevelTwo(levelData)
            elif levelData.room_nr == 3: levelData.level = GreenLevelThree(levelData)
            elif levelData.room_nr == 4: levelData.level = GreenLevelFour(levelData)
            elif levelData.room_nr == 5: levelData.level = GreenLevelFive(levelData)
            else: print("ERROR")

        elif levelData.color == "blue":
            if levelData.room_nr == 1: levelData.level = BlueLevelOne(levelData)
            elif levelData.room_nr == 2: levelData.level = BlueLevelTwo(levelData)
            elif levelData.room_nr == 3: levelData.level = BlueLevelThree(levelData)
            elif levelData.room_nr == 4: levelData.level = BlueLevelFour(levelData)
            elif levelData.room_nr == 5: levelData.level = BlueLevelFive(levelData)
            else: print("ERROR")

        elif levelData.color == "navy":
            if levelData.room_nr == 1: levelData.level = NavyLevelOne(levelData)
            elif levelData.room_nr == 2: levelData.level = NavyLevelTwo(levelData)
            elif levelData.room_nr == 3: levelData.level = NavyLevelThree(levelData)
            elif levelData.room_nr == 4: levelData.level = NavyLevelFour(levelData)
            elif levelData.room_nr == 5: levelData.level = NavyLevelFive(levelData)
            else: print("ERROR")

        elif levelData.color == "purple":
            if levelData.room_nr == 1: levelData.level = PurpleLevelOne(levelData)
            elif levelData.room_nr == 2: levelData.level = PurpleLevelTwo(levelData)
            elif levelData.room_nr == 3: levelData.level = PurpleLevelThree(levelData)
            elif levelData.room_nr == 4: levelData.level = PurpleLevelFour(levelData)
            elif levelData.room_nr == 5: levelData.level = PurpleLevelFive(levelData)
            else: print("ERROR")

        elif levelData.color == "black":
            if levelData.room_nr == 1: levelData.level = BlackLevel(levelData)
            elif levelData.room_nr == 2: levelData.level = BlackLevelCredits(levelData)
            else: print("ERROR")
            
        else: print("ERROR")

        levelSprites = levelData.level.generate()
        levelData.sprites.add(levelSprites)
        levelData.sprites.add(levelData.player)
