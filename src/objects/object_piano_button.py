import pygame
from random import randint
from src.objects.object_base import CustomObject
from src.main_const import *

class PianoKey(CustomObject):
    def __init__(self, name, ktype, x,y, levelData):
        super().__init__(PATH.obj+"key"+ktype+".png",x,y,levelData)
        self.active  = pygame.image.load(PATH.obj + "key"+ktype+"D.png").convert_alpha()
        self.name = name
        self.sound = pygame.mixer.Sound(SOUNDS.piano+name+".mp3")
        self.dur = -1

    def update(self):
        super().update()
        if(self.dur>0): 
            self.dur -= 1
        elif(self.dur==0):
            self.image = self.image_base
            self.dur -=1

    def didHeMessUp(self,shouldBeThis):
        self.levelData.channel1.play(self.sound)
        self.image = self.active
        self.dur = GAME.max_fps//2
        return not self.name == shouldBeThis



