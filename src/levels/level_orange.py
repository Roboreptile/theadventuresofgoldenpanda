import pygame
from src.popups.popup_cloud import Cloud
from src.main_const import *
from src.levels.level_base import Level
from src.objects.object_base import CustomObject
from src.objects.object_floor import Floor
from src.objects.object_portal import Portal
from src.objects.object_textinput import TextInput
from src.objects.object_board import Board
from src.objects.object_door import Door
from src.objects.object_launchpad import LaunchPad
from src.objects.object_launchpad_button import Pad
from src.objects.object_throwable import Throwable
from src.objects.object_basket import Basket
from src.objects.object_seaweed import SeaWeed
from src.objects.object_piano import Piano
from src.objects.object_piano_button import PianoKey
from src.objects.object_queue import Queue
from src.popups.popup_trophy import Trophy
from src.popups.popup_over import GameOver
from src.objects.object_head import Head
from src.objects.object_keyboard import KeyBoard
from src.objects.object_keyboard_button import Key
from src.objects.object_crystal import Crystal



class OrangeLevelOne(Level):
    def __init__(self, levelData):
        super().__init__(levelData)
        self.levelData.background = "hearts.jfif"
        self.levelData.level_len = TILE.w * 15
        self.starting_objects = [
            Portal(100,0,levelData),
            CustomObject(PATH.obj+"notes.png", 575,100,levelData),
            CustomObject(PATH.obj+"sign_1.png",-100,0,levelData),
            CustomObject(PATH.obj+"sign_3.png",1800,0,levelData),
            Door("orange",1200,0,levelData)
        ]
        self.piano = 0
        self.stageOne(levelData)
        self.loadProgress()

        s = pygame.mixer.Sound(SOUNDS.orange + "careless whisper.mp3")
        s.set_volume(0.1)
        self.levelData.channel0.play(s)

        self.levelData.popup_manager.add(Cloud(
            textArray = ["Witaj na poziomach pomarańczowych - MUSICAL!",
            "Czeka Cię tu wiele dźwiękowych cacuszków od najlepszych wykonawców!",
            "Ale zanim wejdziesz, musisz udowodnić swoją wartość. Zagraj melodię która leci w tle :P",
            "Poznajesz co to za piosenka Michałku? <3"], 
            personArray = ["peter","peter","peter","wojciu"], 
            levelData = levelData,),
        )
        self.stage = 1

        if levelData.im_cheating: self.open()

    def stageOne(self,levelData):
        x = 500
        y = 10
        keys = [
            PianoKey("C","w",x,y+10,levelData),
            PianoKey("D","w",x+30,y+10,levelData),
            PianoKey("E","w",x+60,y+10,levelData),
            PianoKey("F","w",x+90,y+10,levelData),
            PianoKey("G","w",x+120,y+10,levelData),
            PianoKey("A","w",x+150,y+10,levelData),
            PianoKey("H","w",x+180,y+10,levelData),
            PianoKey("c1","w",x+210,y+10,levelData),
            PianoKey("d1","w",x+240,y+10,levelData),
            PianoKey("e1","w",x+270,y+10,levelData),
            PianoKey("f1","w",x+300,y+10,levelData),
            PianoKey("g1","w",x+330,y+10,levelData),
            PianoKey("a1","w",x+360,y+10,levelData),
            PianoKey("h1","w",x+390,y+10,levelData),
            PianoKey("c2","w",x+420,y+10,levelData),
            PianoKey("d2","w",x+450,y+10,levelData),
            PianoKey("e2","w",x+480,y+10,levelData),
            PianoKey("f2","w",x+510,y+10,levelData),
            PianoKey("g2","w",x+540,y+10,levelData),
            PianoKey("a2","w",x+570,y+10,levelData),
            PianoKey("h2","w",x+600,y+10,levelData),

            PianoKey("DES","b",x+15,y+10+31,levelData),
            PianoKey("ES","b",x+45,y+10+31,levelData),
            PianoKey("GES","b",x+105,y+10+31,levelData),
            PianoKey("AS","b",x+135,y+10+31,levelData),
            PianoKey("B","b",x+165,y+10+31,levelData),
            PianoKey("des1","b",x+225,y+10+31,levelData),
            PianoKey("es1","b",x+255,y+10+31,levelData),
            PianoKey("ges1","b",x+315,y+10+31,levelData),
            PianoKey("as1","b",x+345,y+10+31,levelData),
            PianoKey("b1","b",x+375,y+10+31,levelData),
            PianoKey("des2","b",x+435,y+10+31,levelData),
            PianoKey("es2","b",x+465,y+10+31,levelData),
            PianoKey("ges2","b",x+525,y+10+31,levelData),
            PianoKey("as2","b",x+555,y+10+31,levelData),
            PianoKey("b2","b",x+585,y+10+31,levelData),
        ]

        self.piano = Piano(x-10,y+3,levelData,keys)
        self.objects = self.starting_objects.copy()
        
        to_add = [
            #Floor(levelData),
            self.piano
        ]
        for key in keys:
            to_add.append(key)
        for obj in to_add:
            self.objects.append(obj)

class OrangeLevelTwo(Level):
    def __init__(self, levelData):
        super().__init__(levelData)
        self.levelData.level_len = TILE.w*15
        self.levelData.background = "party2.png"

        Head.pre_init()

        s = pygame.mixer.Sound(SOUNDS.orange + "what is love.wav")
        s.set_volume(0.5)
        self.levelData.channel0.play(s)

        self.queue = Queue(
            {
               1.33:Head(levelData,self.stop),
               1.41:Head(levelData,self.stop),
               1.44:Head(levelData,self.stop),
               2.24:Head(levelData,self.stop),
               2.32:Head(levelData,self.stop),
               2.34:Head(levelData,self.stop),
               2.43:Head(levelData,self.stop),
               3.34:Head(levelData,self.stop),
               3.42:Head(levelData,self.stop),
               4.34:Head(levelData,self.stop),
               5.44:Head(levelData,self.stop),
               6.14:Head(levelData,self.stop),
               6.23:Head(levelData,self.stop),
               6.32:Head(levelData,self.stop),
               6.41:Head(levelData,self.stop),
               6.44:Head(levelData,self.stop),
               7.14:Head(levelData,self.stop),
               7.24:Head(levelData,self.stop),
               7.32:Head(levelData,self.stop),
               7.42:Head(levelData,self.stop),
               7.44:Head(levelData,self.stop),
               8.14:Head(levelData,self.stop),
               8.24:Head(levelData,self.stop),
               8.33:Head(levelData,self.stop),
               8.42:Head(levelData,self.stop),
               9.14:Head(levelData,self.stop),
               9.23:Head(levelData,self.stop),
               9.32:Head(levelData,self.stop),
               9.41:Head(levelData,self.stop),
               9.43:Head(levelData,self.stop),
               9.44:Head(levelData,self.stop),
               10.14:Head(levelData,self.stop),
               10.23:Head(levelData,self.stop),
               10.33:Head(levelData,self.stop),
               10.42:Head(levelData,self.stop),
               10.44:Head(levelData,self.stop),
               11.14:Head(levelData,self.stop),
               11.24:Head(levelData,self.stop),
               11.33:Head(levelData,self.stop),
               11.42:Head(levelData,self.stop),
               11.44:Head(levelData,self.stop),
               12.14:Head(levelData,self.stop),
               12.24:Head(levelData,self.stop),
               12.33:Head(levelData,self.stop),
               12.42:Head(levelData,self.stop),
               12.44:Head(levelData,self.stop),
               13.14:Head(levelData,self.stop),
               13.24:Head(levelData,self.stop),
               13.33:Head(levelData,self.stop),
               13.42:Head(levelData,self.stop),
               13.44:Head(levelData,self.stop),
               14.14:Head(levelData,self.stop),
               14.24:Head(levelData,self.stop),
               14.33:Head(levelData,self.stop),
               14.42:Head(levelData,self.stop),
               14.44:Head(levelData,self.stop),
               15.14:Head(levelData,self.stop),
               15.24:Head(levelData,self.stop),
               15.33:Head(levelData,self.stop),
               15.42:Head(levelData,self.stop),
               15.44:Head(levelData,self.stop),
               16.14:Head(levelData,self.stop),
               16.24:Head(levelData,self.stop),
               16.33:Head(levelData,self.stop),
               16.42:Head(levelData,self.stop),
               16.44:Head(levelData,self.stop),
               17.14:Head(levelData,self.stop),
               17.24:Head(levelData,self.stop),
               17.33:Head(levelData,self.stop),
               17.42:Head(levelData,self.stop),
               17.44:Head(levelData,self.stop),
               18.14:Head(levelData,self.stop),
               18.24:Head(levelData,self.stop),
               18.32:Head(levelData,self.stop),
               18.42:Head(levelData,self.stop),
               18.44:Head(levelData,self.stop),
               19.14:Head(levelData,self.stop),
               19.24:Head(levelData,self.stop),
               19.33:Head(levelData,self.stop),
               19.42:Head(levelData,self.stop),
               19.44:Head(levelData,self.stop),
               20.14:Head(levelData,self.stop),
               20.23:Head(levelData,self.stop),
               20.33:Head(levelData,self.stop),
               20.42:Head(levelData,self.stop),
               20.44:Head(levelData,self.stop),
               21.14:Head(levelData,self.stop),
               21.22:Head(levelData,self.stop),
               21.33:Head(levelData,self.stop),
               21.42:Head(levelData,self.stop),
               21.44:Head(levelData,self.stop),
               22.14:Head(levelData,self.stop),
               22.23:Head(levelData,self.stop),
               22.24:Head(levelData,self.stop),
               22.33:Head(levelData,self.stop),
               22.42:Head(levelData,self.stop),
               23.24:Head(levelData,self.stop),
               23.32:Head(levelData,self.stop),
               23.42:Head(levelData,self.stop),
               24.24:Head(levelData,self.stop),
               24.32:Head(levelData,self.stop),
               24.34:Head(levelData,self.stop),
               24.43:Head(levelData,self.stop),
               25.14:Head(levelData,self.stop),
               25.22:Head(levelData,self.stop),
               25.34:Head(levelData,self.stop),
               25.42:Head(levelData,self.stop),
               26.24:Head(levelData,self.stop),
               26.33:Head(levelData,self.stop),
               26.42:Head(levelData,self.stop),
               27.24:Head(levelData,self.stop),
               27.32:Head(levelData,self.stop),
               27.42:Head(levelData,self.stop),
               28.24:Head(levelData,self.stop),
               28.33:Head(levelData,self.stop),
               28.41:Head(levelData,self.stop),
               28.43:Head(levelData,self.stop),
               29.32:Head(levelData,self.stop),
               29.34:Head(levelData,self.stop),
               29.43:Head(levelData,self.stop),
               30.14:Head(levelData,self.stop),
               30.24:Head(levelData,self.stop),
               30.33:Head(levelData,self.stop),
               30.41:Head(levelData,self.stop),
               30.43:Head(levelData,self.stop),
               31.12:Head(levelData,self.stop),
               31.34:Head(levelData,self.stop),
               31.42:Head(levelData,self.stop),
               32.12:Head(levelData,self.stop),
               32.34:Head(levelData,self.stop),
               32.42:Head(levelData,self.stop),
               33.33:Head(levelData,self.stop),
               33.41:Head(levelData,self.stop),
               33.43:Head(levelData,self.stop),
               34.24:Head(levelData,self.stop),
               34.32:Head(levelData,self.stop),
               34.41:Head(levelData,self.stop),
               32.43:Head(levelData,self.stop),
               35.13:Head(levelData,self.stop),
               35.34:Head(levelData,self.stop),
               35.43:Head(levelData,self.stop),
               36.13:Head(levelData,self.stop),
               36.34:Head(levelData,self.stop),
               36.42:Head(levelData,self.stop),
               37.43:Head(levelData,self.stop)
            }, levelData, True)
        self.score_count = self.queue.scoreCount()    
        self.starting_objects = [
            Floor(levelData,"D"),
            Portal(200,0,levelData),
            Door("orange", 1200, 0, levelData),
            self.queue
        ]
        self.stageOne()
        self.loadProgress()

        self.levelData.popup_manager.add(Cloud(
            textArray = ["Vladislaw! Bby do't hurt me!",
            "Nie klikaj na Vlada! Don't hurt him!",
            "And now, dance!"], 
            personArray = ["wojciu","peter","peter"], 
            levelData = levelData,),
        )

        self.lock = False

        if levelData.im_cheating: self.open()
    
    def stop(self):
        self.queue.stop()
        self.levelData.popup_manager.add(GameOver(self.levelData))

    def stageOne(self):
        self.objects = self.starting_objects.copy()

    def doNextStep(self):

        if self.queue.isFinished() and not self.lock:
            self.lock = True
            if self.score[0] == self.score_count:
                self.levelData.popup_manager.add(Trophy(self.levelData, onFinish=lambda:self.open()))
            else:
                self.levelData.popup_manager.add(GameOver(self.levelData, onFinish=lambda:self.restart()))
            return True

        return False  

    def restart(self):
        pass



class OrangeLevelThree(Level):
    def __init__(self, levelData):
        super().__init__(levelData)
        self.levelData.background = "main.png"
        self.levelData.level_len = TILE.w * 15
        
        self.starting_objects = [
            Floor(levelData),
            Portal(0,0,levelData),
            CustomObject(PATH.obj+"tree2.png", 400, 0, self.levelData),
            CustomObject(PATH.obj+"tree.png", 800, 0, self.levelData),
            CustomObject(PATH.obj+"flower.png", 170, 0, self.levelData),

            CustomObject(PATH.obj+"flower_2.png", 692, 0, self.levelData),

            Door("orange",1200,0,levelData),


        ]
        self.launchPad = 0
        self.stageOne(levelData)
        self.loadProgress()
        self.stage = 1
        self.levelData.popup_manager.add(Cloud(
            textArray = ["Ugh.. who farted?!",
            "A, to ten launchpad.. I hope.",
            "Zadanie jest proste - powtórz kombinację która się wyświetla.",
            "What could possibly go wrong?"], 
            personArray = ["peter","peter","peter","peter"], 
            levelData = levelData,),
        )


        if levelData.im_cheating: self.open()

    def stageOne(self,levelData):
        x = 700
        y = 0
        pads = [
            Pad(1,"1.wav",x+20,y+240,levelData),
            Pad(2,"2.wav",x+130,y+240,levelData),
            Pad(3,"3.wav",x+240,y+240,levelData),

            Pad(4,"4.wav",x+20,y+130,levelData),
            Pad(5,"5.wav",x+130,y+130,levelData),
            Pad(6,"6.wav",x+240,y+130,levelData),

            Pad(7,"7.wav",x+20,y+20,levelData),
            Pad(8,"8.wav",x+130,y+20,levelData),
            Pad(9,"9.wav",x+240,y+20,levelData),
        ]

        self.launchPad = LaunchPad(x,y,levelData,pads)
        self.objects = self.starting_objects.copy()
        
        to_add = [
            self.launchPad
        ]
        for pad in pads:
            to_add.append(pad)
        for obj in to_add:
            self.objects.append(obj)


class OrangeLevelFour(Level):
    def __init__(self, levelData):
        super().__init__(levelData)
        self.levelData.level_len = TILE.w*13
        self.levelData.background = "animu.jfif"

        self.starting_objects = [
            Portal(0,0,levelData),
            CustomObject(PATH.obj+"rainbow_flower.png",100,0,levelData),
            Door("orange", 1200+200, 0, levelData),
        ]
        self.stageOne()
        self.loadProgress()
        self.levelData.popup_manager.add(Cloud(
            textArray = ["Zapewne nie uwierzysz, ale pomysł na ten poziom miała..",
            "It's me!",
            "Myślę że łatwo jest domyśleć się, co trzeba zrobić. Ej bi si do i ef you!"], 
            personArray = ["peter","weronika","peter"], 
            levelData = levelData,),
        )

        s = pygame.mixer.Sound(SOUNDS.orange + "animu.mp3")
        s.set_volume(0.5)
        self.levelData.channel0.play(s)

        if levelData.im_cheating: self.open()

    def stageOne(self):
        x = 400
        y = 200
        keys = [
            Key("a","a",x,y,self.levelData),
            Key("adash","b",x+60,y,self.levelData),
            Key("b","c",x+60*2,y,self.levelData),
            Key("c","chouse",x+60*3,y,self.levelData),
            Key("cdash","cdash",x+60*4,y,self.levelData),
            Key("d","d",x+60*5,y,self.levelData),
            Key("e","dz",x+60*6,y,self.levelData),
            Key("edash","ddash",x+60*7,y,self.levelData),
            Key("f","e",x+60*8,y,self.levelData),
            Key("g","f",x+60*9,y,self.levelData),
            Key("h","g",x,y+60,self.levelData),
            Key("i","h",x+60,y+60,self.levelData),
            Key("j","i",x+60*2,y+60,self.levelData),
            Key("k","j",x+60*3,y+60,self.levelData),
            Key("l","k",x+60*4,y+60,self.levelData),
            Key("m","l",x+60*5,y+60,self.levelData),
            Key("n","lj",x+60*6,y+60,self.levelData),
            Key("o","m",x+60*7,y+60,self.levelData),
            Key("p","n",x+60*8,y+60,self.levelData),
            Key("r","nj",x+60*9,y+60,self.levelData),
            Key("s","o",x,y+120,self.levelData),
            Key("sdash","p",x+60,y+120,self.levelData),
            Key("t","r",x+60*2,y+120,self.levelData),
            Key("u","s",x+60*3,y+120,self.levelData),
            Key("v","shouse",x+60*4,y+120,self.levelData),
            Key("w","t",x+60*5,y+120,self.levelData),
            Key("x","u",x+60*6,y+120,self.levelData),
            Key("y","v",x+60*7,y+120,self.levelData),
            Key("z","z",x+60*8,y+120,self.levelData),
            Key("zdash","zhouse",x+60*9,y+120,self.levelData),
        ]
        keyboard = KeyBoard(x-10,y-10,levelData=self.levelData,keys=keys)

        self.objects = self.starting_objects.copy()
        self.objects.append(keyboard)
        for key in keys:
            self.objects.append(key)

    def doNextStep(self):
        return False


class OrangeLevelFive(Level):
    def __init__(self, levelData):
        super().__init__(levelData)
        self.levelData.background = "main.png"
        self.levelData.level_len = TILE.w*10

        self.starting_objects = [
            Floor(levelData),
            
            Portal(200,0,levelData),
            CustomObject(PATH.obj+"tree2.png", 800, 0, self.levelData),
            CustomObject(PATH.obj+"flower_2.png", 692, -90, self.levelData),
            CustomObject(PATH.obj+"flower_3.png", 320, -90, self.levelData),

            Crystal("orange",800,0,levelData),

        ]
        self.stageOne()
        self.loadProgress()
        self.levelData.popup_manager.add(Cloud(
            textArray = ["Wow! Gratulacje!",
            "Zdobywasz pomarańczowy kryształ!"],
            personArray = ["peter","weronika"], 
            levelData = levelData,),
        )

        s = pygame.mixer.Sound(SOUNDS.f+"yayy.mp3")
        s.set_volume(0.5)
        self.levelData.channel0.play(s)

        if levelData.im_cheating: self.open()

    def stageOne(self):
        self.objects = self.starting_objects.copy()