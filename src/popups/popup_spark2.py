import pygame
from src.popups.popup_plus import PopupPlus
from src.main_const import *

s2_array = []

class Spark2(PopupPlus):
    def __init__(self, levelData, onFinish = None, change_every_x_frames = 2):
        global s2_array
        super().__init__(onFinish, s2_array, levelData, change_every_x_frames, True)

    @staticmethod
    def pre_init():
        global s2_array
        s2_array = PopupPlus.loadGIFFolder("sparkles2",scale=(400,400))