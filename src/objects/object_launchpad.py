import pygame
from random import randint

from src.objects.object_base import CustomObject
from src.objects.object_launchpad_button import Pad
from src.main_const import *

class LaunchPad(CustomObject):
    def __init__(self,x,y,levelData, pads):
        super().__init__(PATH.obj+"board.png",x,y,levelData, scale = (360,360))
        self.hihi = 0
        self.hihiIdx = 0

        self.howForkedIsHe = 30

        self.displayingSeq = True
        self.displayingSeqIdx = -1
        self.displayingSeqIdxMax = 1
        self.restarting = False
        self.restart()
        
        self.pads = pads

    def restart(self):
        self.hihi = [0]*self.howForkedIsHe
        self.hihiIdx = 0
        self.displayingSeq = True
        self.displayingSeqIdx = -1
        self.displayingSeqIdxMax = 1
        for i in range(0,self.howForkedIsHe):
            self.hihi[i] = randint(0,8)
        self.restarting = False

        if(self.levelData.im_cheating): 
            print(self.hihi)

    def update(self):
        super().update()

        if self.restarting == True:
            for i in range(0,9):
                if self.pads[i].isFlashing(): return
            self.restart()
            return

        if self.displayingSeq == False:
            if self.levelData.events != None: self.handleEvents()
        else:
            for i in range(0,9):
                if self.pads[i].isFlashing(): return

            if self.displayingSeqIdx == -1:
                self.displayingSeqIdx += 1
                self.pads[self.hihi[self.displayingSeqIdx]].startFlashing()
            else:
                if(self.pads[self.hihi[self.displayingSeqIdx]].isFlashing()):
                    return
                else:
                    self.displayingSeqIdx+=1
                    if(self.displayingSeqIdx == self.displayingSeqIdxMax):
                        self.displayingSeq = False
                        self.displayingSeqIdx = -1

                    else:
                        if(self.displayingSeqIdxMax <=3): 
                            self.pads[self.hihi[self.displayingSeqIdx]].startFlashing()
                        else:
                            self.pads[self.hihi[self.displayingSeqIdx]].startFlashing(True)

    def handleEvents(self):
        for event in self.levelData.events:
            if event.type == pygame.MOUSEBUTTONDOWN:
                for pad in self.pads:
                    if pad.rect.collidepoint(event.pos):
                        if(pad.didHeMessUp(self.hihi[self.hihiIdx]+1)): self.restarting = True
                        else:
                            self.hihiIdx+=1
                            if(self.hihiIdx==self.displayingSeqIdxMax):
                                self.hihiIdx = 0
                                if(self.displayingSeqIdxMax < 3):
                                    self.displayingSeq = True
                                    self.displayingSeqIdxMax += 1
                                elif(self.displayingSeqIdxMax == 3):
                                    self.displayingSeq = True
                                    self.displayingSeqIdxMax = self.howForkedIsHe
                                elif(self.displayingSeqIdxMax==self.howForkedIsHe):
                                    self.hihiIdx = 0
                                    self.levelData.level.open()
                    else:
                        pass     


