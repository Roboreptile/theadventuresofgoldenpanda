import pygame
from src.main_const import *
from src.objects.object_base import CustomObject


class Quiz(CustomObject):
    def __init__(self, x,y, levelData, question, q_map, key, path_img = PATH.dating, path_sound = SOUNDS.dating_sim):
        super().__init__(PATH.obj+"board.png",x-20,y,levelData, scale = (750,620))
        self.question_dict = q_map
        self.question = question
        self.q = self.question_dict[key]
        self.pi = path_img
        self.ps = path_sound
        self.question.modify(self.q, path_img, path_sound)

    def update(self):
        super().update()
        if(self.question.block == True): return
        if(self.question.answered == True):
            if(self.question.on_finish!=None):
                self.question.on_finish()
                self.question.stop()
            else:
                self.q = self.question_dict[self.question.answer]
                self.question.modify(self.q, self.pi, self.ps)
            
            


