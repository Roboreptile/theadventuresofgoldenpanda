import pygame
from src.objects.object_portal import Portal
from src.objects.object_secret_bush import SecretBush
from src.main_const import *
from src.levels.level_base import Level
from src.levels.level_factory_data import LevelFactoryData
from src.objects.object_door import Door
from src.objects.object_floor import  Floor
from src.objects.object_base import CustomObject
from src.popups.popup_cloud import Cloud
from src.popups.popup_darkness import Darkness

from pygame.mixer import Channel

class WelcomeLevel(Level):
    def __init__(self, levelData:LevelFactoryData):
    
        super().__init__(levelData)
        self.levelData.level_len = TILE.w*20
        self.levelData.background = "main.png"
        self.objects = [

            CustomObject(PATH.obj+"tree.png", 900, -20, self.levelData),
            CustomObject(PATH.obj+"tree.png", 100, -40, self.levelData),

            Floor(self.levelData),
                        
            Door("red", 0, 0, self.levelData),
            Door("orange", 0+1*DOOR.w, 0, self.levelData),
            Door("yellow", 0+2*DOOR.w, 0, self.levelData),
            Door("green", 0+3*DOOR.w, 0, self.levelData),
            Door("blue", 0+4*DOOR.w, 0, self.levelData),
            Door("navy", 0+5*DOOR.w, 0, self.levelData),
            Door("purple", 0+6*DOOR.w, 0, self.levelData),

            CustomObject(PATH.obj+"stone.png", 450, 0, self.levelData),
            
            CustomObject(PATH.obj+"bush_3.png",1510,-60,levelData),
            CustomObject(PATH.obj+"bush_2.png",860,-20,levelData),
            CustomObject(PATH.obj+"flower.png",600,0,levelData),
            CustomObject(PATH.obj+"flower_2.png",100,-80,levelData),

            SecretBush(7*DOOR.w, 0, self.levelData),
            #CustomObject(PATH.obj+"big_bush.png", 0+7*DOOR.w, 0, self.levelData),
        ]

        #self.open("red")
        #self.open("orange")
        #self.open("yellow")
        #self.open("green")
        #self.open("blue")
        #self.open("navy")
        #self.open("purple")


        if(len(self.levelData.clouds)!=0):
            self.levelData.popup_manager.add(self.levelData.clouds)
            self.levelData.clouds = []

        sound = pygame.mixer.Sound(SOUNDS.welcome + "happy.mp3")
        self.levelData.channel0.play(sound)
        
    def enter(self, pos):
        for obj in self.objects:
            if isinstance(obj, Door) or isinstance(obj, SecretBush):
                if obj.enter(pos):
                    self.levelData.popup_manager.add(Darkness(speed = GAME.max_fps//3, reverse = False, onFinish = lambda: self._enter(obj, pos)))
                    break
                elif obj.could_enter(pos) and isinstance(obj, Door):
                    obj.open()
        

        

