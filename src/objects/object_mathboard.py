import pygame

from src.objects.object_base import CustomObject
from src.main_const import *
from src.levels.level_factory_data import LevelFactoryData
from random import randint

class MathBoard(CustomObject):
    def __init__(self, x, y, levelData, size, mathtextinput):
        super().__init__(PATH.obj+"board.png", x, y, levelData, scale=size)
        self.mti = mathtextinput
        self.randomQuestion()

    def randomQuestion(self):
        nr1 = randint(0,49)
        nr2 = randint(0,49)
        s = str(nr1+nr2)
        self.mti.key = s

        txt = self.levelData.font.render(str(nr1)+"+"+str(nr2), True, (0,0,0))
        self.image = self.image_base.copy()
        self.image.blit(txt, (100,40))


