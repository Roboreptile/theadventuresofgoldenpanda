from src.objects.object_base import CustomObject
from src.levels.level_factory_data import LevelFactoryData
from src.popups.popup_fuse import Fuse
from src.main_const import *
from src.popups.popup_boom import Boom


class Tnt(CustomObject):
    def __init__(self, x, y, levelData:LevelFactoryData):
        super().__init__(PATH.obj+"tnt.png", x, y, levelData)
        
            

    