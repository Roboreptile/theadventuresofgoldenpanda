import pygame
from src.levels.level_factory_data import  LevelFactoryData
from src.popups.popup_cloud import Cloud

class PopupManager:
    def __init__(self, levelData:LevelFactoryData):
        self.levelData = levelData
        self.currentPopups = []
        self.toAdd = []

    def handle(self):
        while True:
            f = False
            for idx,popup in enumerate(self.currentPopups):
                if popup.isFinished():
                    self.currentPopups.pop(idx)
                    self.levelData.sprites.remove(popup)
                    f = True
                    break
            if not f: break

        if(len(self.toAdd)>0):
            for popup in self.toAdd:
                self.currentPopups.append(popup)
                self.levelData.sprites.add(popup)
            self.toAdd.clear()
            
    def add(self,popups):
        if isinstance(popups, list):
            for p in popups:
                self.toAdd.append(p)
        else: self.toAdd.append(popups)
        

    def deactivateAll(self):
        for popup in self.currentPopups:
            popup.f = None
            popup.status = True
         

    def ping(self):
        for popup in self.currentPopups:
            if isinstance(popup, Cloud):
                popup.next()
        if(len(self.currentPopups)==0): return True
        return False
