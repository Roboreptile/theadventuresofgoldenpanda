
class PLAYER:
    ms = 6*3
    us = 15 #up
    g = 1
    w = 300
    h = 300

class SCREEN:
    w = 1800
    h = 720

class DOOR:
    w = 294
    h = 373
    
class TILE:
    w = 128
    h = 128
    offset = 20
    
class BUSH:
    w = 512
    h = 256
    
class GAME:
    max_fps = 60
    camera_sides_px = 100

class PATH:
    char = "./assets/textures/characters/"
    textures = "./assets/textures/"
    doors = "./assets/textures/doors/"
    tiles = "./assets/textures/tiles/"
    obj = "./assets/textures/objects/"
    anims = "./assets/textures/animations/"
    anims_f = "./assets/textures/anims_f/"
    bgs = "./assets/textures/backgrounds/"
    main = "./assets/textures/"
    emoji = "./assets/textures/emojis/"
    keys = "./assets/textures/alphabet/"
    dating = "./assets/textures/dating/"
    gay = "./assets/textures/gay_quiz/"
    dt = "./assets/textures/dt_quiz/"
    moo = "./assets/textures/cow/"
    rebus = "./assets/textures/rebus/"
    riddle = "./assets/textures/riddle/"
    boss = "./assets/textures/boss/"

class SOUNDS:
    welcome = "./assets/sounds/"
    f = welcome
    red = "./assets/sounds/red/"
    orange = "./assets/sounds/orange/"
    piano = "./assets/sounds/orange/piano/"
    anime = "./assets/sounds/orange/alphabet/"
    yellow = "./assets/sounds/yellow/"
    dating_sim = "./assets/sounds/yellow/dating/"
    green = "./assets/sounds/green/"
    blue = "./assets/sounds/blue/"
    navy = "./assets/sounds/navy/"
    purple = "./assets/sounds/purple/"
    black = "./assets/sounds/black/"
    gay = "./assets/sounds/navy/gay_quiz/"
    dt = "./assets/sounds/navy/dt_quiz/"
    boss = "./assets/sounds/black/h/"


