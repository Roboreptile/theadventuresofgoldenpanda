import pygame

from src.main_const import *
from src.objects.object_base import CustomObject
from src.levels.level_factory_data import LevelFactoryData
from math import cos, pi
from random import randint
from src.popups.popup_crit_laser import LaserCrit
from src.popups.popup_fire import FireBoom
from src.popups.popup_forestfire import ForestFire
from src.popups.popup_heal import Heal
from src.popups.popup_ima_fire import ImaFireMyLazer
from src.popups.popup_kapow import Kapow
from src.popups.popup_ko import KO
from src.popups.popup_megalaser import MegaLaser
from src.popups.popup_load import Load
from src.popups.popup_nuke import Nuke
from src.popups.popup_planetboom import PlanetBoom

class FinalPlayer(CustomObject):
    def __init__(self, x,y, levelData,scale = (300,300), restart = None):
        super().__init__(PATH.boss+"char.png", x, y, levelData, scale=scale)

        self.ch1 = pygame.image.load(PATH.boss+"char_1.png").convert_alpha()
        self.ch1 = pygame.transform.scale(self.ch1, scale)

        self.ch2 = pygame.image.load(PATH.boss+"char_2.png").convert_alpha()
        self.ch2 = pygame.transform.scale(self.ch2, scale)

        self.atk = pygame.image.load(PATH.boss+"char_attack.png").convert_alpha()
        self.atk = pygame.transform.scale(self.atk, scale)

        self.shield = pygame.image.load(PATH.boss+"char_shield.png").convert_alpha()
        self.shield = pygame.transform.scale(self.shield, scale)

        self.is_attacking = 0

        self.boss = None ## set it
        self.health_pts = 100
        self.max_health_pts = 100

        self.health = pygame.image.load(PATH.boss+"health.png").convert_alpha()
        self.no_health = pygame.image.load(PATH.boss+"no_health.png").convert_alpha()

        self.h = pygame.transform.scale(self.health, (self.health_pts*2, 40))
        self.no_health = pygame.transform.scale(self.no_health, (self.max_health_pts*2, 40))

        self.counter = 0
        self.img_c = 0
        self.radians = 0
        self.countdown = -1

        self.choicesLocked = False
        self.charges = 0

        self.restart = restart

        self.updateHealthbar()

    def update(self):
        super().update()
        self.counter +=1 
        self.countdown -= 1
        if(self.countdown == 0):
            self.unlockChoicesAfterDelay()
        if(self.is_attacking == 0):
            if self.counter>= 20:
                self.counter = 0
                if(self.img_c == 0):
                    self.img_c = 1
                    self.image = self.ch2
                else:
                    self.img_c = 0
                    self.image = self.ch1

        elif(self.is_attacking == 1):
            self.image = self.atk
            if(self.counter>=20):
                self.counter = 0
                self.is_attacking = 0
                if(self.img_c == 0):
                    self.img_c = 1
                    self.image = self.ch2
                else:
                    self.img_c = 0
                    self.image = self.ch1
        else:
            self.image = self.shield
            self.counter = 0

    def onAttack(self):
        if(self.choicesLocked == True): return
        else: self.choicesLocked = True
        self.is_attacking = 1
        self.counter = 0
        self.playSound("punch")
        self.boss.takeDmg(2)
        self.levelData.popup_manager.add(Kapow(self.levelData,self.boss.rect.center,lambda:self.boss.takeAction()))

    def onShield(self):
        if(self.choicesLocked == True): return
        else: self.choicesLocked = True
        self.is_attacking = 2
        self.counter = 0
        self.playSound("shield")
        self.boss.takeAction()
    
    def onHeal(self):
        if(self.choicesLocked == True): return
        else: self.choicesLocked = True
        self.health_pts += 20
        self.h = pygame.transform.scale(self.health, (self.health_pts*2, 40))
        self.updateHealthbar()
        self.playSound("heal")
        self.levelData.popup_manager.add(Heal(self.levelData,self.rect.center,lambda:self.boss.takeAction()))

    def onLoad(self):
        if(self.choicesLocked == True): return False
        else: self.choicesLocked = True

        if(self.charges < 7):
            self.playSound("charge")
            self.charges += 1
            self.levelData.popup_manager.add(Load(self.levelData, self.rect.center, lambda:self.boss.takeAction()))
        else:
            self.playSound("ima_firin")
            self.levelData.popup_manager.add(ImaFireMyLazer(self.levelData, self.rect.center, lambda:self.lazerSecond()))
        return True

    def lazerSecond(self):
        self.levelData.popup_manager.add(ForestFire(self.levelData, lambda:self.lazerThird()))
    def lazerThird(self):
        self.playSound("final_attack")
        self.levelData.popup_manager.add(Nuke(self.levelData, lambda:self.lazerFourth()))
    def lazerFourth(self):
        self.levelData.popup_manager.add(PlanetBoom(self.levelData, lambda:self.killBoss()))
    def killBoss(self):
        self.boss.takeDmg(100)
        self.boss.takeAction()

    def takeDmg(self, damage, true):
        if(true):
            self.health_pts -= damage
        else:
            if(self.is_attacking == 2):
                self.health_pts -= damage//10
            else:
                self.health_pts -= damage

        l = self.health_pts
        if(l<0): 
            l = 0
            self.health_pts = 0
        self.h = pygame.transform.scale(self.health, (l*2, 40))
        self.is_attacking = 0
        self.updateHealthbar()

        if(self.health_pts == 0):
            self.playSound("ko")
            self.playKillSound()
            self.levelData.popup_manager.add(KO(self.levelData,lambda:self.restart()))

    def playSound(self, sound, volume = 1):
        s = pygame.mixer.Sound(SOUNDS.black+sound+".mp3")
        s.set_volume(volume)
        self.levelData.channel1.play(s)

    def playSoundH(self, sound, volume = 1):
        s = pygame.mixer.Sound(SOUNDS.boss+sound+".mp3")
        s.set_volume(volume)
        self.levelData.channel2.play(s)

    def unlockChoices(self):
        self.countdown = 30

    def unlockChoicesAfterDelay(self):
        self.boss.newAction()
        self.choicesLocked = False

    def playKillSound(self):
        s = pygame.mixer.Sound(SOUNDS.boss+"kill"+".mp3")
        s.set_volume(1)
        self.levelData.channel2.play(s)
        self.levelData.channel1.stop()
        self.levelData.channel3.stop()
        self.levelData.channel4.stop()

        
        
    def updateHealthbar(self):

        h = self.levelData.font.render(str(self.health_pts), True, (255,255,255))

        self.ch1.blit(self.no_health, (0,0))
        self.ch1.blit(self.h, (0,0))
        self.ch1.blit(h, (0,0))

        self.ch2.blit(self.no_health, (0,0))
        self.ch2.blit(self.h, (0,0))
        self.ch2.blit(h, (0,0))


        self.shield.blit(self.no_health, (0,0))
        self.shield.blit(self.h, (0,0))
        self.shield.blit(h, (0,0))

        self.atk.blit(self.no_health, (0,0))
        self.atk.blit(self.h, (0,0))
        self.atk.blit(h, (0,0))





    


    

    
