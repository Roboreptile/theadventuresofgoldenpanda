import pygame
from src.popups.popup_plus import PopupPlus
from src.main_const import *

thumbs_up_array = []

class ThumbsUp(PopupPlus):
    def __init__(self, levelData, onFinish = None, change_every_x_frames = 2):
        global thumbs_up_array
        super().__init__(onFinish, thumbs_up_array, levelData, change_every_x_frames)

        self.levelData.channel3.play(pygame.mixer.Sound(SOUNDS.f+"yayy.mp3"))

    @staticmethod
    def pre_init():
        global thumbs_up_array
        thumbs_up_array = PopupPlus.loadGIFFolder("yeah_dawg")