from src.objects.object_base import CustomObject
from src.levels.level_factory_data import LevelFactoryData
from src.main_const import *
import pygame

class SecretButton(CustomObject):
    def __init__(self, x, y, levelData:LevelFactoryData, onClick):
        super().__init__(PATH.obj+"button.png", x, y, levelData)
        self.c = onClick

    def update(self):
        super().update()
        if(self.levelData.events!=None):
            for event in self.levelData.events:
                if event.type == pygame.MOUSEBUTTONDOWN:
                        if self.rect.collidepoint(event.pos):
                            self.c()