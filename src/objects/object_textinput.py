import pygame
from src.main_const import *
from src.levels.level_factory_data import LevelFactoryData
from src.objects.object_base import CustomObject

class TextInput(CustomObject):
    def __init__(self, x, y,  levelData, size = (300,50), message = "Hasło", inactive_color = (128,128,128), active_color = (0,0,0), key = "", max_len = 15):
        super().__init__(PATH.obj+"_textbox.png", x, y, levelData, scale=size)
        self.string = message
        self.message = message
        self.active = False
        self.inactive_color = inactive_color
        self.active_color = active_color
        self.color = inactive_color
        self.txt_surface = self.levelData.font.render(self.string, True, self.color)
        self.key = key
        self.correct = False
        self.max_len = max_len

        self.active_surface = pygame.Surface((10,10), pygame.SRCALPHA, 32)
        self.active_surface.fill((0,255,0))

    def update(self):
        super().update()
        if self.levelData.events != None: self.handleEvents()
        self.image = self.image_base.copy()
        self.txt_surface = self.levelData.font.render(self.string, True, self.color)
        if(self.active):
            self.image.blit(self.active_surface,(4,4))
        self.image.blit(self.txt_surface, (10,0))
    
    def handleEvents(self):
        for event in self.levelData.events:
            if event.type == pygame.MOUSEBUTTONDOWN:
                if self.rect.collidepoint(event.pos):
                    if self.string == self.message: self.string = ""
                    self.active = not self.active
                else:
                    self.active = False
                    if self.string=="": self.string = self.message
                self.color = self.active_color if self.active else self.inactive_color
            if event.type == pygame.KEYDOWN:
                if self.active:
                    if event.key == pygame.K_RETURN:
                        if self.key.lower() == self.string.lower(): 
                            self.correct = True
                            self.active = False
                        else: self.correct = False
                        self.string = self.message
                    elif event.key == pygame.K_BACKSPACE:
                        self.string = self.string[:-1]
                    else:
                        if len(self.string)<self.max_len: self.string += event.unicode
        
    def isCorrect(self):
        if self.correct:
            self.correct = False
            return True
        return False