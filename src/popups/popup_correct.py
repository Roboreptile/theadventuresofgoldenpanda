import pygame
from src.popups.popup_plus import PopupPlus
from src.main_const import *

correct_array = []

class Correct(PopupPlus):
    def __init__(self, levelData, onFinish = None, change_every_x_frames = 2):
        global correct_array
        super().__init__(onFinish, correct_array, levelData, change_every_x_frames)

        self.levelData.channel3.play(pygame.mixer.Sound(SOUNDS.f+"yayy.mp3"))

    @staticmethod
    def pre_init():
        global correct_array
        correct_array = PopupPlus.loadGIFFolder("correct")