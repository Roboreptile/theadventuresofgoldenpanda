import pygame
from src.popups.popup_plus import PopupPlus
from src.main_const import *

s1_array = []

class Spark1(PopupPlus):
    def __init__(self, levelData, onFinish = None, change_every_x_frames = 4):
        global s1_array
        super().__init__(onFinish, s1_array, levelData, change_every_x_frames, True)

    @staticmethod
    def pre_init():
        global s1_array
        s1_array = PopupPlus.loadGIFFolder("sparkles1",scale=(400,400))