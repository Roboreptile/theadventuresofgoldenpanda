import pygame
from src.popups.popup_plus import PopupPlus
from src.main_const import *

ds_array = []

class DarkSouls(PopupPlus):
    def __init__(self, levelData, onFinish = None, change_every_x_frames = 4):
        global ds_array
        super().__init__(onFinish, ds_array, levelData, change_every_x_frames)

        self.levelData.channel3.play(pygame.mixer.Sound(SOUNDS.f+"dark.mp3"))

    @staticmethod
    def pre_init():
        global ds_array
        ds_array = PopupPlus.loadGIFFolder("dead", scale=(SCREEN.w,SCREEN.h))