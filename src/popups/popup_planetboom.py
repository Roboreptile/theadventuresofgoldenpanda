import pygame
from src.popups.popup_boss import PopupBoss
from src.main_const import *


planetboom_boss_arr = []

class PlanetBoom(PopupBoss):
    def __init__(self, levelData, onFinish = None):
        global planetboom_boss_arr
        super().__init__(levelData, None, onFinish, planetboom_boss_arr, 2, True)

    @staticmethod
    def pre_init():
        global planetboom_boss_arr
        planetboom_boss_arr = PopupBoss.loadGIFFolder("planet_boom", scale = (SCREEN.w,SCREEN.h))

