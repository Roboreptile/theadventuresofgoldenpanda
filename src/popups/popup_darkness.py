import pygame
from src.main_const import *
from src.popups.popup_base import Popup

dark_array = [0] * 255
dark_array_reversed = [0] * 255

class Darkness(Popup):
    def __init__(self, speed, reverse = False, color = (0,0,0),  onFinish = None):
        global dark_array, dark_array_reversed
        super().__init__(onFinish)
        self.speed = speed
        self.images = [0]*255
        
        if color != (0,0,0):
            if not reverse:
                ii = 0
                for i in range(254,-1,-1):
                    surface = pygame.Surface([SCREEN.w, SCREEN.h], pygame.SRCALPHA, 32)
                    surface.fill((color[0],color[1],color[2],i+1))
                    self.images[ii] = surface
                    ii+=1
            else:
                for i in range(0,255,1):
                    surface = pygame.Surface([SCREEN.w, SCREEN.h], pygame.SRCALPHA, 32)
                    surface.fill((color[0],color[1],color[2],i+1))
                    self.images[i] = surface
        else:
            if not reverse:
                self.images = dark_array
            else:
                self.images = dark_array_reversed           

        self.image = self.images[0]   
        self.rect = self.image.get_rect()
        self.idx = 0

    def update(self):
        self.idx += self.speed
        if(self.idx>254):
            self.idx = 254
            self.status = True
        self.image = self.images[self.idx]

    @staticmethod
    def pre_init():
        global dark_array, dark_array_reversed
        ii = 0
        for i in range(254,-1,-1):
            surface = pygame.Surface([SCREEN.w, SCREEN.h], pygame.SRCALPHA, 32)
            surface.fill((0,0,0, i+1))
            dark_array_reversed[ii] = surface
            ii+=1
        for i in range(0,255,1):
            surface = pygame.Surface([SCREEN.w, SCREEN.h], pygame.SRCALPHA, 32)
            surface.fill((0,0,0,i+1))
            dark_array[i] = surface


