import pygame
from src.popups.popup_boss import PopupBoss
from src.main_const import *


fireboom_boss_arr = []

class FireBoom(PopupBoss):
    def __init__(self, levelData, pos, onFinish = None):
        global fireboom_boss_arr
        super().__init__(levelData, pos, onFinish, fireboom_boss_arr, 2, True)

    @staticmethod
    def pre_init():
        global fireboom_boss_arr
        fireboom_boss_arr = PopupBoss.loadGIFFolder("fire_boom", scale = (300,300))

