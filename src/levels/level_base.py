import pygame
from src.objects.object_genie_textinput import GenieTextInput
from src.objects.object_secret_bush import SecretBush
from src.objects.object_crystal import Crystal
from src.objects.object_floor import Floor
from src.objects.object_door import Door
from src.objects.object_portal import Portal
from src.objects.object_textinput import TextInput
from src.levels.level_factory_data import LevelFactoryData
from src.popups.popup_darkness import Darkness
from src.objects.object_throwable import Throwable
from src.main_const import *

class Level:

    # always call this as a first function in inherited level (super.init())
    # OVERRIDE this and add additionall stuff
    # level_len and background to set basic stuff
    # self.startiong_objects - array that contains objects that are present throughout all stages
    # add check for self.levelData.im_cheating
    # self.levelData.player.lockMovement() locks movement of the player until .. .unlockMovement() is called
    # at the end, call self.stageOne() where you set self.objects as the starting objects
    def __init__(self, levelData:LevelFactoryData, stop_sound = True):
        self.levelData = levelData
        self.objects = []
        self.activity = False #set this to true whenever you want to remove unnecessary objects from the level
        self.score = [0]
        self.levelData.player.unlockMovement()
        if(stop_sound == True): 
            self.levelData.channel0.stop()
            self.levelData.channel1.stop()
            self.levelData.channel2.stop()
            self.levelData.channel3.stop()
            self.levelData.channel4.stop()
        self.levelData.force_restart = False
        self.levelData.player.shrink()    

    # OVERRIDE, return true when level needs rebuild, change self.objects
    def doNextStep(self):
        return False  

    # USE this to open the given level's door
    def open(self, color=""):
        if color == "":
            for obj in self.objects:
                if isinstance(obj, Door):
                    color = obj.get_color()
                    break
        
        for obj in self.objects:
            if isinstance(obj, Door):
                if(obj.get_color() == color):
                    obj.open()

    # USE this to increase level's score
    def increaseScore(self):
        self.score[0] = self.score[0] + 1 

    #======================= DO NOT EDIT STUFF BELOW ==============================================

    # do not touch
    def generate(self):
        return self.objects

    # do not touch
    def enter(self, pos):
        for obj in self.objects:
            if isinstance(obj, TextInput) or isinstance(obj, GenieTextInput):
                if(obj.active == True): return
        for obj in self.objects:
            if isinstance(obj, Door) or isinstance(obj, Portal) or isinstance(obj, Crystal) or isinstance(obj, SecretBush):
                if obj.enter(pos):
                    self.levelData.popup_manager.add(Darkness(speed = GAME.max_fps//3, reverse = False, onFinish = lambda: self._enter(obj, pos)))
                    break

    # do not touch
    def _enter(self, obj, pos):
        self.levelData.player.resetPos()
        self.levelData.popup_manager.deactivateAll()
        if isinstance(obj, Door):
            self.doorRoomChange(obj)
            self.doorColorChange(obj)
            self.saveProgress(obj.get_color(), self.levelData.room_nr)
        if isinstance(obj, Portal) or isinstance(obj, Crystal):
            self.portalColorChange(obj)
            self.portalRoomChange(obj)
        if isinstance(obj, SecretBush):
            self.secretColorChange(obj)
            self.secretRoomChange(obj)

        self.levelData.popup_manager.add(Darkness(speed = GAME.max_fps//3, reverse = True))

    # do not touch        
    def doorColorChange(self, obj):
        self.levelData.color = obj.get_color()

    # do not touch
    def doorRoomChange(self, obj):
       self.levelData.room_nr += 1

    # do not touch
    def portalColorChange(self, obj):   
        self.levelData.color = "all"

    # do not touch
    def portalRoomChange(self, obj):
        self.levelData.room_nr = 0     

    def secretColorChange(self, obj):   
        self.levelData.color = "black"

    # do not touch
    def secretRoomChange(self, obj):
        self.levelData.room_nr = 1         
    
    # do not touch
    def dealWithActivity(self):
        while True:
            f = False
            for idx, obj in enumerate(self.objects):
                if isinstance(obj, Throwable) and  not obj.isActive():
                    f = True
                    self.objects.pop(idx)
                    self.levelData.sprites.remove(obj)
                    break
            if not f:
                break

    # do not touch
    def handleActivity(self):
        return self.activity

    def reset(self):
        self.levelData.force_restart = True


    def saveProgress(self, color, room):
        file = open("./src/.SAVE", "r")
        text = file.readline()
        text = [a for a in text]
        file.close()

        
        if(color!="all"):
            idx = 0
            if(color=="red"): idx = 0
            elif(color=="orange"): idx = 1
            elif(color=="yellow"): idx = 2
            elif(color=="green"): idx = 3
            elif(color=="blue"): idx = 4
            elif(color=="navy"): idx = 5
            elif(color=="purple"): idx = 6
            else: idx = 7

            if(int(text[idx])<room):
                text[idx] = str(room)
                text = "".join(text)
                file = open("./src/.SAVE", "w")
                file.write(text)
                file.close()

    def loadProgress(self):
        room = self.levelData.room_nr
        color = self.levelData.color
        
        if(color!="all"):
            idx = 0
            if(color=="red"): idx = 0
            elif(color=="orange"): idx = 1
            elif(color=="yellow"): idx = 2
            elif(color=="green"): idx = 3
            elif(color=="blue"): idx = 4
            elif(color=="navy"): idx = 5
            elif(color=="purple"): idx = 6
            else: idx = 7

            file = open("./src/.SAVE", "r")
            text = file.readline()
            text = [a for a in text]
            file.close()

            if(int(text[idx]) > room):
                self.open()
                self.levelData.player.unlockMovement()
