import pygame
from src.main_const import *
from src.levels.level_base import Level
from src.objects.object_base import CustomObject
from src.objects.object_floor import Floor
from src.objects.object_portal import Portal
from src.objects.object_textinput import TextInput
from src.objects.object_board import Board
from src.objects.object_door import Door
from src.objects.object_throwable import Throwable
from src.objects.object_basket import Basket
from src.objects.object_seaweed import SeaWeed
from src.objects.object_queue import Queue
from src.popups.popup_trophy import Trophy
from src.popups.popup_over import GameOver
from src.objects.object_keyboard import KeyBoard
from src.objects.object_keyboard_button import Key
from src.objects.object_genie import Genie
from src.popups.popup_genie_cloud import GenieCloud
from src.objects.object_luckyblock import LuckyBlock
from src.popups.popup_cloud import Cloud
from src.objects.object_crystal import Crystal
from src.objects.object_multitextinput import MultiTextInput
from src.objects.object_mathboard import MathBoard
from src.objects.object_mathtextinput import MathTextInput
from src.popups.popup_fuse import Fuse
from src.popups.popup_boom import Boom
from src.objects.object_secret_button import SecretButton
from src.popups.popup_darksouls import DarkSouls
from src.objects.object_crystal import Crystal
from src.objects.object_moo import Moo
from src.objects.object_tnt import Tnt


class PurpleLevelOne(Level):
    def __init__(self, levelData):
        super().__init__(levelData)
        self.levelData.level_len = TILE.w*15
        self.levelData.background = "labirynth.png"

        self.textBox = TextInput(1150+300, 360, self.levelData, key = "Im zoomin like a crazy man",max_len=26, size=(400,50))
        self.starting_objects = [
            #Floor(levelData),
            Portal(200,0,levelData),
            CustomObject(PATH.obj+"thing.png", 600, -50, self.levelData, scale=(720,630)),
            Door("purple", 1200+300, 0, levelData),
            Board(1135+300,350, levelData, size = (430, 70)),
            self.textBox
        ]
        self.stageOne()
        self.loadProgress()
        s = pygame.mixer.Sound(SOUNDS.f+"dungeon.mp3")
        s.set_volume(0.5)
        self.levelData.channel0.play(s)

        self.levelData.popup_manager.add(Cloud(
            textArray = [
            "Witamy na poziomach fioletowych - RANDOM STUFF",
            "Są tu losowe zagadki, które wymyśliliśmy, które nie do końca pasowały do pozostałych kategorii.",
            "Have fun!"
            ],
            personArray = ["peter", "wojciu","peter"], 
            levelData = levelData,),
        )

        if levelData.im_cheating: self.open()

    def stageOne(self):
        self.objects = self.starting_objects.copy()

    def doNextStep(self):
        if self.textBox.isCorrect():
            self.levelData.popup_manager.add(Trophy(self.levelData, onFinish=lambda:self.open()))  

class PurpleLevelTwo(Level):
    def __init__(self, levelData):
        super().__init__(levelData)
        self.levelData.level_len = TILE.w*13
        self.levelData.background = "main.png"

        self.textBox = MultiTextInput(1200+200, 360, self.levelData, answers = [
            "Róża", "Ass", "Farmer", "Penis", "Urwis","Cry", "Ryj", "Wir", "Nigger", "Cap", "Rura", "Porn"
        ])
        self.starting_objects = [
            Portal(100,0,levelData),
            CustomObject(PATH.obj+"tree3.png", 1223, -20, self.levelData),
            Door("purple", 1200+200, 0, levelData),
            Floor(levelData),
            CustomObject(PATH.obj+"tree2.png", 348, -20, self.levelData),
            CustomObject(PATH.obj+"tree.png", 721, -20, self.levelData),
            CustomObject(PATH.obj+"flower_2.png", 200, -40, self.levelData),
            CustomObject(PATH.obj+"cross.png",600,100,levelData, scale=(350,350)),
            Board(1185+200,350, levelData, size = (330, 70)),
            

            self.textBox
        ]
        self.stageOne()
        self.loadProgress()

        s = pygame.mixer.Sound(SOUNDS.f+"sponge.mp3")
        s.set_volume(0.5)
        self.levelData.channel0.play(s)

        self.levelData.popup_manager.add(Cloud(
            textArray = [
            "Znajdź wszystkie słowa na tablicy (12).",
            "Słowa bierz zawsze max długości (aka, z 'sexy' i 'sex' dobre będzie tylko sexy)"], 
            personArray = ["peter","peter"], 
            levelData = levelData,),
        )

        if levelData.im_cheating: self.open()

    def stageOne(self):
        self.objects = self.starting_objects.copy()

    def doNextStep(self):
        if self.textBox.isCorrect():
            self.levelData.popup_manager.add(Trophy(self.levelData, onFinish=lambda:self.open())) 



class PurpleLevelThree(Level):
    def __init__(self, levelData):
        super().__init__(levelData)

        self.levelData.level_len = TILE.w*15
        self.levelData.background = "main.png"
        self.starting_objects = [
            Floor(levelData),
            Portal(200,0,levelData),
            CustomObject(PATH.obj+"rainbow_flower.png",300,0,levelData),
            Board(800-200,0, levelData, size = (1000,500)),
            CustomObject(PATH.obj+"bush_2.png", 560, -20, self.levelData),

            Door("purple", 1800-200, 0, levelData),     
            CustomObject(PATH.obj+"bush_3.png", 1490, -40, self.levelData),

        ]
        self.textBox = 0
        self.stageOne()
        self.loadProgress()
        
        self.stage = 1

        if levelData.im_cheating: self.open()

        s = pygame.mixer.Sound(SOUNDS.f+"wii.mp3")
        s.set_volume(0.5)
        self.levelData.channel0.play(s)


    def stageOne(self):
        self.objects = self.starting_objects.copy()
        to_add = [
            CustomObject(PATH.rebus+"butterfly.png", 950, 150, self.levelData, scale = (300,300)),
        ]
        for obj in to_add:
            self.objects.append(obj)
        self.textBox = TextInput(1300-150-200,40, self.levelData, key = "Butterfly", max_len=17)
        self.objects.append(self.textBox)

        self.levelData.popup_manager.add(Cloud(
            textArray = ["Czas na rebusy!", "What could it be?"], 
            personArray = ["peter","peter"], 
            levelData = self.levelData,),
        )

    def stageTwo(self):
        self.objects = self.starting_objects.copy()
        to_add = [
            CustomObject(PATH.rebus+"butter.png", 1000-250, 150, self.levelData, scale = (300,300)),
            CustomObject(PATH.rebus+"fly.png", 1400-300, 150, self.levelData, scale = (300,300)),
        ]
        for obj in to_add:
            self.objects.append(obj)
        self.textBox = TextInput(1300-150-200,40, self.levelData, key = "Butterfly", max_len=17)

        self.objects.append(self.textBox)

    def stageThree(self):
        self.objects = self.starting_objects.copy()
        to_add = [
            CustomObject(PATH.rebus+"ass.jfif", 800-150, 150, self.levelData, scale = (300,300)),
            CustomObject(PATH.rebus+"ass.jfif", 820-150, 110, self.levelData, scale = (300,300)),
            CustomObject(PATH.rebus+"er.jfif", 1150-200, 150, self.levelData, scale = (300,300)),
            CustomObject(PATH.rebus+"ufo.png", 1400-150, 150, self.levelData, scale = (300,300)),
        ]
        for obj in to_add:
            self.objects.append(obj)
        self.textBox = TextInput(1300-150-200,40, self.levelData, key = "Big Ass Butterfly", max_len=17)

        self.objects.append(self.textBox)
        

    def doNextStep(self):
        if self.textBox.isCorrect():
            self.stage += 1
            if(self.stage == 2):
                self.stageTwo()
            elif(self.stage == 3):
                self.stageThree()
            else:
                self.levelData.popup_manager.add(Trophy(self.levelData, onFinish=lambda:self.open()))
            return True         
        return False

class PurpleLevelFour(Level):
    def __init__(self, levelData):
        super().__init__(levelData)
        self.levelData.level_len = TILE.w*16
        self.levelData.background = "main.png"
        self.mti = MathTextInput(610,20, levelData)
        self.mb = MathBoard(590,0,levelData,(340,320),self.mti)
        self.levelData.player.lockMovement()

        self.starting_objects = [
            Floor(levelData),
            Portal(200,0,levelData),
            CustomObject(PATH.obj+"tree.png", 420, -40, self.levelData),
            Door("purple", 1200+200, 0, levelData),
            self.mb,
            self.mti,
            Tnt(1300,-100,levelData),
            CustomObject(PATH.obj+"flower_2.png", 880, -100, self.levelData),
            SecretButton(972,-18, levelData, lambda:self.victory())
        ]
        self.stageOne()
        self.loadProgress()

        s = pygame.mixer.Sound(SOUNDS.f+"happy.mp3")
        s.set_volume(0.5)
        self.levelData.channel0.play(s)

        self.levelData.popup_manager.add(Cloud(
            textArray = [
            "Szybko! zanim bomba wybuchnie!",
            "TIP: Jeśli chcesz wejść do portalu, jest to możliwe tylko na pół rekundy po resecie poziomu.",
            "Także spamuj spację ;P"], 
            personArray = ["wojciu", "peter","peter"], 
            levelData = levelData,),
        )

        if levelData.im_cheating: 
            self.levelData.popup_manager.deactivateAll()
            self.open()

    def stageOne(self):
        self.objects = self.starting_objects.copy()
        self.levelData.popup_manager.add(Fuse(1250,-100, self.levelData, lambda:self.onFuseFinish()))

    def onFuseFinish(self):
        self.levelData.popup_manager.add(Boom(1200,-100, self.levelData, lambda:self.reset()))

    def victory(self):
        self.levelData.popup_manager.deactivateAll()
        self.levelData.channel3.stop()
        self.levelData.player.unlockMovement()
        self.levelData.popup_manager.add(Trophy(self.levelData, lambda:self.open()))

    def doNextStep(self):
        if(self.mti.isCorrect()):
            self.mb.randomQuestion()


class PurpleLevelFive(Level):
    def __init__(self, levelData):
        super().__init__(levelData)
        self.levelData.background = "main.png"
        self.levelData.level_len = TILE.w*10

        self.starting_objects = [
            Floor(levelData),
            Portal(200,0,levelData),
            CustomObject(PATH.obj+"tree3.png", 100, -20, self.levelData),
            CustomObject(PATH.obj+"tree3.png", 632, -20, self.levelData),
            CustomObject(PATH.obj+"tree3.png", 1023, -20, self.levelData),
            Crystal("purple",800,0,levelData),

        ]
        self.stageOne()
        self.loadProgress()
        s = pygame.mixer.Sound(SOUNDS.f+"yayy.mp3")
        s.set_volume(0.5)
        self.levelData.channel0.play(s)

        self.levelData.popup_manager.add(Cloud(
            textArray = [
            "Crazy, fioletowy diament jest Twój!", "GG!"], 
            personArray = ["peter", "peter"], 
            levelData = levelData,),
        )

        if levelData.im_cheating: self.open()

    def stageOne(self):
        self.objects = self.starting_objects.copy()