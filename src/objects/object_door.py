import pygame

from src.main_const import *
from src.objects.object_base import CustomObject
from src.levels.level_factory_data import LevelFactoryData

class Door(CustomObject):
    def __init__(self, color, x, y, levelData:LevelFactoryData,scale = None):
        super().__init__(PATH.doors+"door_"+color+".png", x, y, levelData, scale = scale)
        self.openDoors = pygame.image.load(PATH.doors+"door_open.png").convert_alpha()
        if(scale!=None):
            self.openDoors = pygame.transform.scale(self.openDoors, scale)
        self.status = False
        self.color = color
        self.s = pygame.mixer.Sound(SOUNDS.f+"open.mp3")

    def open(self):
        self.status = True
        self.image = self.openDoors
        self.image_base = self.image.copy()
        self.levelData.channel2.play(self.s)

    def enter(self, pos):
        if self.status == False:
            return False
        if abs(self.real_x - pos) < self.rect.width//2:
            return True
        return False   
    
    def could_enter(self, pos):
        if abs(self.real_x - pos) < self.rect.width//2:
            return True
        return False   

    def get_color(self):
        return self.color    