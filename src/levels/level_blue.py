import pygame
from src.main_const import *
from src.levels.level_base import Level
from src.objects.object_base import CustomObject
from src.objects.object_floor import Floor
from src.objects.object_portal import Portal
from src.objects.object_textinput import TextInput
from src.objects.object_board import Board
from src.objects.object_door import Door
from src.objects.object_throwable import Throwable
from src.objects.object_basket import Basket
from src.objects.object_seaweed import SeaWeed
from src.objects.object_queue import Queue
from src.popups.popup_trophy import Trophy
from src.popups.popup_over import GameOver
from src.popups.popup_cloud import Cloud
from src.objects.object_crystal import Crystal


class BlueLevelThree(Level):
    def __init__(self, levelData):
        super().__init__(levelData)
        self.levelData.level_len = TILE.w*13
        self.levelData.background = "wojtek1.png"

        self.textBox = TextInput(1200+200, 360, self.levelData, key = "malinowy siusiak", max_len = 18)
        self.starting_objects = [
            Portal(10,0,levelData),
            Door("blue", 1200+200, 0, levelData),
            Board(1185+200,350, levelData, size = (330, 70)),
            CustomObject(PATH.obj+"rainbow_flower.png",700,0,levelData),
            self.textBox
        ]
        self.stageOne()
        self.loadProgress()

        s = pygame.mixer.Sound(SOUNDS.blue+"idk.mp3")
        s.set_volume(0.5)
        self.levelData.channel0.play(s)

        self.levelData.popup_manager.add(Cloud(
            textArray = [
            "Kutluralne krówki. Mooooooooo"], 
            personArray = ["wojciu"], 
            levelData = levelData,),
        )

        if levelData.im_cheating: self.open()

    def stageOne(self):
        self.objects = self.starting_objects.copy()

    def doNextStep(self):
        if self.textBox.isCorrect():
            self.levelData.popup_manager.add(Trophy(self.levelData, onFinish=lambda:self.open()))

class BlueLevelFour(Level):
    def __init__(self, levelData):
        super().__init__(levelData)
        self.levelData.level_len = TILE.w*13
        self.levelData.background = "wojtek2.png"

        self.textBox = TextInput(1200+200, 360, self.levelData, key = "siusiak")
        self.starting_objects = [
            Portal(100,0,levelData),
            Door("blue", 1200+200, 0, levelData),
            CustomObject(PATH.obj+"sign_3.png",0,0,levelData),
            CustomObject(PATH.obj+"sign_1.png",400,0,levelData),
            Board(1185+200,350, levelData, size = (330, 70)),
            self.textBox
        ]
        self.stageOne()
        self.loadProgress()

        s = pygame.mixer.Sound(SOUNDS.blue+"god.mp3")
        s.set_volume(0.5)
        self.levelData.channel0.play(s)

        self.levelData.popup_manager.add(Cloud(
            textArray = [
            "In the name of Moooooooooo!"], 
            personArray = ["wojciu"], 
            levelData = levelData,),
        )

        if levelData.im_cheating: self.open()

    def stageOne(self):
        self.objects = self.starting_objects.copy()

    def doNextStep(self):
        if self.textBox.isCorrect():
            self.levelData.popup_manager.add(Trophy(self.levelData, onFinish=lambda:self.open())) 

class BlueLevelTwo(Level):
    def __init__(self, levelData):
        super().__init__(levelData)
        self.levelData.level_len = TILE.w*13
        self.levelData.background = "wojtek3.png"

        self.textBox = TextInput(1200+50, 360, self.levelData, key = "1337")
        self.starting_objects = [
            Portal(200,0,levelData),
            Door("blue", 1200+50, 0, levelData),
            Board(1185+50,350, levelData, size = (330, 70)),
            self.textBox
        ]
        self.stageOne()
        self.loadProgress()

        s = pygame.mixer.Sound(SOUNDS.blue+"ress.mp3")
        s.set_volume(0.5)
        self.levelData.channel0.play(s)

        self.levelData.popup_manager.add(Cloud(
            textArray = [
            "Look at that sexy Bowser",
            "I jeszcze ci seksowni papieże...",
            "It's erection time!"], 
            personArray = ["peter","wojciu","peter"], 
            levelData = levelData,),
        )

        if levelData.im_cheating: self.open()

    def stageOne(self):
        self.objects = self.starting_objects.copy()

    def doNextStep(self):
        if self.textBox.isCorrect():
            self.levelData.popup_manager.add(Trophy(self.levelData, onFinish=lambda:self.open())) 

class BlueLevelOne(Level):
    def __init__(self, levelData):
        super().__init__(levelData)
        self.levelData.level_len = TILE.w*13
        self.levelData.background = "wojtek4.png"

        self.textBox = TextInput(1200+200, 360, self.levelData, key = "dupa kozła")
        self.starting_objects = [
            Portal(0,0,levelData),
            Door("blue", 1200+200, 0, levelData),
            Board(1185+200,350, levelData, size = (330, 70)),
            self.textBox
        ]
        self.stageOne()
        self.loadProgress()

        s = pygame.mixer.Sound(SOUNDS.blue+"hallelujah.mp3")
        s.set_volume(0.5)
        self.levelData.channel0.play(s)

        self.levelData.popup_manager.add(Cloud(
            textArray = [
            "Witaj w poziomach niebieskich - specjalość Wojcia!",
            "Wszystkie poziomy tego koloru są ode mnie Michałku!",
            "Szykuj więc dupsko i let's go!"], 
            personArray = ["peter","wojciu","wojciu"], 
            levelData = levelData,),
        )

        if levelData.im_cheating: self.open()

    def stageOne(self):
        self.objects = self.starting_objects.copy()

    def doNextStep(self):
        if self.textBox.isCorrect():
            self.levelData.popup_manager.add(Trophy(self.levelData, onFinish=lambda:self.open())) 


class BlueLevelFive(Level):
    def __init__(self, levelData):
        super().__init__(levelData)
        self.levelData.background = "main.png"
        self.levelData.level_len = TILE.w*10

        self.starting_objects = [
            Floor(levelData),
            Portal(200,0,levelData),
            CustomObject(PATH.obj+"tree.png", -100, -20, self.levelData),
            CustomObject(PATH.obj+"tree2.png", 700, -20, self.levelData),
            CustomObject(PATH.obj+"flower.png",500,0,levelData),
            Crystal("blue",800,0,levelData),
        ]
        self.stageOne()
        self.loadProgress()
        s = pygame.mixer.Sound(SOUNDS.f+"yayy.mp3")
        s.set_volume(0.5)
        self.levelData.channel0.play(s)

        self.levelData.popup_manager.add(Cloud(
            textArray = [
            "Wowowowow wow!","Lecisz jak burza!","Jasnoniebieski kryształ jest Twój!"], 
            personArray = ["peter","wojciu","wojciu"], 
            levelData = levelData,),
        )

        if levelData.im_cheating: self.open()

    def stageOne(self):
        self.objects = self.starting_objects.copy()