import pygame

from src.objects.object_base import CustomObject
from src.main_const import *

class Basket(CustomObject):
    def __init__(self,x,y,levelData):
        super().__init__(PATH.obj+"basket.png",x,y,levelData, scale = (150,100))
    
    def update(self):
        super().update()
        if self.levelData.events != None: self.handleEvents()

    def handleEvents(self):
        for event in self.levelData.events:
            if event.type == pygame.MOUSEBUTTONDOWN:
                if self.rect.collidepoint(event.pos):
                    self.levelData.player_hand[0] = 1
                


