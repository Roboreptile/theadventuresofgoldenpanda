import pygame
from src.popups.popup_darkness import Darkness
from src.objects.object_boss_button import BossButton
from src.objects.object_genie_textinput import GenieTextInput
from src.main_const import *
from src.levels.level_base import Level
from src.objects.object_base import CustomObject
from src.objects.object_floor import Floor
from src.objects.object_portal import Portal
from src.objects.object_textinput import TextInput
from src.objects.object_board import Board
from src.objects.object_door import Door
from src.objects.object_throwable import Throwable
from src.objects.object_basket import Basket
from src.objects.object_seaweed import SeaWeed
from src.objects.object_queue import Queue
from src.objects.object_mathboard import MathBoard
from src.objects.object_mathtextinput import MathTextInput
from src.popups.popup_trophy import Trophy
from src.popups.popup_over import GameOver
from src.objects.object_keyboard import KeyBoard
from src.objects.object_keyboard_button import Key
from src.objects.object_genie import Genie
from src.popups.popup_genie_cloud import GenieCloud
from src.objects.object_luckyblock import LuckyBlock
from src.objects.object_answer import Answer
from src.objects.object_question import Question, Q
from src.objects.object_quiz import Quiz
from src.popups.popup_cloud import Cloud
from src.popups.popup_fuse import Fuse
from src.popups.popup_boom import Boom
from src.objects.object_secret_button import SecretButton
from src.popups.popup_darksouls import DarkSouls
from src.objects.object_crystal import Crystal
from src.objects.object_moo import Moo
from src.objects.object_tnt import Tnt
from src.popups.popup_crit_laser import LaserCrit
from src.popups.popup_fire import FireBoom
from src.popups.popup_forestfire import ForestFire
from src.popups.popup_heal import Heal
from src.popups.popup_ima_fire import ImaFireMyLazer
from src.popups.popup_kapow import Kapow
from src.popups.popup_ko import KO
from src.popups.popup_megalaser import MegaLaser
from src.popups.popup_load import Load
from src.popups.popup_nuke import Nuke
from src.popups.popup_planetboom import PlanetBoom
from src.objects.object_playerf import FinalPlayer
from src.objects.object_boss import Boss

class BlackLevel(Level):
    def __init__(self, levelData):
        super().__init__(levelData)
        self.levelData.level_len = TILE.w*1
        self.levelData.background = "boss.png"

        self.levelData.player.makeInvisible()
        self.levelData.player.lockMovement()

        self.p = FinalPlayer(300,138, levelData, restart=lambda:self.reset())
        self.b = Boss(1100,138,levelData, victory=lambda:self.victory())
        self.p.boss = self.b
        self.b.player = self.p

        self.b1 = BossButton("atk",0,-5,self.levelData, lambda:self.p.onAttack())
        self.b2 = BossButton("def",900,-5,self.levelData, lambda:self.p.onShield())
        self.b3 = BossButton("heal",0,-123,self.levelData, lambda:self.p.onHeal())
        self.b4 = BossButton("charge_0",900,-123,self.levelData, lambda:self.p.onLoad())


        self.starting_objects = [
            self.p,
            self.b,
            self.b1,
            self.b2,
            self.b3,
            self.b4
        ]

        self.stageOne()

        s = pygame.mixer.Sound(SOUNDS.black+"boss.mp3")
        s.set_volume(0.3)
        self.levelData.channel0.play(s, -1)

        if levelData.im_cheating: self.open()

    def stageOne(self):
        self.objects = self.starting_objects.copy()

    def doNextStep(self):
        return False

    def victory(self):
        self.doorRoomChange(None)
        self.levelData.popup_manager.add(Darkness(GAME.max_fps//5, False, (0,0,0)))

class BlackLevelCredits(Level):
    def __init__(self, levelData):
        super().__init__(levelData, False)
        self.levelData.level_len = TILE.w*1
        self.levelData.background = "credits.png"

        self.levelData.player.lockMovement()

        self.starting_objects = []

        self.stageOne()

    def stageOne(self):
        self.objects = self.starting_objects.copy()

    def doNextStep(self):
        return False