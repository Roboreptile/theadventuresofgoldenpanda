import pygame
from src.main_const import *
from src.popups.popup_base import Popup

gameover_array = []

class GameOver(Popup):
    def __init__(self, levelData, onFinish = None, change_every_x_frames = 2):
        global gameover_array
        super().__init__(onFinish)
        self.idx = 0
        self.counter = 0
        self.images = gameover_array.copy()
        self.image = self.images[self.idx]
        self.rect = self.image.get_rect()
        self.rect.center = (SCREEN.w//2,SCREEN.h//2)
        self.target = change_every_x_frames
        self.levelData = levelData
        self.pos = self.rect.center

        self.levelData.channel3.play(pygame.mixer.Sound(SOUNDS.welcome+"over.mp3"))


    def update(self):
        global gameover_array
        self.counter +=1
        if self.counter>=self.target:
            self.counter = 0
            self.idx += 1
            if self.idx>=len(gameover_array):
                self.status = True
            else:
                self.image = self.images[self.idx]

    @staticmethod
    def pre_init(scale = None, count = 70, file = PATH.anims+"gameover.png"):
        global gameover_array
        gameover_array = [0]*count
        img = pygame.image.load(file)
        for i in range(0, count):
            w = img.get_width()//count
            h = img.get_height()
            new_surface = pygame.Surface((w,h), pygame.SRCALPHA, 32).convert_alpha()
            new_surface.blit(img, (-w*i,0))
            if scale!= None: new_surface = pygame.transform.scale(new_surface, scale)
            gameover_array[i] = new_surface.copy()