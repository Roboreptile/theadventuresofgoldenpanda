import pygame
from src.popups.popup_plus import PopupPlus
from src.main_const import *

thumbs_down_array = []

class ThumbsDown(PopupPlus):
    def __init__(self, levelData, onFinish = None, change_every_x_frames = 2):
        global thumbs_down_array
        super().__init__(onFinish, thumbs_down_array, levelData, change_every_x_frames)

        self.levelData.channel3.play(pygame.mixer.Sound(SOUNDS.f+"sad.mp3"))

    @staticmethod
    def pre_init():
        global thumbs_down_array
        thumbs_down_array = PopupPlus.loadGIFFolder("no_dawg")