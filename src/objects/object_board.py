import pygame

from src.objects.object_base import CustomObject
from src.main_const import *
from src.levels.level_factory_data import LevelFactoryData


class Board(CustomObject):
    def __init__(self, x, y, levelData, size, background = None, offsets = (10,10)):
        super().__init__(PATH.obj+"board.png", x, y, levelData, scale=size)
        self.background = None
        if background != None:
            self.background = pygame.image.load(background).convert_alpha()
            sx = size[0] - offsets[0]*2
            sy = size[1] - offsets[1]*2
            self.background = pygame.transform.scale(self.background, (sx,sy))
            self.image.blit(self.background, offsets)


