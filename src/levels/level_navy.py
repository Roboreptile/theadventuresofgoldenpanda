import pygame
from src.objects.object_genie_textinput import GenieTextInput
from src.main_const import *
from src.levels.level_base import Level
from src.objects.object_base import CustomObject
from src.objects.object_floor import Floor
from src.objects.object_portal import Portal
from src.objects.object_textinput import TextInput
from src.objects.object_board import Board
from src.objects.object_door import Door
from src.objects.object_throwable import Throwable
from src.objects.object_basket import Basket
from src.objects.object_seaweed import SeaWeed
from src.objects.object_queue import Queue
from src.objects.object_mathboard import MathBoard
from src.objects.object_mathtextinput import MathTextInput
from src.popups.popup_trophy import Trophy
from src.popups.popup_over import GameOver
from src.objects.object_keyboard import KeyBoard
from src.objects.object_keyboard_button import Key
from src.objects.object_genie import Genie
from src.popups.popup_genie_cloud import GenieCloud
from src.objects.object_luckyblock import LuckyBlock
from src.objects.object_answer import Answer
from src.objects.object_question import Question, Q
from src.objects.object_quiz import Quiz
from src.popups.popup_cloud import Cloud
from src.popups.popup_fuse import Fuse
from src.popups.popup_boom import Boom
from src.objects.object_secret_button import SecretButton
from src.popups.popup_darksouls import DarkSouls
from src.objects.object_crystal import Crystal
from src.objects.object_moo import Moo
from src.objects.object_tnt import Tnt



class NavyLevelOne(Level):
    def __init__(self, levelData):
        super().__init__(levelData)
        self.levelData.level_len = TILE.w*15
        self.levelData.background = "main.png"

        self.a = Answer(500,120,levelData, "answer.png")
        self.b = Answer(500,70,levelData, "answer.png")
        self.c = Answer(500,20,levelData, "answer.png")

        self.question = Question(500,165,levelData, self.a, self.b, self.c, 35, is_romantic = False, scale=(700,430))
        self.quiz = Quiz(500,0,levelData, self.question,
        {
            "1":Q("1.png","kahoot",
                "1: Jaka jest ulubiona pozycja seksualna Krówci?",
                "Taka, jak Twojej mamy",
                "Na camemberta",
                "Chad Pieter nie uprawia seksu tylko łamie serca",
                a_key="err", b_key="err", c_key="2"),
            "2":Q("1.png","kahoot",
                "No wiadomo że chad Pieter! 2: Najlpesza figura geometryczna?",
                "Koło",
                "Kwadrat",
                "Trójkąt",
                a_key="err", b_key="err", c_key="3"),
            "3":Q("1.png","kahoot",
                "Hi hi <wink wink> 3: Jak na imię ma siostra Krówci?",
                "Aneta",
                "Anita",
                "Anna",
                a_key="err", b_key="err", c_key="4"),
            "4":Q("1.png","kahoot",
                "4: Czyje imieniny są 24 kwietnia?",
                "Wojtka",
                "Krówci",
                "Siostry krówci",
                a_key="err", b_key="err", c_key="5"),
            "5":Q("1.png","kahoot",
                "Ani Wojtka ani Krówci! Hahahahaha! Did you get it? 5: Jak należy jeść kiwi?",
                "Umyte, łyżką z środka",
                "Umyte, ze skórką",
                "Pure raw bbyyyy",
                a_key="err", b_key="6", c_key="err"),
            "6":Q("1.png","kahoot",
                "No cieszę się że się zgadzamy. 6: Co kolekcjonuje Wojtek?",
                "Złamane serca",
                "Kaczki",
                "Dresy",
                a_key="err", b_key="7", c_key="err"),
            "7":Q("1.png","kahoot",
                "I hope że w dniu grania nie wbiłeś mu wcześniej do kibla... 7: Ile jest gatunków kaczek?",
                "110",
                "32",
                "273",
                a_key="8", b_key="err", c_key="err"),
            "8":Q("1.png","kahoot",
                "8: Ile BTC dasz Krówci i Wojciowi gdy będziesz milionerem?",
                "100 BTC",
                "100 BTC",
                "100 BTC",
                a_key="9", b_key="9", c_key="9"),
            "9":Q("1.png","kahoot",
                "That was a binding contract :) 9: Jakie parówki najbardziej lubi Weronika?",
                "Vege",
                "Mięsne",
                "Wojtka <wink wink>",
                a_key="err", b_key="err", c_key="10"),
            "10":Q("1.png","kahoot",
                "Obviously! I 10: Ostatnie pytanie... Kiedy jest Twoja rocznica z Magdą?",
                "1 czerwca",
                "22 lipca",
                "8 sierpnia",
                a_key="err", b_key="err", c_key="11"),
            "err":Q("err.png","err",
                "No cóż. Zdarza się. Zła odpowiedź!",
                ":(",":(",":(",
                on_finish=lambda:self.defeat(), bg = SOUNDS.f+"over_long.mp3"),
            "11":Q("win.png","yayy",
                "You did it!",
                "<3","<3","<3",
                on_finish=lambda:self.victory())
        }, "1", PATH.gay, SOUNDS.gay)

        self.starting_objects = [
            Portal(0,0,levelData),
            Door("navy", 1200+200, 0, levelData),
            Floor(levelData),
            CustomObject(PATH.obj+"tree.png", 320, -20, self.levelData),
            CustomObject(PATH.obj+"tree.png", 1000, -20, self.levelData),

            self.quiz,
            self.question,
            self.a,
            self.b,
            self.c,
            CustomObject(PATH.obj+"flower_3.png",300,-50,levelData),
        ]
        self.stageOne()
        self.loadProgress()

        s = pygame.mixer.Sound(SOUNDS.gay+"kahoot.mp3")
        self.levelData.channel2.play(s) ## WHEN COPY CHANGE CHANNEL TO 0

        self.levelData.popup_manager.add(Cloud(
            textArray = [
            "Witamy na poziomach granatowych - WIEDZA!",
            "Jak dużo wiesz o gay squadzie?",
            "Can you win this mini gay kahoot?",
            "Let's find out!"], 
            personArray = ["peter","wojciu","peter","wojciu"], 
            levelData = levelData,),
        )

        if levelData.im_cheating: self.open()

    def stageOne(self):
        self.objects = self.starting_objects.copy()

    def defeat(self):
        self.reset()

    def victory(self):
        self.levelData.popup_manager.add(Trophy(self.levelData, onFinish=lambda:self.open()))

    def doNextStep(self):
        return False
        
class NavyLevelTwo(Level):
    def __init__(self, levelData):
        super().__init__(levelData)
        self.levelData.level_len = TILE.w*18
        self.levelData.background = "main.png"

        x = 220
        self.shared = [1]
        self.starting_objects = [
            Portal(0,0,levelData),
            Floor(levelData),
            Moo(5,self.shared, x+100,100, levelData),
            Moo(2,self.shared, x+350,100, levelData),
            Moo(1,self.shared, x+600,100, levelData),
            Moo(4,self.shared, x+850,100, levelData),
            Moo(3,self.shared, x+1100,100, levelData),
            Moo(6,self.shared, x+1350,100, levelData),
            CustomObject(PATH.obj+"bush_1.png",400,-40,levelData),
            CustomObject(PATH.obj+"bush_2.png",900,-40,levelData),
            CustomObject(PATH.obj+"bush_3.png",1400,-50,levelData),

            Door("navy", x+1700, 0, levelData)
        ]
        self.stageOne()
        self.loadProgress()

        s = pygame.mixer.Sound(SOUNDS.f+"happy.mp3")
        s.set_volume(0.5)
        self.levelData.channel0.play(s)

        self.levelData.popup_manager.add(Cloud(
            textArray = [
            "Story of gay squad summed up in 6 images...",
            "W sumie, jak ta historia szła?",
            "Set it in order!"], 
            personArray = ["peter","peter","peter"], 
            levelData = levelData,),
        )

        if levelData.im_cheating: self.open()

    def stageOne(self):
        self.objects = self.starting_objects.copy()

    def doNextStep(self):
        all_clicked = True
        
        for obj in self.objects:
            if(isinstance(obj, Moo)):
                if(not obj.clicked): all_clicked = False

        if all_clicked:
            is_ok = True
            for obj in self.objects:
                if(isinstance(obj, Moo)):
                    if(not obj.correct): is_ok = False
            if is_ok:
                self.levelData.popup_manager.add(Trophy(self.levelData, onFinish=lambda:self.open())) 
            else:
                self.levelData.popup_manager.add(GameOver(self.levelData))
            for obj in self.objects:
                    if(isinstance(obj, Moo)): obj.reset()
            self.shared[0] = 1

class NavyLevelThree(Level):
    def __init__(self, levelData):
        super().__init__(levelData)
        self.levelData.level_len = TILE.w*13
        self.levelData.background = "main.png"

        self.a = Answer(500,120,levelData, "answer.png")
        self.b = Answer(500,70,levelData, "answer.png")
        self.c = Answer(500,20,levelData, "answer.png")

        self.question = Question(500,165,levelData, self.a, self.b, self.c, 80, is_romantic = True, scale=(700,430))
        self.quiz = Quiz(500,0,levelData, self.question,
        {
            "1":Q("1.png","kahoot",
                "1: Co to jest WiFi?",
                "Winda z Firanką",
                "Wielkie Fistaszki",
                "Bezprzewodowa sieć komputerowa",
                a_key="err", b_key="err", c_key="2"),
            "2":Q("2.jfif","kahoot",
                "What is this?",
                "Lampka babci",
                "Żyłka na ryby",
                "Światłowód",
                a_key="err", b_key="err", c_key="3"),
            "3":Q("3.jfif","kahoot",
                "Czym mierzy się prędkość sieci?",
                "Mb/s",
                "Km/h",
                "Sex/s",
                a_key="4", b_key="err", c_key="err"),
            "4":Q("b.png","kahoot",
                "Które połączenie jest siecią komputerową?",
                "Światłowód",
                "Kabel telefoniczny",
                "WiFi",
                a_key="err", b_key="err", c_key="5"),
            "5":Q("1.png","kahoot",
                "Które litera NIE jest oznaczeniem klasy Wi-Fi?",
                "N",
                "H",
                "B",
                a_key="err", b_key="6", c_key="err"),
            "6":Q("b.png","kahoot",
                "Standard IEEE 802.11 definiuje sieci...?",
                "Przewodowe LAN",
                "Bezprzewodowe LAN",
                "Ethernet",
                a_key="err", b_key="7", c_key="err"),
            "7":Q("b.png","kahoot",
                "Który port jest wykorzystywany przez protokół FTP?",
                "69",
                "20",
                "25",
                a_key="err", b_key="8", c_key="err"),
            "8":Q("8.jfif","kahoot",
                "Jak nazywa się końcówka skrętki telefonicznej?",
                "Rj18",
                "Rj45",
                "Rj11",
                a_key="err", b_key="err", c_key="9"),
            "9":Q("9.png","kahoot",
                "Jak nazywa się takie połączenie komputerów?",
                "Klient-serwer",
                "Drzewo",
                "Many-to-one",
                a_key="10", b_key="err", c_key="err"),
            "10":Q("b.png","kahoot",
                "Co deniniuje sieć LAN?",
                "Sieć lokalna, technologia IEEE 802.11",
                "sieć globalna, technologia IEEE 802.11",
                "sieć lokalna, technologia Ethernet",
                a_key="err", b_key="err", c_key="11"),
            "err":Q("err.png","err",
                "No cóż. Zdarza się. Zła odpowiedź!",
                ":(",":(",":(",
                on_finish=lambda:self.defeat(), bg = SOUNDS.f+"over_long.mp3"),
            "11":Q("win.png","yayy",
                "You did it!",
                "<3","<3","<3",
                on_finish=lambda:self.victory())
        }, "1", PATH.dt, SOUNDS.dt)

        self.starting_objects = [
            Portal(0,0,levelData),
            Door("navy", 1200+200, 0, levelData),
            Floor(levelData),
            CustomObject(PATH.obj+"tree2.png", 230, -10, self.levelData),
            self.quiz,
            self.question,
            self.a,
            self.b,
            self.c,
            CustomObject(PATH.obj+"tree2.png", 1200, -10, self.levelData)

        ]
        self.stageOne()
        self.loadProgress()

        s = pygame.mixer.Sound(SOUNDS.gay+"kahoot.mp3")
        self.levelData.channel2.play(s) ## WHEN COPY CHANGE CHANNEL TO 0

        self.levelData.popup_manager.add(Cloud(
            textArray = [
            "Wiemy jak bardzo kochasz quizy z DT, więc...",
            "Zrobiliśmy nasz własny!",
            "Liczymy na max punktów!"], 
            personArray = ["peter","wojciu","peter"], 
            levelData = levelData,),
        )

        if levelData.im_cheating: self.open()

    def stageOne(self):
        self.objects = self.starting_objects.copy()

    def defeat(self):
        self.reset()

    def victory(self):
        self.levelData.popup_manager.add(Trophy(self.levelData, onFinish=lambda:self.open()))

    def doNextStep(self):
        return False

class NavyLevelFour(Level):
    def __init__(self, levelData):
        super().__init__(levelData)

        self.levelData.level_len = TILE.w*15
        self.levelData.background = "main.png"
        self.starting_objects = [
            Portal(200,0,levelData),
            Floor(levelData),
            Board(800-200,0, levelData, size = (1000,600)),
            CustomObject(PATH.obj+"bush_3.png", 580, -50, self.levelData),
            Door("navy", 1800-200, 0, levelData)
        ]
        self.textBox = 0
        self.stageOne()
        self.loadProgress()
        
        self.stage = 1

        if levelData.im_cheating: self.open()

        s = pygame.mixer.Sound(SOUNDS.f+"happy.mp3")
        s.set_volume(0.5)
        self.levelData.channel0.play(s)


    def stageOne(self):
        self.objects = self.starting_objects.copy()
        to_add = [
            CustomObject(PATH.riddle+"1.png", 1000-200, 150, self.levelData, scale = (600,400)),
        ]
        for obj in to_add:
            self.objects.append(obj)
        self.textBox = TextInput(1300-150-200,40, self.levelData, key = "Candle", max_len=17)
        self.objects.append(self.textBox)

        self.levelData.popup_manager.add(Cloud(
            textArray = ["<Bez Googlania ;P> Spróbuj rozwiązać kilka moich ulubionych riddles.",
            "Odpowiedź to jedno słowo, po angielsku.",
            "Have fun!"], 
            personArray = ["peter","peter", "peter"], 
            levelData = self.levelData,),
        )

    def stageTwo(self):
        self.objects = self.starting_objects.copy()
        to_add = [
            CustomObject(PATH.riddle+"2.png", 1000-200, 150, self.levelData, scale = (600,400)),
        ]
        for obj in to_add:
            self.objects.append(obj)
        self.textBox = TextInput(1300-150-200,40, self.levelData, key = "Cloud", max_len=17)

        self.objects.append(self.textBox)

    def stageThree(self):
        self.objects = self.starting_objects.copy()
        to_add = [
            CustomObject(PATH.riddle+"3.png", 1000-200, 150, self.levelData, scale = (600,400)),
        ]
        for obj in to_add:
            self.objects.append(obj)
        self.textBox = TextInput(1300-150-200,40, self.levelData, key = "Trouble", max_len=17)

        self.objects.append(self.textBox)

    def stageFour(self):
        self.objects = self.starting_objects.copy()
        to_add = [
            CustomObject(PATH.riddle+"4.png", 1000-200, 150, self.levelData, scale = (600,400)),
        ]
        for obj in to_add:
            self.objects.append(obj)
        self.textBox = TextInput(1300-150-200,40, self.levelData, key = "Gum", max_len=17)

        self.objects.append(self.textBox)

    def stageFive(self):
        self.objects = self.starting_objects.copy()
        to_add = [
            CustomObject(PATH.riddle+"5.png", 1000-200, 150, self.levelData, scale = (600,400)),
        ]
        for obj in to_add:
            self.objects.append(obj)
        self.textBox = TextInput(1300-150-200,40, self.levelData, key = "Grape", max_len=17)

        self.objects.append(self.textBox)
        

    def doNextStep(self):
        if self.textBox.isCorrect():
            self.stage += 1
            if(self.stage == 2):
                self.stageTwo()
            elif(self.stage == 3):
                self.stageThree()
            elif(self.stage == 4):
                self.stageFour()
            elif(self.stage == 5):
                self.stageFive()    
            else:
                self.levelData.popup_manager.add(Trophy(self.levelData, onFinish=lambda:self.open()))
            return True         
        return False

class NavyLevelFive(Level):
    def __init__(self, levelData):
        super().__init__(levelData)
        self.levelData.background = "main.png"
        self.levelData.level_len = TILE.w*10

        self.starting_objects = [
            Floor(levelData),
            Portal(200,0,levelData),
            CustomObject(PATH.obj+"sign_5.png",-20,0,levelData),
            CustomObject(PATH.obj+"sign_3.png", 470,0,levelData),
            CustomObject(PATH.obj+"rainbow_flower.png",300,0,levelData),
            CustomObject(PATH.obj+"tree.png", 100, -20, self.levelData),
            CustomObject(PATH.obj+"tree2.png", 780, -20, self.levelData),
            CustomObject(PATH.obj+"tree3.png", 1000, -20, self.levelData),
            CustomObject(PATH.obj+"flower.png", 721, -20, self.levelData),

            Crystal("navy",800,0,levelData),
        ]
        self.stageOne()
        self.loadProgress()
        s = pygame.mixer.Sound(SOUNDS.f+"yayy.mp3")
        s.set_volume(0.5)
        self.levelData.channel0.play(s)

        self.levelData.popup_manager.add(Cloud(
            textArray = [
            "WP, WP. Granatowy diament jest Twój."], 
            personArray = ["peter"], 
            levelData = levelData,),
        )

        if levelData.im_cheating: self.open()

    def stageOne(self):
        self.objects = self.starting_objects.copy()