import pygame
from src.main_const import *
from src.objects.object_base import CustomObject


class Mushroom(CustomObject):
    def __init__(self, x, y, xvel, yvel, levelData):
        super().__init__(PATH.obj + "transparent.png", x, y, levelData)
        self.xvel = xvel
        self.yvel = yvel
        self.originalyvel = yvel
        self.originalxvel = xvel
        self.originaly = self.rect.centery
        self.transparent = self.image.copy()
        self.mushroom = pygame.image.load(PATH.obj + "mushroom.png").convert_alpha()
        self.still = True

    def update(self):
        if (not self.still):
            if self.rect.center[1] <= SCREEN.h - TILE.h - self.mushroom.get_height():
                self.yvel -= PLAYER.g
                self.rect_base = (self.rect_base[0] + self.xvel, self.rect_base[1] - self.yvel)
        super().update()

        rect = self.rect.center
        rectp = self.levelData.player.rect.center
        if (not self.still and abs(rect[0] - rectp[0]) < PLAYER.w // 2 and abs(rect[1] - rectp[1]) < PLAYER.h // 2):
            self.dissapear()

    def appear(self):
        self.still = False
        self.image_base = self.mushroom
        self.image = self.mushroom

    def dissapear(self):
        self.image_base = self.transparent
        self.image = self.transparent
        self.levelData.player.grow()
        self.levelData.level.open()
