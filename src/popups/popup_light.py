import pygame
from src.popups.popup_plus import PopupPlus
from src.main_const import *

light_array = []

class Light(PopupPlus):
    def __init__(self, levelData, onFinish = None, change_every_x_frames = 8):
        global light_array
        super().__init__(onFinish, light_array, levelData, change_every_x_frames, False)

    @staticmethod
    def pre_init():
        global light_array
        light_array = PopupPlus.loadGIFFolder("light")