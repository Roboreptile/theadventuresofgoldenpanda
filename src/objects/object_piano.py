import pygame
from random import randint
from src.objects.object_base import CustomObject
from src.objects.object_launchpad_button import Pad
from src.popups.popup_over import GameOver
from src.popups.popup_trophy import Trophy
from src.main_const import *

class Piano(CustomObject):
    def __init__(self,x,y,levelData, keys:list):
        super().__init__(PATH.obj+"board.png",x,y,levelData, scale = (21*30+20,130))
        self.hihi = [
            "f1","e2","d2","a1","f1","e2","d2","a1","f1","c2",
            "b1","f1","d1","c2","b1","f1","b1","a1","f1","d1",
            "B","A","B","c1","d1","e1","f1","g1","a1"
        ]
        self.hihiIdx = 0
        self.keys = keys

    def restart(self):
        self.hihiIdx = 0

        if(self.levelData.im_cheating): 
            print(self.hihi)

    def update(self):
        super().update()
        if self.levelData.events != None: self.handleEvents()

    def handleEvents(self):
        for event in self.levelData.events:
            if event.type == pygame.MOUSEBUTTONDOWN:
                for key in reversed(self.keys):
                    if key.rect.collidepoint(event.pos):
                        if(key.didHeMessUp(self.hihi[self.hihiIdx])): 
                            self.levelData.popup_manager.add(GameOver(self.levelData,lambda:self.restart()))
                            pass
                        else:
                            self.hihiIdx+=1
                            if(self.hihiIdx==len(self.hihi)):
                                self.hihiIdx = 0
                                self.levelData.popup_manager.add(Trophy(self.levelData,lambda:self.levelData.level.open()))
                        break


