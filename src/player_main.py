import pygame
from src.main_const import *
from src.levels.level_factory_data import LevelFactoryData


class Player(pygame.sprite.Sprite):
    def __init__(self, levelData: LevelFactoryData):
        x = PLAYER.w
        y = SCREEN.h - PLAYER.h - TILE.h
        self.startingpos = (x, y)
        pygame.sprite.Sprite.__init__(self)
        self.levelData = levelData

        textures = pygame.image.load(PATH.char + "panda.png").convert_alpha()

        self.originalWidth = textures.get_width()
        self.originalHeight = textures.get_height()

        self.minX = 0

        image_frame = pygame.Surface((self.originalWidth, self.originalHeight), pygame.SRCALPHA, 32)
        image_frame.blit(textures, (0, 0))

        self.image = image_frame.copy()  # DO NOT CHANGE THE NAME
        self.originalImage = self.image.copy()

        self.rect = self.image.get_rect()  # DO NOT CHANGE THE NAME
        self.rect.topleft = (x, y + TILE.offset)

        self.realtopleft = (x, y + TILE.offset)
        self.originalTopLeft = (x, y + TILE.offset)

        self.xSpeed = 0
        self.ySpeed = 0

        self.groundLevel = y + TILE.offset
        self.originalGroundLevel = y + TILE.offset
        self.jumping = False

        self.direction = "idle"
        self.lock = False

        self.bigBoi = False
        self.s = pygame.mixer.Sound(SOUNDS.f+"mushroom.mp3")


        self.invis = pygame.image.load(PATH.obj+"transparent.png")
        self.invisible = False

    def update(self):
        previous = self.realtopleft
        newX = previous[0] + self.xSpeed

        previous_semi = self.rect.topleft
        prevX = previous_semi[0]
        prevY = previous_semi[1]
        if (prevX < GAME.camera_sides_px and self.xSpeed < 0) or (
                prevX > SCREEN.w - PLAYER.w - GAME.camera_sides_px and self.xSpeed > 0):
            self.levelData.offset[0] += self.xSpeed

        newY = previous[1] + self.ySpeed
        if newY > self.groundLevel:
            newY = self.groundLevel
            self.ySpeed = 0
            self.jumping = False
        else:
            self.ySpeed += PLAYER.g

        if newX < self.minX:
            newX = self.minX
        elif newX > self.levelData.level_len - PLAYER.w:
            newX = self.levelData.level_len - PLAYER.w

        self.realtopleft = (newX, newY)
        self.rect.topleft = (newX - self.levelData.offset[0], newY)

    def move(self, left_or_right, x=PLAYER.ms):
        if self.lock == True: return
        if (x == 0):
            if (self.direction == "right" and left_or_right == "left"):
                return
            elif (self.direction == "left" and left_or_right == "right"):
                return
            else:
                self.xSpeed = 0
                self.direction = "idle"
        elif (left_or_right == "left"):
            self.xSpeed = -x
            self.direction = left_or_right
        elif (left_or_right == "right"):
            self.xSpeed = x
            self.direction = left_or_right

    def jump(self):
        if not self.lock and not self.jumping:
            self.ySpeed = - PLAYER.us
            self.jumping = True

    def animate(self):
        return None

    def ping(self):
        self.levelData.level.enter(self.realtopleft[0] + PLAYER.w // 2)

    def resetPos(self):
        self.rect.topleft = self.startingpos
        self.realtopleft = self.startingpos
        self.levelData.offset[0] = 0

    def lockMovement(self):
        self.xSpeed = 0
        self.ySpeed = 0
        self.lock = True

    def unlockMovement(self):
        self.lock = False

    def grow(self):
        if self.bigBoi: return

        self.levelData.channel3.play(self.s)
        self.groundLevel = self.groundLevel - self.image.get_height()
        self.realtopleft = (self.realtopleft[0], self.realtopleft[1] - self.image.get_height())
        self.image = pygame.transform.scale(self.image, (self.image.get_width() * 2, self.image.get_height() * 2))
        self.bigBoi = True

    def shrink(self):
        if not self.bigBoi: return
        self.image = self.originalImage
        self.realtopleft = self.originalTopLeft
        self.groundLevel = self.originalGroundLevel
        self.bigBoi = False

    def makeInvisible(self):
        if (self.invisible==True): return
        else:
            self.invisible = True
            self.image = self.invis

    def makeVisible(self):
        if (not self.invisible): return
        else:
            self.invisible = False
            self.image = self.originalImage