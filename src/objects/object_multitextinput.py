import pygame
from src.main_const import *
from src.levels.level_factory_data import LevelFactoryData
from src.objects.object_base import CustomObject
from src.objects.object_textinput import TextInput
from src.popups.popup_correct import Correct
from src.popups.popup_wrong import Wrong


class MultiTextInput(TextInput):
    def __init__(self, x, y,  levelData, size = (300,50), answers = []):
        super().__init__(x, y, levelData, size, "Wynik", (128,128,128),(0,0,0),"abc",10)

        self.txt_surface = self.levelData.font.render(self.string, True, self.color)
        self.answers = [a.lower() for a in answers]
        self.active_surface = pygame.Surface((10,10), pygame.SRCALPHA, 32)
        self.active_surface.fill((0,255,0))
    
    def handleEvents(self):
        for event in self.levelData.events:
            if event.type == pygame.MOUSEBUTTONDOWN:
                if self.rect.collidepoint(event.pos):
                    if self.string == self.message: self.string = ""
                    self.active = not self.active
                else:
                    self.active = False
                    if self.string=="": self.string = self.message
                self.color = self.active_color if self.active else self.inactive_color
            if event.type == pygame.KEYDOWN:
                if self.active:
                    if event.key == pygame.K_RETURN:
                        if self.string.lower() in self.answers: 
                            self.answers.remove(self.string.lower())
                            self.levelData.popup_manager.add(Correct(self.levelData))
                            self.active = False
                            if(len(self.answers) == 0): 
                                self.correct = True
                        else: 
                            self.correct = False
                            self.levelData.popup_manager.add(Wrong(self.levelData))
                        self.string = self.message
                    elif event.key == pygame.K_BACKSPACE:
                        self.string = self.string[:-1]
                    else:
                        if len(self.string)<self.max_len: self.string += event.unicode
        