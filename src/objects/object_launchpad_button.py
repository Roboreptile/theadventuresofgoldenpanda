import pygame
from random import randint

from src.objects.object_base import CustomObject
from src.main_const import *

class Pad(CustomObject):
    def __init__(self, idx, sound, x,y, levelData):
        super().__init__(PATH.obj+"pad_base.png",x,y,levelData)
        self.err = pygame.image.load(PATH.obj + "pad_err.png").convert_alpha()
        self.ok  = pygame.image.load(PATH.obj + "pad_ok.png").convert_alpha()
        self.flash = pygame.image.load(PATH.obj + "pad_flash.png").convert_alpha()
        self.idx = idx
        self.sound = pygame.mixer.Sound(SOUNDS.orange + sound)
        self.dur = -1
        self.sonic = False

    def update(self):
        super().update()
        if(self.dur>0): 
            self.dur -= 1
        elif(self.dur==0):
            self.image = self.image_base
            self.dur -=1
        elif(self.dur>-10):
            self.dur-=1

    def didHeMessUp(self,shouldBeThis):
        self.levelData.channel1.play(self.sound)
        if self.idx == shouldBeThis:
            self.image = self.ok
            self.dur = GAME.max_fps//2
            return False
        else:
            self.image = self.err
            self.dur = GAME.max_fps//2
            return True

    def isFlashing(self):
        if(self.sonic): return self.dur>-3
        return self.dur>-10

    def startFlashing(self,iAmSpeed = False):
        self.levelData.channel1.play(self.sound)
        self.image = self.flash
        self.sonic = iAmSpeed
        if(iAmSpeed): self.dur = GAME.max_fps//10
        else: self.dur = GAME.max_fps//2



