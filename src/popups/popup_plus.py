import pygame
from src.main_const import *
from src.popups.popup_base import Popup
from os import listdir
from pygame.image import load as l
from pygame import Surface
from random import randint

class PopupPlus(Popup):

        def __init__(self,onFinish,frames_array, levelData, change = 2, random = False, x = None, y = None, centered = False):
            super().__init__(onFinish)
            
            self.idx = 0
            self.counter = 0
            self.images = frames_array.copy()
            self.images_len = len(self.images)
            self.image = self.images[self.idx]
            self.rect = self.image.get_rect()
            if(random == True): self.rect.center = (randint(0,SCREEN.w),randint(0,SCREEN.h))
            elif(x!=None and y!=None): 
                if(centered == True): 
                    self.rect.center = (x,y)
                else:
                    self.rect.bottomleft = (x,y)
            else: self.rect.center = (SCREEN.w//2,SCREEN.h//2)
            self.target = change
            self.levelData = levelData
            self.pos = self.rect.center

        def update(self):
            self.counter +=1
            if self.counter>=self.target:
                self.counter = 0
                self.idx += 1
                if self.idx>=self.images_len:
                    self.status = True
                else:
                    self.image = self.images[self.idx]


        @staticmethod
        def loadGIFFolder(folder, scale = None):
            dir = listdir(PATH.anims_f+folder)
            count = len(dir)
            size = pygame.image.load(PATH.anims_f+folder+"/"+dir[0]).get_size()

            out = [0]*count
            for i,file in enumerate(dir):
                surface = pygame.image.load(PATH.anims_f+folder+"/"+file)
                s = pygame.Surface(size, pygame.SRCALPHA, 32).convert_alpha()
                s.blit(surface, (0,0))
                if(scale!=None): 
                    s = pygame.transform.scale(s,scale)
                out[i] = s
            return out

        @staticmethod
        def loadGIFSheet(file, count, scale = (400,400)):
            array = [0]*count
            img = pygame.image.load(PATH.anims+file)
            for i in range(0, count):
                w = img.get_width()//count
                h = img.get_height()
                new_surface = pygame.Surface((w,h), pygame.SRCALPHA, 32).convert_alpha()
                new_surface.blit(img, (-w*i,0))
                if scale!= None: new_surface = pygame.transform.scale(new_surface, scale)
                array[i] = new_surface.copy()

            return array