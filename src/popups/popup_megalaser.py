import pygame
from src.popups.popup_boss import PopupBoss
from src.main_const import *


megalaser_boss_arr = []

class MegaLaser(PopupBoss):
    def __init__(self, levelData, onFinish = None):
        global megalaser_boss_arr
        super().__init__(levelData, None, onFinish, megalaser_boss_arr, 2, True)

    @staticmethod
    def pre_init():
        global megalaser_boss_arr
        megalaser_boss_arr = PopupBoss.loadGIFFolder("laser", scale = (SCREEN.w,SCREEN.h))

