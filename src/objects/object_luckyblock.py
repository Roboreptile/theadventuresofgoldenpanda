import pygame
from src.objects.object_base import CustomObject
from src.main_const import *
from src.objects.object_mushroom import Mushroom


class LuckyBlock(CustomObject):
    def __init__(self, x, y, mushroom: Mushroom, levelData, size=(100, 100)):
        super().__init__(PATH.obj + "transparent.png", x, y, levelData, scale=size)
        self.luckyImage = pygame.image.load(PATH.obj + "luckyblock.png").convert_alpha()
        self.luckyImage = pygame.transform.scale(self.luckyImage, size)
        self.invisible = True
        self.mushroom = mushroom
        self.s = pygame.mixer.Sound(SOUNDS.green+"p_up.mp3")

    def update(self):
        super().update()

        rect = self.rect.center
        rectp = self.levelData.player.rect.center
        if (abs(rect[0] - rectp[0]) <= 80 and abs(rect[1] - rectp[1]) < (PLAYER.h // 2 + 10)):
            if self.invisible:
                self.image_base = self.luckyImage
                self.image = self.luckyImage
                self.invisible = False
                self.mushroom.appear()
                self.levelData.channel4.play(self.s)
            if self.levelData.player.ySpeed < 0:
                self.levelData.player.ySpeed = 0
