import pygame

from src.main_const import *
from src.objects.object_base import CustomObject
from src.levels.level_factory_data import LevelFactoryData
from math import cos, pi
from random import randint
from src.popups.popup_crit_laser import LaserCrit
from src.popups.popup_fire import FireBoom
from src.popups.popup_forestfire import ForestFire
from src.popups.popup_heal import Heal
from src.popups.popup_ima_fire import ImaFireMyLazer
from src.popups.popup_kapow import Kapow
from src.popups.popup_ko import KO
from src.popups.popup_megalaser import MegaLaser
from src.popups.popup_load import Load
from src.popups.popup_nuke import Nuke
from src.popups.popup_planetboom import PlanetBoom

class Boss(CustomObject):
    def __init__(self, x,y, levelData,scale = (300,300), victory = None):
        super().__init__(PATH.boss+"base.png", x, y, levelData, scale=scale)

        self.charge_base = pygame.image.load(PATH.boss+"charge.png").convert_alpha()
        self.charge_base = pygame.transform.scale(self.charge_base, scale)

        self.attack_base = pygame.image.load(PATH.boss+"attack.png").convert_alpha()
        self.attack_base = pygame.transform.scale(self.attack_base, scale)

        self.stun_base = pygame.image.load(PATH.boss+"stun.png").convert_alpha()
        self.stun_base = pygame.transform.scale(self.stun_base, scale)

        self.super_charge_base = pygame.image.load(PATH.boss+"super_charge.png").convert_alpha()
        self.super_charge_base = pygame.transform.scale(self.super_charge_base, scale)

        self.super_attack_base = pygame.image.load(PATH.boss+"super_attack.png").convert_alpha()
        self.super_attack_base = pygame.transform.scale(self.super_attack_base, scale)
        
        self.dead = pygame.image.load(PATH.boss+"dead.png").convert_alpha()
        self.dead = pygame.transform.scale(self.dead, scale)

        self.is_attacking = False

        self.player = None ## set it
        self.health_pts = 100
        self.max_health_pts = 100


        self.health = pygame.image.load(PATH.boss+"health.png").convert_alpha()
        self.no_health = pygame.image.load(PATH.boss+"no_health.png").convert_alpha()

        self.h = pygame.transform.scale(self.health, (self.health_pts*2, 40))
        self.no_health = pygame.transform.scale(self.no_health, (self.max_health_pts*2, 40))

        self.radians = 0

        self.action = 0 #'attack' 'super attack', 'stunned', 'dead'
        self.countdown = -1

        self.victory = victory

        self.stunned_rounds = 0

        self.playSoundH("start")
        self.updateHealthbar()

    def update(self):
        super().update()
        self.rect.center = (self.rect.center[0],self.rect.center[1] + cos(self.radians)*30)
        self.radians += 2*pi/180
        if self.radians>=2*pi: self.radians = 0 

        self.countdown -= 1
        if(self.countdown == 0): self.takeActionAfterDelay()

    
    def takeDmg(self, dmg = 0, true = True):
        self.health_pts -= dmg
        if(self.health_pts<= 0): 
            self.action = 3
            self.image = self.dead
        elif(self.action == 1): 
            self.image  = self.stun_base
            self.stunned_rounds = 1
            self.action = 2
        l = self.health_pts

        if(l<0): 
            l = 0
            self.health_pts = 0

        self.h = pygame.transform.scale(self.health, (self.health_pts*2, 40))
        self.updateHealthbar()

    def takeAction(self):
        self.countdown = 60

    def takeActionAfterDelay(self):
        if(self.action == 0):
            self.attack()
        elif(self.action == 1):
            self.super_attack()
        elif(self.action == 2):
            self.stunned()
        else:
            self.death()


    def attack(self):
        dmg = 10
        true = False
        self.image = self.attack_base   
        if(randint(1,4) == 1):
            dmg = 40
            true = True
        self.player.takeDmg(dmg, False)
        self.playSound("attack")
        if(true):
            self.playSoundH("crit")
            self.levelData.popup_manager.add(LaserCrit(self.levelData, self.player.rect.center, lambda:self.player.unlockChoices()))
        else:
            i = randint(0,5)
            if(i<2):
                self.playSoundH("attack"+str(i+1))
            self.levelData.popup_manager.add(Kapow(self.levelData, self.player.rect.center, lambda:self.player.unlockChoices()))
    
    def super_attack(self):
        dmg = 70
        true = False
        self.image = self.super_attack_base
        self.player.takeDmg(dmg, true)
        self.playSound("super_attack")
        self.playSoundH("super_take_dmg")
        self.levelData.popup_manager.add(FireBoom(self.levelData, self.player.rect.center, lambda:self.player.unlockChoices()))
        
    def stunned(self):
        self.updateHealthbar()
        self.player.unlockChoices()

    def death(self):
        self.playSound("ko")
        self.levelData.popup_manager.add(KO(self.levelData, lambda:self.victory()))
        self.player.unlockChoices()

    def newAction(self):
        if self.player.health_pts<=0 or self.health_pts<=0: return
        else:
            action = randint(0,4)
            if(self.stunned_rounds>0):
                self.action = 2
                self.image = self.stun_base
                self.stunned_rounds-=1
            elif(action == 0):
                self.action = 1
                self.image = self.super_charge_base
                self.playSoundH("super")
            else:
                self.action = 0
                self.image = self.charge_base
        self.updateHealthbar()

    def updateHealthbar(self):
        h = self.levelData.font.render(str(self.health_pts), True, (255,255,255))

        self.image.blit(self.no_health, (0,0))
        self.image.blit(self.h, (0,0))
        self.image.blit(h, (0,0))


    def playSound(self, sound, volume = 1):
        s = pygame.mixer.Sound(SOUNDS.black+sound+".mp3")
        s.set_volume(volume)
        self.levelData.channel1.play(s)

    def playSoundH(self, sound, volume = 1):
        s = pygame.mixer.Sound(SOUNDS.boss+sound+".mp3")
        s.set_volume(volume)
        self.levelData.channel2.play(s)
