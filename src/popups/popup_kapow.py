import pygame
from src.popups.popup_boss import PopupBoss
from src.main_const import *


kapow_boss_arr = []

class Kapow(PopupBoss):
    def __init__(self, levelData, pos, onFinish = None):
        global kapow_boss_arr
        super().__init__(levelData, pos, onFinish, kapow_boss_arr, 2, True)

    @staticmethod
    def pre_init():
        global kapow_boss_arr
        kapow_boss_arr = PopupBoss.loadGIFFolder("kapow", scale = (300,300))

