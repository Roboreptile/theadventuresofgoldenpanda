import pygame
from src.popups.popup_plus import PopupPlus
from src.main_const import *

wrong_array = []

class Wrong(PopupPlus):
    def __init__(self, levelData, onFinish = None, change_every_x_frames = 2):
        global wrong_array
        super().__init__(onFinish, wrong_array, levelData, change_every_x_frames)

        self.levelData.channel3.play(pygame.mixer.Sound(SOUNDS.f+"sad.mp3"))

    @staticmethod
    def pre_init():
        global wrong_array
        wrong_array = PopupPlus.loadGIFFolder("wrong")