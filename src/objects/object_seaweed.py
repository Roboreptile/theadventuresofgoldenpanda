import pygame
from src.objects.object_base import CustomObject
from src.main_const import *

class SeaWeed(CustomObject):
    def __init__(self,levelData):
        super().__init__(PATH.obj+"seaweed.png",-100,-100,levelData, scale = (150,100))
        self.levelData.player_hand[0] = 0

    def update(self):
        if self.levelData.player_hand[0]:
            self.rect.center = pygame.mouse.get_pos()
        else:
            self.rect.center = (-100,-100)