import pygame
from src.popups.popup_plus import PopupPlus
from src.main_const import *

t2_array = []

class Thunder2(PopupPlus):
    def __init__(self, levelData, onFinish = None, change_every_x_frames = 4):
        global t2_array
        super().__init__(onFinish, t2_array, levelData, change_every_x_frames, True)

    @staticmethod
    def pre_init():
        global t2_array
        t2_array = PopupPlus.loadGIFFolder("thunder2",scale=(400,400))