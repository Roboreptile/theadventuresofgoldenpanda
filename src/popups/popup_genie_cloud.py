import pygame
from src.main_const import *
from src.popups.popup_base import Popup

genie_array = []

class GenieCloud(Popup):
    def __init__(self, x,y, genie, levelData, onFinish = None, change_every_x_frames = 1):
        super().__init__(onFinish)
        global genie_array
        
        self.idx = 0
        self.counter = 0
        self.target = change_every_x_frames

        self.images = genie_array.copy()
        self.image = self.images[self.idx]
        self.rect = self.image.get_rect()
        self.rect.center = (x,y)
        
        self.levelData = levelData
        self.pos = (x,y)
        self.genie = genie

    def update(self):
        self.counter +=1
        if self.counter>=self.target:
            self.counter = 0
            self.idx += 1
            if self.idx==len(genie_array):
                self.status = True
            else:
                self.image = self.images[self.idx]

            if self.idx == 4:
                self.genie.rect_base = self.pos

    @staticmethod
    def pre_init(scale = None, count = 8, file = PATH.anims+"cloud.png"):
        global genie_array
        genie_array = [0]*count
        img = pygame.image.load(file)
        for i in range(0, count):
            w = img.get_width()//count
            h = img.get_height()
            new_surface = pygame.Surface((w,h), pygame.SRCALPHA, 32).convert_alpha()
            new_surface.blit(img, (-w*i,0))
            if scale!= None: new_surface = pygame.transform.scale(new_surface, scale)
            genie_array[i] = new_surface.copy()
