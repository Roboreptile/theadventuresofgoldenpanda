import pygame
from src.popups.popup_lolol import Lolol
from src.main_const import *
from src.levels.level_factory_data import LevelFactoryData
from src.objects.object_base import CustomObject
from src.popups.popup_thumbs_down import ThumbsDown
from src.popups.popup_thumbs_up import ThumbsUp
from src.objects.object_base import CustomObject

from random import randint

class GenieTextInput(CustomObject):
    def __init__(self, x, y,  levelData, size = (300,50), message = "Hasło", inactive_color = (128,128,128), active_color = (0,0,0), max_len = 40):
        super().__init__(PATH.obj+"_textbox.png", x, y, levelData, scale=size)
        self.string = message
        self.message = message
        self.active = False
        self.inactive_color = inactive_color
        self.active_color = active_color
        self.color = inactive_color
        self.txt_surface = self.levelData.font.render(self.string, True, self.color)
        self.max_len = max_len

    def update(self):
        super().update()
        if self.levelData.events != None: self.handleEvents()
        self.image = self.image_base.copy()
        self.txt_surface = self.levelData.font.render(self.string, True, self.color)
        self.image.blit(self.txt_surface, (10,0))
    
    def handleEvents(self):
        for event in self.levelData.events:
            if event.type == pygame.MOUSEBUTTONDOWN:
                if self.rect.collidepoint(event.pos):
                    if self.string == self.message: self.string = ""
                    self.active = not self.active
                else:
                    self.active = False
                    if self.string=="": self.string = self.message
                self.color = self.active_color if self.active else self.inactive_color
            if event.type == pygame.KEYDOWN:
                if self.active:
                    if event.key == pygame.K_RETURN:
                        if(randint(0,1) == 0):
                            if(randint(0,20) ==0):
                                self.levelData.popup_manager.add(Lolol(self.levelData))
                            else:
                                self.levelData.popup_manager.add(ThumbsUp(self.levelData))
                        else:
                            self.levelData.popup_manager.add(ThumbsDown(self.levelData))
                        self.string = self.message
                    elif event.key == pygame.K_BACKSPACE:
                        self.string = self.string[:-1]
                    else:
                        if len(self.string)<self.max_len: self.string += event.unicode
        
    def isCorrect(self):
        if self.correct:
            self.correct = False
            return True
        return False