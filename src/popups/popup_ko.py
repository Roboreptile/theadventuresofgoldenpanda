import pygame
from src.popups.popup_boss import PopupBoss
from src.main_const import *


ko_boss_arr = []

class KO(PopupBoss):
    def __init__(self, levelData, onFinish = None):
        global ko_boss_arr
        super().__init__(levelData, None, onFinish, ko_boss_arr, 6, True)

    @staticmethod
    def pre_init():
        global ko_boss_arr
        ko_boss_arr = PopupBoss.loadGIFFolder("ko", scale = (SCREEN.w,SCREEN.h))

