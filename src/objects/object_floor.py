import pygame

from src.main_const import *
from src.levels.level_factory_data import LevelFactoryData

class Floor(pygame.sprite.Sprite):
    def __init__(self, levelData, special=""):
        pygame.sprite.Sprite.__init__(self)
        self.levelData = levelData

        self.image = self.generateNewFloor(special)
        self.rect = self.image.get_rect()
        
    def update(self):
        self.rect.topleft = (-self.levelData.offset[0] - GAME.camera_sides_px, SCREEN.h - TILE.h)

    def generateNewFloor(self,special):   
        floor_len = self.levelData.level_len
        floor = pygame.Surface((floor_len+2*GAME.camera_sides_px,TILE.h), pygame.SRCALPHA, 32)
        tile = pygame.image.load(PATH.tiles+"2"+special+".png").convert_alpha()
        first_tile = pygame.image.load(PATH.tiles+"1"+special+".png").convert_alpha() 
        last_tile = pygame.image.load(PATH.tiles+"3"+special+".png").convert_alpha()
        if(tile.get_size()[0]!=128):
            tile = pygame.transform.scale(tile,(128,128))
            first_tile = pygame.transform.scale(first_tile,(128,128))
            last_tile = pygame.transform.scale(last_tile,(128,128))
        tmp = 0
        floor.blit(first_tile, (tmp,0))
        tmp += TILE.w
        for x in range(tmp, floor_len+2*GAME.camera_sides_px-2*TILE.w,TILE.w):
            floor.blit(tile,(x,0))   
            tmp = x
        floor.blit(last_tile,(tmp+TILE.w,0))
        return floor