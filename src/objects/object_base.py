import pygame

from src.main_const import *
from src.levels.level_factory_data import LevelFactoryData

class CustomObject(pygame.sprite.Sprite):
    def __init__(self, file_name, x,y, levelData:LevelFactoryData, angle_change = 0, sprites_count = 1, change_every_x_frames = 0, scale = None, image_base = None):
        pygame.sprite.Sprite.__init__(self)
        self.levelData = levelData
        if(image_base!=None):
            self.image_base = image_base
        else:
            self.image_base = pygame.image.load(file_name).convert_alpha()
        if scale != None: self.image_base = pygame.transform.scale(self.image_base, scale)
        self.images = [0]*sprites_count
        for i in range(0,sprites_count):
            w = self.image_base.get_width()//sprites_count
            h = self.image_base.get_height()
            img = pygame.Surface((w,h), pygame.SRCALPHA, 32)
            img.blit(self.image_base, (-i*w,0))
            self.images[i] = img

        self.image = self.images[0]
        self.rect = self.image.get_rect()
        y = SCREEN.h - TILE.h - self.image.get_height() - y + TILE.offset
        self.rect.topleft = (x,y)
        self.real_x = x + self.rect.width//2
        self.rect_base = self.rect.center
        self.rect_topleft = (x,y)
        self.angle = 0
        self.angle_change = angle_change
        self.freq = change_every_x_frames

        self.curr_img = 0
        self.curr_img_max = sprites_count
        self.freq_countdown = change_every_x_frames
        self.active = True
        
    def update(self):
        if(self.freq!=0): self.next_image()

        if(self.angle_change!=0): self.rot_center()
        elif(self.freq!=0): self.image = self.image_base

        self.rect.center = (self.rect_base[0]-self.levelData.offset[0], self.rect_base[1])

    def rot_center(self):
        self.angle += self.angle_change
        if(self.angle>360): self.angle -= 360
        self.image = pygame.transform.rotate(self.image_base, self.angle)
        self.rect = self.image.get_rect(center=self.rect_base)

    def next_image(self):
        self.freq_countdown -= 1
        if(self.freq_countdown == 0):
            self.curr_img += 1
            self.freq_countdown = self.freq
            if self.curr_img >= self.curr_img_max:
                self.curr_img = 0

            self.image_base = self.images[self.curr_img] 

    def isActive(self):
        return self.active